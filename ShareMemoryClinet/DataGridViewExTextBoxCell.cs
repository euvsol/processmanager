﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace ProcessWatcher
{
    public class DataGridViewExTextBoxCell : DataGridViewTextBoxCell, ISpannedCell
    {
        private int m_ColumnSpan = 1;

        private int m_RowSpan = 1;

        private DataGridViewExTextBoxCell m_OwnerCell;

        public int ColumnSpan
        {
            get
            {
                return m_ColumnSpan;
            }
            set
            {
                if (base.DataGridView != null && m_OwnerCell == null)
                {
                    if (value < 1 || base.ColumnIndex + value - 1 >= base.DataGridView.ColumnCount)
                    {
                        throw new ArgumentOutOfRangeException("value");
                    }

                    if (m_ColumnSpan != value)
                    {
                        SetSpan(value, m_RowSpan);
                    }
                }
            }
        }

        public int RowSpan
        {
            get
            {
                return m_RowSpan;
            }
            set
            {
                if (base.DataGridView != null && m_OwnerCell == null)
                {
                    if (value < 1 || base.RowIndex + value - 1 >= base.DataGridView.RowCount)
                    {
                        throw new ArgumentOutOfRangeException("value");
                    }

                    if (m_RowSpan != value)
                    {
                        SetSpan(m_ColumnSpan, value);
                    }
                }
            }
        }

        public DataGridViewCell OwnerCell
        {
            get
            {
                return m_OwnerCell;
            }
            private set
            {
                m_OwnerCell = value as DataGridViewExTextBoxCell;
            }
        }

        public override bool ReadOnly
        {
            get
            {
                return base.ReadOnly;
            }
            set
            {
                base.ReadOnly = value;
                if (m_OwnerCell != null || (m_ColumnSpan <= 1 && m_RowSpan <= 1) || base.DataGridView == null)
                {
                    return;
                }

                for (int i = base.ColumnIndex; i < base.ColumnIndex + m_ColumnSpan; i++)
                {
                    for (int j = base.RowIndex; j < base.RowIndex + m_RowSpan; j++)
                    {
                        if (i != base.ColumnIndex || j != base.RowIndex)
                        {
                            base.DataGridView[i, j].ReadOnly = value;
                        }
                    }
                }
            }
        }

        protected override void Paint(Graphics graphics, Rectangle clipBounds, Rectangle cellBounds, int rowIndex, DataGridViewElementStates cellState, object value, object formattedValue, string errorText, DataGridViewCellStyle cellStyle, DataGridViewAdvancedBorderStyle advancedBorderStyle, DataGridViewPaintParts paintParts)
        {
            if (m_OwnerCell != null && m_OwnerCell.DataGridView == null)
            {
                m_OwnerCell = null;
            }

            if (base.DataGridView == null || (m_OwnerCell == null && m_ColumnSpan == 1 && m_RowSpan == 1))
            {
                base.Paint(graphics, clipBounds, cellBounds, rowIndex, cellState, value, formattedValue, errorText, cellStyle, advancedBorderStyle, paintParts);
                return;
            }

            DataGridViewExTextBoxCell DataGridViewExTextBoxCell = this;
            int columnIndex = base.ColumnIndex;
            int columnSpan = m_ColumnSpan;
            int rowSpan = m_RowSpan;
            if (m_OwnerCell != null)
            {
                DataGridViewExTextBoxCell = m_OwnerCell;
                columnIndex = m_OwnerCell.ColumnIndex;
                rowIndex = m_OwnerCell.RowIndex;
                columnSpan = m_OwnerCell.ColumnSpan;
                rowSpan = m_OwnerCell.RowSpan;
                value = m_OwnerCell.GetValue(rowIndex);
                errorText = m_OwnerCell.GetErrorText(rowIndex);
                cellState = m_OwnerCell.State;
                cellStyle = m_OwnerCell.GetInheritedStyle(null, rowIndex, includeColors: true);
                formattedValue = m_OwnerCell.GetFormattedValue(value, rowIndex, ref cellStyle, null, null, DataGridViewDataErrorContexts.Display);
            }

            if (CellsRegionContainsSelectedCell(columnIndex, rowIndex, columnSpan, rowSpan))
            {
                cellState |= DataGridViewElementStates.Selected;
            }

            Rectangle spannedCellBoundsFromChildCellBounds = DataGridViewExCellHelper.GetSpannedCellBoundsFromChildCellBounds(this, cellBounds, DataGridViewHelper.SingleVerticalBorderAdded(base.DataGridView), DataGridViewHelper.SingleHorizontalBorderAdded(base.DataGridView));
            clipBounds = DataGridViewExCellHelper.GetSpannedCellClipBounds(DataGridViewExTextBoxCell, spannedCellBoundsFromChildCellBounds, DataGridViewHelper.SingleVerticalBorderAdded(base.DataGridView), DataGridViewHelper.SingleHorizontalBorderAdded(base.DataGridView));
            Graphics graphics2 = base.DataGridView.CreateGraphics();
            graphics2.SetClip(clipBounds);
            advancedBorderStyle = DataGridViewExCellHelper.AdjustCellBorderStyle(DataGridViewExTextBoxCell);
            DataGridViewExTextBoxCell.NativePaint(graphics2, clipBounds, spannedCellBoundsFromChildCellBounds, rowIndex, cellState, value, formattedValue, errorText, cellStyle, advancedBorderStyle, paintParts & ~DataGridViewPaintParts.Border);
            if ((paintParts & DataGridViewPaintParts.Border) != 0)
            {
                DataGridViewExTextBoxCell DataGridViewExTextBoxCell2 = DataGridViewExTextBoxCell;
                DataGridViewAdvancedBorderStyle dataGridViewAdvancedBorderStyle = new DataGridViewAdvancedBorderStyle();
                dataGridViewAdvancedBorderStyle.Left = advancedBorderStyle.Left;
                dataGridViewAdvancedBorderStyle.Top = advancedBorderStyle.Top;
                dataGridViewAdvancedBorderStyle.Right = DataGridViewAdvancedCellBorderStyle.None;
                dataGridViewAdvancedBorderStyle.Bottom = DataGridViewAdvancedCellBorderStyle.None;
                DataGridViewAdvancedBorderStyle advancedBorderStyle2 = dataGridViewAdvancedBorderStyle;
                DataGridViewExTextBoxCell2.PaintBorder(graphics2, clipBounds, spannedCellBoundsFromChildCellBounds, cellStyle, advancedBorderStyle2);
                DataGridViewExTextBoxCell DataGridViewExTextBoxCell3 = (base.DataGridView[columnIndex + columnSpan - 1, rowIndex + rowSpan - 1] as DataGridViewExTextBoxCell) ?? this;
                DataGridViewAdvancedBorderStyle dataGridViewAdvancedBorderStyle2 = new DataGridViewAdvancedBorderStyle();
                dataGridViewAdvancedBorderStyle2.Left = DataGridViewAdvancedCellBorderStyle.None;
                dataGridViewAdvancedBorderStyle2.Top = DataGridViewAdvancedCellBorderStyle.None;
                dataGridViewAdvancedBorderStyle2.Right = advancedBorderStyle.Right;
                dataGridViewAdvancedBorderStyle2.Bottom = advancedBorderStyle.Bottom;
                DataGridViewAdvancedBorderStyle advancedBorderStyle3 = dataGridViewAdvancedBorderStyle2;
                DataGridViewExTextBoxCell3.PaintBorder(graphics2, clipBounds, spannedCellBoundsFromChildCellBounds, cellStyle, advancedBorderStyle3);
            }
        }

        private void NativePaint(Graphics graphics, Rectangle clipBounds, Rectangle cellBounds, int rowIndex, DataGridViewElementStates cellState, object value, object formattedValue, string errorText, DataGridViewCellStyle cellStyle, DataGridViewAdvancedBorderStyle advancedBorderStyle, DataGridViewPaintParts paintParts)
        {
            base.Paint(graphics, clipBounds, cellBounds, rowIndex, cellState, value, formattedValue, errorText, cellStyle, advancedBorderStyle, paintParts);
        }

        private void SetSpan(int columnSpan, int rowSpan)
        {
            int columnSpan2 = m_ColumnSpan;
            int rowSpan2 = m_RowSpan;
            m_ColumnSpan = columnSpan;
            m_RowSpan = rowSpan;
            if (base.DataGridView == null)
            {
                return;
            }

            for (int i = base.RowIndex; i < base.RowIndex + rowSpan2; i++)
            {
                for (int j = base.ColumnIndex; j < base.ColumnIndex + columnSpan2; j++)
                {
                    if (base.DataGridView[j, i] is DataGridViewExTextBoxCell DataGridViewExTextBoxCell)
                    {
                        DataGridViewExTextBoxCell.OwnerCell = null;
                    }
                }
            }

            for (int i = base.RowIndex; i < base.RowIndex + m_RowSpan; i++)
            {
                for (int j = base.ColumnIndex; j < base.ColumnIndex + m_ColumnSpan; j++)
                {
                    if (base.DataGridView[j, i] is DataGridViewExTextBoxCell DataGridViewExTextBoxCell2 && DataGridViewExTextBoxCell2 != this)
                    {
                        if (DataGridViewExTextBoxCell2.ColumnSpan > 1)
                        {
                            DataGridViewExTextBoxCell2.ColumnSpan = 1;
                        }

                        if (DataGridViewExTextBoxCell2.RowSpan > 1)
                        {
                            DataGridViewExTextBoxCell2.RowSpan = 1;
                        }

                        DataGridViewExTextBoxCell2.OwnerCell = this;
                    }
                }
            }

            OwnerCell = null;
            base.DataGridView.Invalidate();
        }

        public override Rectangle PositionEditingPanel(Rectangle cellBounds, Rectangle cellClip, DataGridViewCellStyle cellStyle, bool singleVerticalBorderAdded, bool singleHorizontalBorderAdded, bool isFirstDisplayedColumn, bool isFirstDisplayedRow)
        {
            if (m_OwnerCell == null && m_ColumnSpan == 1 && m_RowSpan == 1)
            {
                return base.PositionEditingPanel(cellBounds, cellClip, cellStyle, singleVerticalBorderAdded, singleHorizontalBorderAdded, isFirstDisplayedColumn, isFirstDisplayedRow);
            }

            DataGridViewExTextBoxCell DataGridViewExTextBoxCell = this;
            if (m_OwnerCell != null)
            {
                int rowIndex = m_OwnerCell.RowIndex;
                cellStyle = m_OwnerCell.GetInheritedStyle(null, rowIndex, includeColors: true);
                m_OwnerCell.GetFormattedValue(m_OwnerCell.Value, rowIndex, ref cellStyle, null, null, DataGridViewDataErrorContexts.Formatting);
                if (base.DataGridView.EditingControl is IDataGridViewEditingControl dataGridViewEditingControl)
                {
                    dataGridViewEditingControl.ApplyCellStyleToEditingControl(cellStyle);
                    Control parent = base.DataGridView.EditingControl.Parent;
                    if (parent != null)
                    {
                        parent.BackColor = cellStyle.BackColor;
                    }
                }

                DataGridViewExTextBoxCell = m_OwnerCell;
            }

            cellBounds = DataGridViewExCellHelper.GetSpannedCellBoundsFromChildCellBounds(this, cellBounds, singleVerticalBorderAdded, singleHorizontalBorderAdded);
            cellClip = DataGridViewExCellHelper.GetSpannedCellClipBounds(DataGridViewExTextBoxCell, cellBounds, singleVerticalBorderAdded, singleHorizontalBorderAdded);
            return base.PositionEditingPanel(cellBounds, cellClip, cellStyle, singleVerticalBorderAdded, singleHorizontalBorderAdded, DataGridViewExCellHelper.InFirstDisplayedColumn(DataGridViewExTextBoxCell), DataGridViewExCellHelper.InFirstDisplayedRow(DataGridViewExTextBoxCell));
        }

        protected override object GetValue(int rowIndex)
        {
            if (m_OwnerCell != null)
            {
                return m_OwnerCell.GetValue(m_OwnerCell.RowIndex);
            }

            return base.GetValue(rowIndex);
        }

        protected override bool SetValue(int rowIndex, object value)
        {
            if (m_OwnerCell != null)
            {
                return m_OwnerCell.SetValue(m_OwnerCell.RowIndex, value);
            }

            return base.SetValue(rowIndex, value);
        }

        protected override void OnDataGridViewChanged()
        {
            base.OnDataGridViewChanged();
            if (base.DataGridView == null)
            {
                m_ColumnSpan = 1;
                m_RowSpan = 1;
            }
        }

        protected override Rectangle BorderWidths(DataGridViewAdvancedBorderStyle advancedBorderStyle)
        {
            if (m_OwnerCell == null && m_ColumnSpan == 1 && m_RowSpan == 1)
            {
                return base.BorderWidths(advancedBorderStyle);
            }

            if (m_OwnerCell != null)
            {
                return m_OwnerCell.BorderWidths(advancedBorderStyle);
            }

            Rectangle rectangle = base.BorderWidths(advancedBorderStyle);
            Rectangle rectangle2 = ((base.DataGridView[base.ColumnIndex + ColumnSpan - 1, base.RowIndex + RowSpan - 1] is DataGridViewExTextBoxCell DataGridViewExTextBoxCell) ? DataGridViewExTextBoxCell.NativeBorderWidths(advancedBorderStyle) : rectangle);
            return new Rectangle(rectangle.X, rectangle.Y, rectangle2.Width, rectangle2.Height);
        }

        private Rectangle NativeBorderWidths(DataGridViewAdvancedBorderStyle advancedBorderStyle)
        {
            return base.BorderWidths(advancedBorderStyle);
        }

        protected override Size GetPreferredSize(Graphics graphics, DataGridViewCellStyle cellStyle, int rowIndex, Size constraintSize)
        {
            if (OwnerCell != null)
            {
                return new Size(0, 0);
            }

            Size preferredSize = base.GetPreferredSize(graphics, cellStyle, rowIndex, constraintSize);
            DataGridView dataGridView = base.DataGridView;
            int num = preferredSize.Width;
            for (int i = base.ColumnIndex + 1; i < base.ColumnIndex + ColumnSpan; i++)
            {
                num -= dataGridView.Columns[i].Width;
            }

            int num2 = preferredSize.Height;
            for (int j = base.RowIndex + 1; j < base.RowIndex + RowSpan; j++)
            {
                num2 -= dataGridView.Rows[j].Height;
            }

            return new Size(num, num2);
        }

        private bool CellsRegionContainsSelectedCell(int columnIndex, int rowIndex, int columnSpan, int rowSpan)
        {
            if (base.DataGridView == null)
            {
                return false;
            }

            for (int i = columnIndex; i < columnIndex + columnSpan; i++)
            {
                for (int j = rowIndex; j < rowIndex + rowSpan; j++)
                {
                    if (base.DataGridView[i, j].Selected)
                    {
                        return true;
                    }
                }
            }

            return false;
        }
    }
}
