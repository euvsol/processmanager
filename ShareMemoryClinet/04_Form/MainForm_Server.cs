﻿#region Class using
using System;
using System.IO;
using System.Linq;
using System.Drawing;
using System.Threading;
using System.Diagnostics;
using System.Collections.Generic;
using System.Windows.Forms;
using Esol.Framework;
using Esol.ShareMemory;
using Esol.ShareTrxControl;
using Esol.LogControl;
using System.Runtime.InteropServices;
using System.Text;
using ProcessWatcher._01_ShareMemory;
using System.Xml;
using System.Xml.Linq;
using ProcessWatcher.Properties;
using static Esol.LogControl.Log;
#endregion

namespace ProcessWatcher
{
    public partial class MainForm_Server : Form
    {

        #region Class Constants
        /// <summary>
        /// 
        /// </summary>
        private const int WM_COPYDATA = 0x4A;
        #endregion

        #region Class Struct
        /// <summary>
        /// 
        /// </summary>
        public struct COPYDATASTRUCT
        {
            public IntPtr dwData;
            public int cbData;
            [MarshalAs(UnmanagedType.LPStr)]
            public string lpData;
        }
        #endregion

        #region Class Members
        /// <summary>
        /// 
        /// </summary>
        private object m_Object;
        /// <summary>
        /// 
        /// </summary>
        private string m_sLogDriver;
        /// <summary>
        /// 
        /// </summary>
        private string m_sServerName;
        /// <summary>
        /// 
        /// </summary>
        private int m_iLogDays;
        /// <summary>
        /// 
        /// </summary>
        private int m_iCpuRamUsageScanTime;
        /// <summary>
        /// 
        /// </summary>
        private bool m_bAutoStart;
        /// <summary>
        /// 
        /// </summary>
        private bool m_bProcessAutoStart;


        private bool _mainAliveState = false;

        private bool _mainAlarmReply = false;
        /// <summary>
        /// 
        /// </summary>
        private DateTime m_OldTime;
        /// <summary>
        /// 
        /// </summary>
        private ShareTrx m_ShareTrx;
        /// <summary>
        /// 
        /// </summary>
        private VirtualMemory m_ShareMem;
        /// <summary>
        /// 
        /// </summary>
        private SeverManager m_SeverManager;
        /// <summary>
        /// 
        /// </summary>
        private ShareMemoryManager m_ShareMemoryManager;
        /// <summary>
        /// 
        /// </summary>
        private LogManager m_LogManager;
        /// <summary>
        /// 
        /// </summary>
        private Configuration m_Configuration;
        /// <summary>
        /// 
        /// </summary>
        private Queue<PlcAddr> m_queueShareDataBunchData;
        /// <summary>
        /// 
        /// </summary>
        public event ShareEventHandler EventShareDataChanged;
        /// <summary>
        /// 
        /// </summary>
        public delegate void ShareEventHandler(PlcAddr Addr);
        /// <summary>
        /// 
        /// </summary>
        private Thread m_threadUsage;

        private Thread MainAlive;

        private Thread MainState;

        private Thread PwAlive;

        /// <summary>
        /// 
        /// </summary>
        private Thread m_threadShareData;
        /// <summary>
        /// 
        /// </summary>
        private Thread m_threadShareDataChanged;
        /// <summary>
        /// 
        /// </summary>
        private List<string> m_listOffProcess;
        /// <summary>
        /// 
        /// </summary>
        private List<Color> m_listColor;
        /// <summary>
        /// 
        /// </summary>
        private Dictionary<string, int> m_hashServer;
        /// <summary>
        /// 
        /// </summary>
        private Dictionary<string, List<string>> m_hashProcessToVersion;
        /// <summary>
        /// 
        /// </summary>
        private Dictionary<string, List<MonitoringData>> m_hashLocalNameToMd;
        /// <summary>
        /// 
        /// </summary>
        private Dictionary<string, string> m_hashLocalNoToLocalName;
        /// <summary>
        ///
        /// </summary>
        private bool m_bWindowsRestart;
        /// <summary>
        /// 
        /// </summary>
        private Dictionary<string, string[]> m_hashProcessDatas;

        private Dictionary<string, string[]> _lastestVersionData;

        private Dictionary<string, string[]> _hashClientProcess;

        private List<LastestVersionData> _versionData;
        /// <summary>
        /// 
        /// </summary>
        [DllImport("user32.dll")]
        private static extern IntPtr FindWindow(string lpClassName, string lpWindowName);
        /// <summary>
        /// 
        /// </summary>
        [DllImport("user32.dll")]
        public static extern IntPtr FindWindowEx(IntPtr hWnd1, IntPtr hWnd2, string lpsz1, string lpsz2);
        #endregion

        #region Class Properties
        /// <summary>
        /// 
        /// </summary>
        public VirtualMemory ShareMem
        {
            get
            {
                return m_ShareMem;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public ShareTrx ShareTrx
        {
            get
            {
                return m_ShareTrx;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public LogManager LogManager
        {
            get
            {
                return m_LogManager;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public string LogDriver
        {
            get
            {
                return m_sLogDriver;
            }
        }
        #endregion

        #region Class Initialize
        /// <summary>
        /// 
        /// </summary>
        public MainForm_Server()
        {
            SharedMem.Knd.Data.LoadMapData();
            SharedMem.Knd.File.Create(20000, "ESOL.SREM.MEM");

            InitializeLogManager();
            m_LogManager.WriteLog(Constants.DEF_PW_BTN, "P.W Program Start");
            InitializeMembers();
            InitializeSetting();

            m_SeverManager.OnAccepted += new EventHandler(SeverManager_OnAccepted);
            m_SeverManager.OnClosed += new EventHandler(SeverManager_OnClosed);

            Logger.FileLogger = new SimpleFileLoggerMark6(Constants.DEF_LOG_FILE_PATH, "ShareMemoryServer", 2000, 1024 * 1024 * 20, false, m_lvLog);
            Logger.FileLogger.AppendLine(LogLevel.Info, "InitializeSetting");

            m_ShareMemoryManager.Open();
            m_lvShareMemory.Items.Clear();

            foreach (VirtualMemory sh in m_ShareMemoryManager.ListShardMemory.Values)
                m_lvShareMemory.Items.Add(new ListViewItem(new string[] { string.Empty, sh.MemoryName, sh.MemorySize.ToString() }));

            InitializeShareTrx();
            m_Configuration = new Configuration(null, this);
            m_Configuration.EventFdcDataChanged += new Configuration.FdcEventHandler(OnFdcDataChange);
            InitializeDataGridView();
            InitializeThread();
            KeyPreview = true;
            this.MinimizeBox = true;

            string sVersion = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
            //this.Text = string.Format("{0} ver.{1}", this.Text, sVersion);

            //m_WarningForm = new WarningForm();
            DataGridViewCrossThread(m_DgvSumView);

            InitializeSettingUI();
        }

        /// <summary>
        /// 
        /// </summary>
        private void InitializeMembers()
        {
            CheckForIllegalCrossThreadCalls = false;
            m_listOffProcess = new List<string>();
            m_hashLocalNameToMd = new Dictionary<string, List<MonitoringData>>();
            m_hashProcessToVersion = new Dictionary<string, List<string>>();
            m_hashLocalNoToLocalName = new Dictionary<string, string>();
            m_hashProcessDatas = new Dictionary<string, string[]>();
            _lastestVersionData = new Dictionary<string, string[]>();
            m_Object = new object();
            m_hashServer = new Dictionary<string, int>();
            InitializeComponent();
            m_ShareMemoryManager = new ShareMemoryManager(m_LogManager);
            m_SeverManager = new SeverManager();
            m_SeverManager.Initialize(m_ShareMemoryManager, m_LogManager);
            m_bAutoStart = true;

            //직관 Monitoring Local별 배경색
            m_listColor = new List<Color>()
            {
                Color.White,
                Color.WhiteSmoke,
                Color.Snow,
                Color.Violet,
                Color.SkyBlue
            };

            m_cbScanTime.SelectedIndex = 0;
        }
        /// <summary>
        /// 종료버튼 비활성화
        /// </summary>
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams myCp = base.CreateParams;
                myCp.ClassStyle = myCp.ClassStyle | Constants.CP_NOCLOSE_BUTTON;
                return myCp;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        private void InitializeSetting()
        {
            try
            {
                string sSettingFilePath = Path.Combine(Application.StartupPath, "_ConfigFiles", Constants.DEF_SERVER_TEXT, Constants.DEF_SETTING_TEXT, "Setting.xml");

                if (!File.Exists(sSettingFilePath))
                {
                    MessageBox.Show("Setting Xml 파일이 존재 하지 않습니다.\n시스템을 종료 합니다.");
                    Environment.Exit(0);
                }

                XmlDataReader xmlreader = new XmlDataReader(sSettingFilePath, false);
                string sHaederKey = "ProcessWatcher/";


                //Defult Setting
                List<string[]> listSetting = xmlreader.XmlReadList(sHaederKey + "Settings", Enum.GetNames(typeof(eServerSettingKeys)));
                m_bProcessAutoStart = listSetting[0][0] == "1" ? true : false;
                m_bWindowsRestart = listSetting[0][1] == "1" ? true : false;
                m_btnAutoProcessState.Text = m_bProcessAutoStart ? Constants.DEF_ENABLE_TEXT : Constants.DEF_DISABLE_TEXT;
                m_lbProcessAutoState.Text = m_bProcessAutoStart ? Constants.DEF_ENABLE_TEXT : Constants.DEF_DISABLE_TEXT;
                m_sLogDriver = listSetting[0][2];
                m_iLogDays = string.IsNullOrEmpty(listSetting[0][3]) ? 30 : Convert.ToInt32(listSetting[0][3]);
                m_sServerName = string.IsNullOrEmpty(listSetting[0][4]) ? "Main" : listSetting[0][4];

                _lastestVersionData = new Dictionary<string, string[]>();
                _lastestVersionData = xmlreader.XmlReadDictionary(sHaederKey + "Lastest", Enum.GetNames(typeof(eLastestVersion)));
                _versionData = new List<LastestVersionData>();
                foreach (string[] sArrayValue in _lastestVersionData.Values)
                {
                    string sProcessName = sArrayValue[0];
                    string sVersion = sArrayValue[1];

                    LastestVersionData data = new LastestVersionData()
                    {
                        ProcessName = sProcessName,
                        Version = sVersion
                    };

                    _versionData.Add(data);
                }

                //Server Process Setting
                m_hashProcessDatas = new Dictionary<string, string[]>();
                m_hashProcessDatas = xmlreader.XmlReadDictionary(sHaederKey + "Pros", Enum.GetNames(typeof(eProcessKeys)));

                List<MonitoringData> listmd = new List<MonitoringData>();
                foreach (string[] sArrayValue in m_hashProcessDatas.Values)
                {
                    string sProcessName = sArrayValue[0];
                    string sProcessPath = sArrayValue[1];
                    int iProcessCommand = string.IsNullOrEmpty(sArrayValue[2]) ? 1 : Convert.ToInt32(sArrayValue[2]);

                    MonitoringData md = new MonitoringData()
                    {
                        ProcessName = sProcessName,
                        ProcessPath = sProcessPath,
                        ProcessCommand = iProcessCommand,
                        LocalName = m_sServerName
                    };

                    string[] sArrayProcessPath = sProcessPath.Split('\\');
                    string[] sArrayProcessPathCheck = sArrayProcessPath.Last().Split('.');

                    string sFileName = sArrayProcessPath.Last();
                    sFileName = sFileName.Substring(0, sFileName.Length - (sArrayProcessPathCheck.Last().Length + 1));

                    if (!File.Exists(sProcessPath))
                    {
                        DialogResult dr = MessageBox.Show(string.Format("[{0}] {1}", sProcessName, Constants.DEF_NOT_FILE_PATH), Constants.DEF_NOTIFICATION_MESSAGE, MessageBoxButtons.YesNo, MessageBoxIcon.Information);

                        if (dr != DialogResult.Yes)
                            Environment.Exit(0);
                    }

                    if (sProcessName.ToLower() != sFileName.ToLower())
                    {
                        DialogResult dr = MessageBox.Show(string.Format("[{0}] Process Name과 파일명이 상이 합니다.\n정상 작동되지 않습니다.\n계속 진행 하겠습니까?", sProcessName), Constants.DEF_NOTIFICATION_MESSAGE, MessageBoxButtons.YesNo, MessageBoxIcon.Information);

                        if (dr != DialogResult.Yes)
                            Environment.Exit(0);
                    }
                    listmd.Add(md);
                }

                m_hashLocalNameToMd = new Dictionary<string, List<MonitoringData>>();
                m_hashLocalNoToLocalName = new Dictionary<string, string>();

                m_hashLocalNameToMd.Add(Constants.DEF_SERVER_LOCAL, listmd);
                m_hashLocalNoToLocalName.Add(Constants.DEF_SERVER_LOCAL, m_sServerName);
                //ShareMemory Setting

                Dictionary<string, string[]> hashShareMemory = xmlreader.XmlReadDictionary(sHaederKey + "Memorys", Enum.GetNames(typeof(eServerShareMemoryKeys)));
                m_ShareMemoryManager.ListShardMemory = new SortedList<string, VirtualMemory>();
                if (m_ShareMem != null)
                    m_ShareMem.Close();
                foreach (string[] sShareValue in hashShareMemory.Values)
                {
                    string sName = sShareValue[0];
                    int iSize = string.IsNullOrEmpty(sShareValue[1]) ? 6000 : Convert.ToInt32(sShareValue[1]);

                    m_ShareMemoryManager.ListShardMemory.Add(sName, new VirtualMemory(sName, iSize));

                    m_ShareMem = m_ShareMemoryManager.ListShardMemory[sName];
                    m_ShareMem.Open();
                }

                //Client IP Setting
                _hashClientProcess = new Dictionary<string, string[]>();
                _hashClientProcess = xmlreader.XmlReadDictionary(sHaederKey + "Clients", Enum.GetNames(typeof(eServerClientProcessKeys)));

                int iCount = 2;
                foreach (string[] sArrayValue in _hashClientProcess.Values)
                {
                    List<MonitoringData> listMD = new List<MonitoringData>();
                    for (int i = 0; i < 5; i++)
                    {
                        if (string.IsNullOrEmpty(sArrayValue[i + 2]))
                            continue;

                        MonitoringData md = new MonitoringData()
                        {
                            LocalName = sArrayValue[0],
                            ProcessName = sArrayValue[i + 2]
                        };

                        listMD.Add(md);
                    }
                    string sLocal = string.Format("L{0}", iCount);
                    m_hashLocalNameToMd.Add(sLocal, listMD);
                    m_hashLocalNoToLocalName.Add(sLocal, sArrayValue[0]);
                    iCount++;
                }

                m_btnStart.Enabled = true;
                m_btnStop.Enabled = false;
            }
            catch (Exception ex)
            {

                MessageBox.Show("Setting Xml 파일 잘 못 기입된 값이 있습니다.\n시스템을 종료 합니다.");
                LogManager.ErrorWriteLog(ex.ToString());
                Environment.Exit(0);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        private void InitializeDataGridView()
        {
            try
            {
                //m_DgvSumView Init
                for (int i = 0; i < m_DgvSumView.ColumnCount; i++)
                    m_DgvSumView.Columns[i].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;

            }
            catch (Exception ex)
            {
                m_LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        /// <summary>
        /// 
        /// </summary>
        private void InitializeThread()
        {
            m_queueShareDataBunchData = new Queue<PlcAddr>();

            m_threadUsage = new Thread(new ThreadStart(OnGetUsage))
            { IsBackground = true };

            m_threadShareDataChanged = new Thread(new ThreadStart(OnShareDataChanged))
            { IsBackground = true };

            m_threadShareData = new Thread(new ThreadStart(OnShareMessageProcessorThread))
            { IsBackground = true };

            MainAlive = new Thread(new ThreadStart(OnCheckMainAlive))
            { IsBackground = true };

            MainState = new Thread(new ThreadStart(OnMainState))
            { IsBackground = true };

            PwAlive = new Thread(new ThreadStart(OnPwAlive))
            { IsBackground = true };

            m_threadUsage.Start();
            m_threadShareData.Start();
            m_threadShareDataChanged.Start();
            MainAlive.Start();
            MainState.Start();
            PwAlive.Start();

            //Timer
            m_TimerAutoStart.Start();
            m_TimerProcessAutoStart.Start();
        }

        private void OnPwAlive()
        {
            bool state = false;

            while (true)
            {
                SharedMem.Knd.File.GetBooleanValue(ref state, "PW_PROGRAM_ALIVE_BIT");
                SharedMem.Knd.File.SetBooleanValue("PW_PROGRAM_ALIVE_BIT", !state);

                Thread.Sleep(500);
            }
        }

        private void OnMainState()
        {
            bool state = false;

            while (true)
            {
                SharedMem.Knd.File.GetBooleanValue(ref state, "ICM_ALARM_PW_REPLY");
                if (_mainAlarmReply != state)
                {
                    _mainAlarmReply = state;
                }

                Thread.Sleep(10);
            }
        }

        private void OnCheckMainAlive()
        {
            bool oldState = false;
            DateTime startTime = DateTime.Now;

            while (true)
            {
                bool state = false;
                SharedMem.Knd.File.GetBooleanValue(ref state, "ICM_PROGRAM_ALIVE_BIT");
                if (oldState != state)
                {
                    startTime = DateTime.Now;
                    oldState = state;
                    _mainAliveState = true;
                }
                else
                {
                    TimeSpan diff = DateTime.Now - startTime;

                    if (diff.TotalSeconds > 5)
                    {
                        _mainAliveState = false;
                    }
                }

                Thread.Sleep(100);
            }
        }

        private void SetAlarmToMain(object value)
        {
            if (!_mainAliveState) return;

            SharedMem.Knd.File.SetBytesValue("PW_ALARM_LEVEL", 0);
            SharedMem.Knd.File.SetBytesValue("PW_ALARM_CODE", (byte)value);

            SharedMem.Knd.File.SetBooleanValue("PW_ALARM_REPORT", true);

            DateTime start = DateTime.Now;
            while ((DateTime.Now - start).TotalSeconds < 3)
            {
                if (_mainAlarmReply)
                    break;
            }

            SharedMem.Knd.File.SetBooleanValue("PW_ALARM_REPORT", false);

            return;
        }

        /// <summary>
        /// 
        /// </summary>
        private void InitializeLogManager()
        {
            m_sLogDriver = Application.StartupPath.Split(':')[0];
            string sLogPath = m_sLogDriver;// Constants.DEF_LOG_FILE_PATH.Replace("D:", string.Format("{0}:", m_sLogDriver));

            m_LogManager = new LogManager(sLogPath, m_iLogDays);

            m_LogManager.Initialize(Constants.DEF_SERVER_TEXT, string.Empty, Constants.DEF_LOG_TEXT, eLogTimeType.Hour);
            m_LogManager.Initialize(Constants.DEF_SHARE_MEMORY_CLIENT, string.Empty, Constants.DEF_LOG_TEXT, eLogTimeType.Hour);
            m_LogManager.Initialize(Constants.DEF_PW_BTN, string.Empty, Constants.DEF_LOG_TEXT, eLogTimeType.Hour);
        }
        /// <summary>
        /// 
        /// </summary>
        private void InitializeShareTrx()
        {
            try
            {
                m_ShareTrx = new ShareTrx(m_ShareMem);
                string sPath = System.Windows.Forms.Application.StartupPath;

                m_ShareTrx.InitializeDataBit(sPath + "\\" + Constants.DEF_BIT_FILE_PATH.Replace(Constants.DEF_CLIENT_TEXT, Constants.DEF_SERVER_TEXT));
                m_ShareTrx.InitializeDataWord(sPath + "\\" + Constants.DEF_WORD_FILE_PATH.Replace(Constants.DEF_CLIENT_TEXT, Constants.DEF_SERVER_TEXT));
                m_ShareTrx.InitializeDataStruct(sPath + "\\" + Constants.DEF_STRUCT_FILE_PATH.Replace(Constants.DEF_CLIENT_TEXT, Constants.DEF_SERVER_TEXT));
                m_ShareTrx.InitializeDataTrx(sPath + "\\" + Constants.DEF_TRX_FILE_PATH.Replace(Constants.DEF_CLIENT_TEXT, Constants.DEF_SERVER_TEXT));

            }
            catch (Exception ex)
            {
                m_LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        #endregion

        #region Class private
        /// <summary>
        /// 
        /// </summary>
        private void Start()
        {
            if (m_btnStart.Enabled == true)
            {
                try
                {
                    m_ShareMemoryManager.Open();
                    m_SeverManager.Port = 7160;
                    m_SeverManager.Start();

                    m_btnStart.Enabled = false;
                    m_btnStop.Enabled = true;

                    m_lbShareMemoryState.Text = "True";
                }
                catch (Exception ex)
                {
                    LogManager.ErrorWriteLog(ex.ToString());
                }
            }
        }
        /// <summary>
        /// 
        /// </summary>
        private void StartupAddRemove(string sProgramName, string sExecutablePath, bool bDelete = false)
        {
            Common.OnStartUpAddRemove(sProgramName, sExecutablePath, bDelete);
        }
        #endregion

        #region Class Event
        /// <summary>
        /// 
        /// </summary>
        private void OnStart(object sender, EventArgs e)
        {
            if (!m_btnStart.Enabled)
                MessageBox.Show("이미 ShareMemory Open 상태 입니다.");
            else
            {
                Start();

                if (!this.Visible)
                    MessageBox.Show("ShareMemory Open");
            }
        }
        /// <summary>
        /// 
        /// </summary>
        private void OnStop(object sender, EventArgs e)
        {
            if (!m_btnStart.Enabled)
            {
                m_SeverManager.Stop();
                m_ShareMemoryManager.Close();

                m_btnStart.Enabled = true;
                m_btnStop.Enabled = false;
                m_lbShareMemoryState.Text = "False";

                if (!this.Visible)
                    MessageBox.Show("ShareMemory Close");
            }
            else
                MessageBox.Show("이미 ShareMemory Close 상태 입니다.");
        }
        /// <summary>
        /// 
        /// </summary>
        public void SeverManager_OnClosed(object sender, EventArgs e)
        {
            if (sender is ShareMemoryServerSession ServerSessionClosed)
            {
                string sLog = string.Format("CLOSED IP = {0}, PORT = {1} ",
                    ServerSessionClosed.RemoteEndPoint.Address.ToString(), ServerSessionClosed.RemoteEndPoint.Port.ToString());
                m_LogManager.WriteLog(Constants.DEF_SHARE_MEMORY_CLIENT, sLog);
                m_lvLog.Items.Add(sLog);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public void SeverManager_OnAccepted(object sender, EventArgs e)
        {
            if (sender is ShareMemoryServerSession ServerSessionAccepted)
            {
                string sLog = string.Format("OPEN IP = {0}, PORT = {1} ",
                    ServerSessionAccepted.RemoteEndPoint.Address.ToString(), ServerSessionAccepted.RemoteEndPoint.Port.ToString());
                m_LogManager.WriteLog(Constants.DEF_SHARE_MEMORY_CLIENT, sLog);
                m_lvLog.Items.Add(sLog);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        private void OnStartUpAdd(object sender, EventArgs e)
        {
            StartupAddRemove(Constants.DEF_SHARE_MEMORY_CLIENT, Application.ExecutablePath);
        }
        /// <summary>
        /// 
        /// </summary>
        private void OnStartUpRemove(object sender, EventArgs e)
        {
            StartupAddRemove(Constants.DEF_SHARE_MEMORY_CLIENT, Application.ExecutablePath, true);
        }
        /// <summary>
        /// 
        /// </summary>
        private void OnHide(object sender, EventArgs e)
        {
            if (m_btnStart.Enabled)
            {
                DialogResult dr = MessageBox.Show(Constants.DEF_SHARE_MEMORY_HIDE_MESSAGE, Constants.DEF_NOTIFICATION_MESSAGE, MessageBoxButtons.OKCancel, MessageBoxIcon.Information);

                if (dr == DialogResult.OK)
                    this.Hide();
            }
            else
                this.Hide();
        }
        /// <summary>
        /// 
        /// </summary>
        private void OnFormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                string sMessage = m_btnStart.Enabled ? Constants.DEF_SHRARE_MEMORY_CLOSE_MESSAGE : Constants.DEF_SHARE_MEMORY_START_CLOSE_MESSAGE;

                DialogResult dr = MessageBox.Show(sMessage, Constants.DEF_NOTIFICATION_MESSAGE, MessageBoxButtons.OKCancel, MessageBoxIcon.Information);

                switch (dr)
                {
                    case DialogResult.OK:
                        m_LogManager.WriteLog(Constants.DEF_PW_BTN, "P.W Program Exit");
                        Thread.Sleep(1000);
                        Logger.FileLogger.Dispose();
                        m_SeverManager.Stop();
                        break;
                    default:
                        if (e != null)
                            e.Cancel = true;
                        break;
                }
            }
            catch (Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());

                Environment.Exit(0);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        private void OnOpenClick(object sender, MouseEventArgs e)
        {
            this.Visible = true;
            this.ShowInTaskbar = true;
            this.Show();
            this.WindowState = FormWindowState.Normal;
        }
        /// <summary>
        /// 
        /// </summary>
        private void OnMouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button != MouseButtons.Right)
                return;
            //JHKIM 듀얼 모니터시 문제점 있음

            contextMenuStrip1.Show(MousePosition.X, MousePosition.Y);
        }
        /// <summary>
        /// 
        /// </summary>
        private void OnContextOpen(object sender, EventArgs e)
        {
            OnOpenClick(null, null);
        }
        /// <summary>
        /// 
        /// </summary>
        private void OnContextClose(object sender, EventArgs e)
        {
            string sMessage = m_btnStart.Enabled ? Constants.DEF_SHRARE_MEMORY_CLOSE_MESSAGE : Constants.DEF_SHARE_MEMORY_START_CLOSE_MESSAGE;

            DialogResult dr = MessageBox.Show(sMessage, Constants.DEF_NOTIFICATION_MESSAGE, MessageBoxButtons.OKCancel, MessageBoxIcon.Information);

            if (dr == DialogResult.OK)
            {
                m_LogManager.WriteLog(Constants.DEF_PW_BTN, "P.W Program Exit");
                Thread.Sleep(1000);
                Logger.FileLogger.Dispose();
                m_SeverManager.Stop();

                Environment.Exit(0);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        private void OnAutoStartChange(object sender, EventArgs e)
        {
            switch (m_bAutoStart)
            {
                case true:
                    m_bAutoStart = false;
                    m_lbProcessAutoState.Text = Constants.DEF_AUTO_START_DISABLE;
                    m_btnAutoStart.Text = Constants.DEF_AUTO_START_DISABLE;
                    break;
                case false:
                    m_bAutoStart = true;
                    m_lbProcessAutoState.Text = Constants.DEF_AUTO_START_ENABLE;
                    m_btnAutoStart.Text = Constants.DEF_AUTO_START_ENABLE;
                    break;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        private void OnAutoStartEnable(object sender, EventArgs e)
        {
            switch (m_btnAutoStart.Text)
            {
                case Constants.DEF_AUTO_START_ENABLE:
                    MessageBox.Show("이미 AutoStart 실행 중입니다.");
                    break;
                default:
                    m_btnAutoStart.Text = Constants.DEF_AUTO_START_ENABLE;
                    m_lbProcessAutoState.Text = Constants.DEF_AUTO_START_ENABLE;
                    m_bAutoStart = true;

                    if (!this.Visible)
                        MessageBox.Show(Constants.DEF_AUTO_START_ENABLE);
                    break;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        private void OnAutoStartDisable(object sender, EventArgs e)
        {
            switch (m_btnStart.Text)
            {
                case Constants.DEF_AUTO_START_DISABLE:
                    MessageBox.Show("이미 AutoStart 해제 중입니다.");
                    break;
                default:
                    m_btnAutoStart.Text = Constants.DEF_AUTO_START_DISABLE;
                    m_lbProcessAutoState.Text = Constants.DEF_AUTO_START_DISABLE;
                    m_bAutoStart = false;

                    if (!this.Visible)
                        MessageBox.Show(Constants.DEF_AUTO_START_DISABLE);
                    break;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        private void OnComboSelectChange(object sender, EventArgs e)
        {
            m_iCpuRamUsageScanTime = (m_cbScanTime.SelectedIndex + 1) * 1000;
        }
        /// <summary>
        /// 
        /// </summary>
        private void OnFdcDataChange(Trx trx, bool bChange = false)
        {
            try
            {
                List<MonitoringData> listmd = ConvertTrxToMonitoring(trx);
                bool bCheck = false;
                string[] sLocals = trx.sTriggerId.Split('_');
                string sLocalName = listmd[0].LocalName;

                if (m_hashLocalNoToLocalName.ContainsKey(sLocals[0]))
                {
                    if (m_hashLocalNoToLocalName[sLocals[0]] != listmd[0].LocalName)
                    {
                        m_hashLocalNoToLocalName[sLocals[0]] = listmd[0].LocalName;
                        bCheck = true;
                    }
                }

                if (m_hashLocalNameToMd.ContainsKey(sLocals[0]))
                    m_hashLocalNameToMd[sLocals[0]] = listmd;
                else
                    m_hashLocalNameToMd.Add(sLocals[0], listmd);

                DataGridViewCrossThread(m_DgvSumView, bCheck);
            }
            catch (Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        /// <summary>
        /// 
        /// </summary>
        private List<MonitoringData> ConvertTrxToMonitoring(Trx trx)
        {
            List<MonitoringData> list = new List<MonitoringData>();
            List<PlcAddr> listPlc = m_ShareTrx.hashShareStruct[trx.ReadItem[0]];

            int iRowCount = 0;
            int iCount = m_ShareMem.GetByte(listPlc[iRowCount++]);

            for (int i = 0; i < iCount; i++)
            {
                MonitoringData md = new MonitoringData();

                if (i == 0)
                {
                    md.LocalName = m_ShareMem.GetAscii(listPlc[iRowCount++]);
                    md.TotalCPU = m_ShareMem.GetAscii(listPlc[iRowCount++]);
                    md.TotalRAM = m_ShareMem.GetAscii(listPlc[iRowCount++]);
                    md.AutoStartMode = m_ShareMem.GetByte(listPlc[iRowCount++]);
                }

                md.ProcessState = m_ShareMem.GetAscii(listPlc[iRowCount++]);
                md.ProcessName = md.ProcessState.Split(':')[0].Trim();
                md.ProcessCPU = m_ShareMem.GetAscii(listPlc[iRowCount++]);
                md.ProcessRAM = m_ShareMem.GetAscii(listPlc[iRowCount++]);
                md.FileVersion = m_ShareMem.GetAscii(listPlc[iRowCount++]);
                md.ProductVersion = m_ShareMem.GetAscii(listPlc[iRowCount++]);
                md.ProcessCommand = m_ShareMem.GetByte(listPlc[iRowCount++]);
                md.ScanTime = DateTime.Now.ToString();
                list.Add(md);
            }

            return list;
        }
        /// <summary>
        /// 
        /// </summary>
        private void OnProcessAutoStart(object sender, EventArgs e)
        {
            switch (m_bProcessAutoStart)
            {
                case true:
                    m_btnAutoProcessState.Text = Constants.DEF_DISABLE_TEXT;
                    m_lbProcessAutoState.Text = Constants.DEF_DISABLE_TEXT;
                    m_bProcessAutoStart = false;
                    break;
                default:
                    m_btnAutoProcessState.Text = Constants.DEF_ENABLE_TEXT;
                    m_lbProcessAutoState.Text = Constants.DEF_ENABLE_TEXT;
                    m_bProcessAutoStart = true;
                    break;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        private void OnMainFormKeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F1)
                Common.OnHelpFileCheck();
        }
        /// <summary>
        /// 
        /// </summary>
        private void OnCellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.RowIndex != -1 && (e.ColumnIndex == 9 || e.ColumnIndex == 10))
                {
                    if (m_DgvSumView[e.ColumnIndex, e.RowIndex].Tag == null ||
                        m_DgvSumView[e.ColumnIndex, e.RowIndex].EditType != null)
                        return;

                    string sTag = m_DgvSumView[e.ColumnIndex, e.RowIndex].Tag.ToString();

                    if (File.Exists(sTag))
                    {
                        string sProcess = sTag;

                        if (e.ColumnIndex == 9)
                        {
                            string[] sArrayProcess = sTag.Split('\\');
                            sProcess = sArrayProcess[sArrayProcess.Length - 1].Split('.')[0];
                        }
                        Process[] processlist = Process.GetProcessesByName(sProcess);

                        if (e.ColumnIndex == 10)
                        {
                            m_LogManager.WriteLog(Constants.DEF_PW_BTN, string.Format("{0} Kill Button Click", m_DgvSumView[1, e.RowIndex].Value.ToString()));
                            if (processlist.Length == 0)
                                MessageBox.Show("이미 종료 되었습니다.");
                            else
                            {
                                processlist[0].Kill();
                                m_LogManager.WriteLog(Constants.DEF_PW_BTN, string.Format("{0} Kill", m_DgvSumView[1, e.RowIndex].Value.ToString()));
                            }
                        }
                        else
                        {
                            m_LogManager.WriteLog(Constants.DEF_PW_BTN, string.Format("{0} Start Button Click", m_DgvSumView[1, e.RowIndex].Value.ToString()));
                            if (processlist.Length != 0)
                                MessageBox.Show("이미 실행 중 입니다.");
                            else
                            {
                                Process.Start(sTag);
                                m_LogManager.WriteLog(Constants.DEF_PW_BTN, string.Format("{0} Start", m_DgvSumView[1, e.RowIndex].Value.ToString()));
                            }
                        }
                    }
                    else
                    {
                        if (sTag.Contains(Constants.DEF_KILL_TEXT))
                        {
                            DialogResult dr = MessageBox.Show(Constants.DEF_PROCESS_KILL_COMMAND_MESSAGE, Constants.DEF_NOTIFICATION_MESSAGE, MessageBoxButtons.OKCancel, MessageBoxIcon.Information);

                            if (dr != DialogResult.OK)
                                return;

                            m_LogManager.WriteLog(Constants.DEF_PW_BTN, string.Format("{0} Kill Button Click", m_DgvSumView[1, e.RowIndex].Value.ToString()));
                        }
                        else if (sTag.Contains(Constants.DEF_RESTART_TEXT.ToUpper()))
                        {
                            DialogResult dr = MessageBox.Show(Constants.DEF_WINDOWS_RESTART_COMMAND_MESSAGE, Constants.DEF_NOTIFICATION_MESSAGE, MessageBoxButtons.OKCancel, MessageBoxIcon.Information);

                            if (dr != DialogResult.OK)
                                return;
                            m_LogManager.WriteLog(Constants.DEF_PW_BTN, string.Format("{0} Start Button Click", m_DgvSumView[1, e.RowIndex].Value.ToString()));
                        }

                        if (m_ShareTrx.hashShareKeyToShareData.ContainsKey(sTag))
                            m_ShareMem.SetBit(m_ShareTrx.hashShareKeyToShareData[sTag]);
                    }

                    //switch (sTag.Split('_').Length)
                    //{
                    //    case 1:
                    //        string sProcess = sTag;

                    //        if (e.ColumnIndex == 9)
                    //        {
                    //            string[] sArrayProcess = sTag.Split('\\');
                    //            sProcess = sArrayProcess[sArrayProcess.Length - 1].Split('.')[0];
                    //        }
                    //        Process[] processlist = Process.GetProcessesByName(sProcess);

                    //        if (e.ColumnIndex == 10)
                    //        {
                    //            m_LogManager.WriteLog(Constants.DEF_PW_BTN, string.Format("{0} Kill Button Click", m_DgvSumView[1, e.RowIndex].Value.ToString()));
                    //            if (processlist.Length == 0)
                    //                MessageBox.Show("이미 종료 되었습니다.");
                    //            else
                    //            {
                    //                processlist[0].Kill();
                    //                m_LogManager.WriteLog(Constants.DEF_PW_BTN, string.Format("{0} Kill", m_DgvSumView[1, e.RowIndex].Value.ToString()));
                    //            }
                    //        }
                    //        else
                    //        {
                    //            m_LogManager.WriteLog(Constants.DEF_PW_BTN, string.Format("{0} Start Button Click", m_DgvSumView[1, e.RowIndex].Value.ToString()));
                    //            if (processlist.Length != 0)
                    //                MessageBox.Show("이미 실행 중 입니다.");
                    //            else
                    //            {
                    //                Process.Start(sTag);
                    //                m_LogManager.WriteLog(Constants.DEF_PW_BTN, string.Format("{0} Start", m_DgvSumView[1, e.RowIndex].Value.ToString()));
                    //            }
                    //        }
                    //        break;
                    //    default:
                    //        if (sTag.Contains(Constants.DEF_KILL_TEXT))
                    //        {
                    //            DialogResult dr = MessageBox.Show(Constants.DEF_PROCESS_KILL_COMMAND_MESSAGE, Constants.DEF_NOTIFICATION_MESSAGE, MessageBoxButtons.OKCancel, MessageBoxIcon.Information);

                    //            if (dr != DialogResult.OK)
                    //                return;

                    //            m_LogManager.WriteLog(Constants.DEF_PW_BTN, string.Format("{0} Kill Button Click", m_DgvSumView[1, e.RowIndex].Value.ToString()));
                    //        }
                    //        else if (sTag.Contains(Constants.DEF_RESTART_TEXT.ToUpper()))
                    //        {
                    //            DialogResult dr = MessageBox.Show(Constants.DEF_WINDOWS_RESTART_COMMAND_MESSAGE, Constants.DEF_NOTIFICATION_MESSAGE, MessageBoxButtons.OKCancel, MessageBoxIcon.Information);

                    //            if (dr != DialogResult.OK)
                    //                return;
                    //            m_LogManager.WriteLog(Constants.DEF_PW_BTN, string.Format("{0} Start Button Click", m_DgvSumView[1, e.RowIndex].Value.ToString()));
                    //        }

                    //        if (m_ShareTrx.hashShareKeyToShareData.ContainsKey(sTag))
                    //            m_ShareMem.SetBit(m_ShareTrx.hashShareKeyToShareData[sTag]);
                    //        break;
                    //}
                }
            }
            catch (Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        /// <summary>
        /// 
        /// </summary>
        private void OnDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                if (e.RowIndex == -1 || e.ColumnIndex != 2)
                    return;

                if (m_DgvSumView.Rows[e.RowIndex].Cells[e.ColumnIndex].Tag == null)
                {
                    MessageBox.Show("단독 Process Auto Start Mode는 Program에서 변경 할 수 없습니다.");
                    return;
                }

                string sValue = m_DgvSumView.Rows[e.RowIndex].Cells[e.ColumnIndex].Tag.ToString();

                if (sValue.Contains("COMMAND") && m_ShareTrx.hashShareKeyToShareData.ContainsKey(sValue))
                    m_ShareMem.SetBit(m_ShareTrx.hashShareKeyToShareData[sValue]);
                else
                    OnProcessAutoStart(null, null);
            }
            catch (Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
            }
        }

        private void tbConfigOnlyNumbers_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
            {
                e.Handled = true;
            }

            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
        }
        #endregion

        #region Class timer
        /// <summary>
        /// 
        /// </summary>
        private void TimerAutoStart(object sender, EventArgs e)
        {
            if (!m_bAutoStart)
                return;

            if (m_btnStart.Enabled)
                Start();
        }
        /// <summary>
        /// 
        /// </summary>
        private void TimerProcessAutoStart_Tick(object sender, EventArgs e)
        {
            try
            {
                if (m_listOffProcess.Count != 0)
                {
                    //Process Auto Start Check
                    bool bCheck = false;

                    string[] sArrayPaths = new string[m_listOffProcess.Count];
                    m_listOffProcess.CopyTo(sArrayPaths);

                    foreach (string sPath in sArrayPaths)
                    {
                        var _key = m_hashProcessDatas.FirstOrDefault(x => x.Value[1] == sPath).Key;

                        Mutex mutex = new Mutex(true, _key, out bool bRun);

                        if (bRun && m_btnAutoProcessState.Text != Constants.DEF_DISABLE_TEXT)
                        {
                            if (File.Exists(sPath))
                            {
                                Process.Start(sPath);
                                bCheck = true;
                            }
                        }
                    }

                    if ((m_listOffProcess.Count != 0 && bCheck) || m_btnAutoProcessState.Text == Constants.DEF_DISABLE_TEXT)
                        m_listOffProcess.Clear();
                }


                //이벤트 없는 Lcoal Monitroing View Refresh
                string[] arrayLocals = new string[m_Configuration.ProcessDataToTime.Keys.Count];
                m_Configuration.ProcessDataToTime.Keys.CopyTo(arrayLocals, 0);

                foreach (string sLocal in arrayLocals)
                {
                    if (DateTime.Now.Ticks - m_Configuration.ProcessDataToTime[sLocal].AddSeconds(10).Ticks > 0)
                    {
                        for (int i = 0; i < m_hashLocalNameToMd[sLocal].Count; i++)
                            m_hashLocalNameToMd[sLocal][i].ScanTime = string.Empty;

                        m_Configuration.ProcessDataToTime.Remove(sLocal);
                        DataGridViewCrossThread(m_DgvSumView);
                    }
                }
            }
            catch (Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        #endregion

        #region Class Thread
        /// <summary>
        /// 
        /// </summary>
        private delegate void ReloadClientDataGridView(DataGridView dgv, bool bChangeCheck);
        /// <summary>
        /// 
        /// </summary>
        private void DataGridViewCrossThread(DataGridView dgv, bool bChangeCheck = false)
        {
            try
            {
                if (dgv.InvokeRequired)
                {
                    ReloadClientDataGridView reload = new ReloadClientDataGridView(DataGridViewCrossThread);
                    this.Invoke(reload, new object[] { dgv, bChangeCheck });
                }
                else
                {
                    if (bChangeCheck)
                        dgv.Rows.Clear();

                    int iCount = 0;

                    foreach (string sLocalName in m_hashLocalNameToMd.Keys)
                        iCount += m_hashLocalNameToMd[sLocalName].Count + 1;

                    int iRowIndex = 0;

                    string[] sArrayKeys = new string[m_hashLocalNameToMd.Keys.Count];
                    m_hashLocalNameToMd.Keys.CopyTo(sArrayKeys, 0);

                    if (iCount != dgv.Rows.Count)
                    {
                        dgv.Rows.Clear();

                        foreach (string sLocalName in sArrayKeys)
                        {
                            string sPcName = m_hashLocalNameToMd[sLocalName][0].LocalName;
                            dgv.Rows.Add(sPcName);
                            dgv.Rows[iRowIndex].Cells[1].Value = sPcName;
                            dgv.Rows[iRowIndex].Cells[2].Style.BackColor = Color.Red;
                            dgv.Rows[iRowIndex].Cells[3].Value = m_bAutoStart ? "True" : "False";

                            var cell = (DataGridViewExTextBoxCell)m_DgvSumView[0, iRowIndex];

                            if (m_hashLocalNameToMd[sLocalName].Count != 0)
                                dgv.Rows.Add(m_hashLocalNameToMd[sLocalName].Count);

                            cell.RowSpan = m_hashLocalNameToMd[sLocalName].Count + 1;

                            iRowIndex++;
                            iRowIndex += m_hashLocalNameToMd[sLocalName].Count;

                        }
                    }
                    else
                    {
                        int iLocalNo = 1;
                        foreach (string sLocalName in sArrayKeys)
                        {
                            string sLocalNo = string.Format("L{0}", iLocalNo);

                            bool bCheck = false;

                            if (string.IsNullOrEmpty(m_hashLocalNameToMd[sLocalName][0].ScanTime))
                            {
                                var cell = (DataGridViewExTextBoxCell)m_DgvSumView[5, iRowIndex];
                                cell.ColumnSpan = 4;
                                m_DgvSumView.Rows[iRowIndex].DefaultCellStyle.BackColor = Color.LightGray;

                                dgv.Rows[iRowIndex].Cells[2].Style.BackColor = Color.Red;
                                dgv.Rows[iRowIndex].Cells[3].Value = string.Empty;
                                dgv.Rows[iRowIndex].Cells[3].Tag = string.Empty;
                            }

                            int iCommandIndex = 1;
                            foreach (MonitoringData md in m_hashLocalNameToMd[sLocalName])
                            {
                                if (!bCheck)
                                {
                                    switch (string.IsNullOrEmpty(md.ScanTime))
                                    {
                                        case true:
                                            dgv.Rows[iRowIndex].Cells[4].Value = string.Empty;
                                            dgv.Rows[iRowIndex].Cells[5].Value = string.Empty;
                                            dgv.Rows[iRowIndex].Cells[6].Value = string.Empty;
                                            dgv.Rows[iRowIndex].Cells[2].Style.BackColor = Color.Red;

                                            if (dgv.Rows[iRowIndex].Cells[6].EditType == null)
                                            {
                                                DataGridViewExTextBoxCell cellRestart = new DataGridViewExTextBoxCell();
                                                dgv.Rows[iRowIndex].Cells[5] = cellRestart;
                                            }
                                            iRowIndex++;
                                            break;
                                        case false:
                                            dgv.Rows[iRowIndex].Cells[4].Value = md.TotalCPU;
                                            dgv.Rows[iRowIndex].Cells[5].Value = md.TotalRAM;
                                            dgv.Rows[iRowIndex].Cells[2].Style.BackColor = Color.Green;

                                            switch (m_bWindowsRestart)
                                            {
                                                case true:
                                                    if (m_DgvSumView[6, iRowIndex].Value == null)
                                                    {
                                                        var TextCell = (DataGridViewExTextBoxCell)m_DgvSumView[5, iRowIndex];
                                                        if (sLocalNo == Constants.DEF_SERVER_LOCAL)
                                                        {
                                                            TextCell.ColumnSpan = 5;
                                                            m_DgvSumView.Rows[iRowIndex].DefaultCellStyle.BackColor = Color.LightGray;
                                                        }
                                                        else
                                                        {
                                                            m_DgvSumView[6, iRowIndex].Value = Constants.DEF_WINDWOS_RESTART_TEXT;

                                                            TextCell.ColumnSpan = 3;
                                                            m_DgvSumView.Rows[iRowIndex].DefaultCellStyle.BackColor = Color.LightGray;
                                                            DataGridViewButtonCell CellRestart = new DataGridViewButtonCell();
                                                            dgv.Rows[iRowIndex].Cells[10] = CellRestart;
                                                            dgv.Rows[iRowIndex].Cells[10].Tag = string.Format("L1_B_WINDOWS_RESTART_{0}_COMMAND", sLocalNo);
                                                        }
                                                    }
                                                    break;
                                                case false:
                                                    var cell = (DataGridViewExTextBoxCell)m_DgvSumView[5, iRowIndex];
                                                    m_DgvSumView.Rows[iRowIndex].DefaultCellStyle.BackColor = Color.LightGray;
                                                    cell.ColumnSpan = 5;
                                                    break;
                                            }

                                            if (sLocalNo == Constants.DEF_SERVER_LOCAL)
                                            {
                                                dgv.Rows[iRowIndex].Cells[3].Value = m_bProcessAutoStart;
                                                dgv.Rows[iRowIndex].Cells[3].Tag = sLocalNo;
                                            }
                                            else
                                            {
                                                dgv.Rows[iRowIndex].Cells[3].Value = md.AutoStartMode.ToString() == "1" ? "True" : "False";
                                                dgv.Rows[iRowIndex].Cells[3].Tag = string.Format("L1_B_PROCESS_AUTO_START_{0}_CHANGE_COMMAND", sLocalNo);
                                            }

                                            iRowIndex++;
                                            break;
                                    }
                                    bCheck = true;
                                }
                                dgv.Rows[iRowIndex].Cells[1].Value = md.ProcessName;

                                switch (string.IsNullOrEmpty(md.ScanTime))
                                {
                                    case true:
                                        dgv.Rows[iRowIndex].Cells[2].Style.BackColor = Color.Red;
                                        dgv.Rows[iRowIndex].Cells[3].Value = string.Empty;
                                        dgv.Rows[iRowIndex].Cells[4].Value = string.Empty;
                                        dgv.Rows[iRowIndex].Cells[5].Value = string.Empty;
                                        dgv.Rows[iRowIndex].Cells[6].Value = string.Empty;
                                        dgv.Rows[iRowIndex].Cells[7].Value = string.Empty;

                                        DataGridViewExTextBoxCell btnText = new DataGridViewExTextBoxCell();

                                        if (dgv.Rows[iRowIndex].Cells[8].EditType == null)
                                            dgv.Rows[iRowIndex].Cells[8] = btnText;
                                        else if (dgv.Rows[iRowIndex].Cells[9].EditType == null)
                                            dgv.Rows[iRowIndex].Cells[9] = btnText;
                                        break;
                                    case false:
                                        string[] sArraySplit = md.ProcessState.Split(':');

                                        bool bProcessState = false;

                                        switch (sArraySplit.Length)
                                        {
                                            case 1: //Server Process
                                                dgv.Rows[iRowIndex].Cells[1].Value = md.ProcessName;
                                                if (!string.IsNullOrEmpty(md.ChildProcessName1))    //Exist Child Process (ex. sq1)
                                                {
                                                    byte iCheck = ChildProcessCheck(md.ProcessName, md.ChildProcessName1, md.ChildProcessName2);

                                                    switch (iCheck)
                                                    {
                                                        case 0:
                                                            dgv.Rows[iRowIndex].Cells[2].Style.BackColor = Color.Green;
                                                            bProcessState = true;
                                                            break;
                                                        case 1:
                                                        case 2:
                                                            dgv.Rows[iRowIndex].Cells[2].Style.BackColor = Color.Red;

                                                            if (md.ProcessName.Equals("Mark IV Esol"))
                                                            {
                                                                //ThreadPool.QueueUserWorkItem(SetAlarmToMain, (byte)ERROR_CODE.SQ1_OFFLINE_STATUS);
                                                            }

                                                            bProcessState = iCheck == 2 ? false : true;
                                                            break;
                                                    }
                                                }
                                                else
                                                {
                                                    //20240215 jkseo, State 색상 변경 Green : Normal / Orange : Version Not Match / Red : Offline
                                                    if (md.ProcessState.Equals("ON"))
                                                    {
                                                        dgv.Rows[iRowIndex].Cells[2].Style.BackColor = Color.Green;

                                                        for (int index = 0; index < _versionData.Count; index++)
                                                        {
                                                            if (md.ProcessName.Equals(_versionData[index].ProcessName))
                                                            {
                                                                if (!md.FileVersion.Equals(_versionData[index].Version))
                                                                {
                                                                    dgv.Rows[iRowIndex].Cells[2].Style.BackColor = Color.Orange;

                                                                    if (md.ProcessName.Equals("EUVSolution"))
                                                                    {
                                                                        ThreadPool.QueueUserWorkItem(SetAlarmToMain, (byte)ERROR_CODE.ICM_VERSION_NOT_MATCH);
                                                                    }
                                                                    else if (md.ProcessName.Equals("ITS"))
                                                                    {
                                                                        ThreadPool.QueueUserWorkItem(SetAlarmToMain, (byte)ERROR_CODE.ITS_VERSION_NOT_MATCH);
                                                                    }
                                                                    else if (md.ProcessName.Equals("MaskImageReconstructor"))
                                                                    {
                                                                        ThreadPool.QueueUserWorkItem(SetAlarmToMain, (byte)ERROR_CODE.MIR_VERSION_NOT_MATCH);
                                                                    }
                                                                    //else if (md.ProcessName.Equals("CIM"))
                                                                    //    SetAlarmToMain((int)ERROR_CODE.CIM_VERSION_NOT_MATCH, 0);
                                                                }

                                                                break;
                                                            }
                                                        }
                                                    }
                                                    else
                                                    {
                                                        dgv.Rows[iRowIndex].Cells[2].Style.BackColor = Color.Red;
                                                        break;
                                                    }

                                                    dgv.Rows[iRowIndex].Cells[2].Style.BackColor = md.ProcessState == "ON" ? Color.Green : Color.Red;
                                                    bProcessState = md.ProcessState == "ON" ? true : false;
                                                }
                                                break;
                                            case 2: //Client Process
                                                dgv.Rows[iRowIndex].Cells[1].Value = sArraySplit[0].Trim();

                                                //20240215 jkseo, State 색상 변경 Green : Normal / Orange : Version Not Match / Red : Offline
                                                if (sArraySplit[1].Trim().Equals("ON"))
                                                {
                                                    dgv.Rows[iRowIndex].Cells[2].Style.BackColor = Color.Green;

                                                    for (int index = 0; index < _versionData.Count; index++)
                                                    {
                                                        if (md.ProcessName.Equals(_versionData[index].ProcessName))
                                                        {
                                                            if (!md.FileVersion.Equals(_versionData[index].Version))
                                                            {
                                                                dgv.Rows[iRowIndex].Cells[2].Style.BackColor = Color.Orange;

                                                                if (md.ProcessName.Equals("EUVO160"))
                                                                {
                                                                    ThreadPool.QueueUserWorkItem(SetAlarmToMain, (byte)ERROR_CODE.SOURCE_CONTROL_VERSION_NOT_MATCH);
                                                                }
                                                                else if (md.ProcessName.Equals("SAM"))
                                                                {
                                                                    ThreadPool.QueueUserWorkItem(SetAlarmToMain, (byte)ERROR_CODE.SOURCE_MONITORING_VERSION_NOT_AMTCH);
                                                                }
                                                                else if (md.ProcessName.Equals("ADAM"))
                                                                {
                                                                    ThreadPool.QueueUserWorkItem(SetAlarmToMain, (byte)ERROR_CODE.ADAM_VERSION_NOT_MATCH);
                                                                }
                                                                else if (md.ProcessName.Equals("CIM"))
                                                                {
                                                                    ThreadPool.QueueUserWorkItem(SetAlarmToMain, (byte)ERROR_CODE.CIM_VERSION_NOT_MATCH);
                                                                }
                                                                break;
                                                            }
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    dgv.Rows[iRowIndex].Cells[2].Style.BackColor = Color.Red;

                                                    if (md.ProcessName.Equals("EUVO160"))
                                                    {
                                                        ThreadPool.QueueUserWorkItem(SetAlarmToMain, (byte)ERROR_CODE.SOURCE_CONTROL_OFFLINE_STATUS);
                                                    }
                                                    else if (md.ProcessName.Equals("SAM"))
                                                    {
                                                        ThreadPool.QueueUserWorkItem(SetAlarmToMain, (byte)ERROR_CODE.SOURCE_MONITORING_OFFLINE_STATUS);
                                                    }
                                                    else if (md.ProcessName.Equals("ADAM"))
                                                    {
                                                        ThreadPool.QueueUserWorkItem(SetAlarmToMain, (byte)ERROR_CODE.ADAM_OFFLINE_STATUS);
                                                    }
                                                    else if (md.ProcessName.Equals("CIM"))
                                                    {
                                                        ThreadPool.QueueUserWorkItem(SetAlarmToMain, (byte)ERROR_CODE.SOURCE_CONTROL_OFFLINE_STATUS);
                                                    }
                                                    break;
                                                }

                                                bProcessState = sArraySplit[1].Trim() == "ON" ? true : false;
                                                break;
                                        }

                                        dgv.Rows[iRowIndex].Cells[3].Value = md.ProcessCommand == 1 ? true : false;
                                        dgv.Rows[iRowIndex].Cells[4].Value = md.ProcessCPU;
                                        dgv.Rows[iRowIndex].Cells[5].Value = md.ProcessRAM;
                                        dgv.Rows[iRowIndex].Cells[6].Value = md.GdiCount == 0 ? string.Empty : md.GdiCount.ToString();
                                        dgv.Rows[iRowIndex].Cells[7].Value = md.FileVersion;
                                        dgv.Rows[iRowIndex].Cells[8].Value = md.ProductVersion;

                                        if (md.ProcessCommand == 1)
                                        {
                                            switch (bProcessState)
                                            {
                                                case false:
                                                    if (dgv[9, iRowIndex].EditType != null)
                                                    {
                                                        DataGridViewButtonCell CellStart = new DataGridViewButtonCell();
                                                        dgv.Rows[iRowIndex].Cells[9] = CellStart;
                                                        dgv.Rows[iRowIndex].Cells[9].Value = Constants.DEF_START_TEXT;
                                                        dgv.Rows[iRowIndex].Cells[9].Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                                                        switch (sLocalNo)
                                                        {
                                                            case Constants.DEF_SERVER_LOCAL:
                                                                dgv.Rows[iRowIndex].Cells[9].Tag = md.ProcessPath;
                                                                break;
                                                            default:
                                                                string sStartCommand = string.Format("L1_B_PROCESS_START_{0}_{1}_COMMAND", sLocalNo, iCommandIndex);
                                                                dgv.Rows[iRowIndex].Cells[9].Tag = sStartCommand;
                                                                break;
                                                        }
                                                    }

                                                    if (dgv[10, iRowIndex].EditType == null)
                                                    {
                                                        DataGridViewExTextBoxCell cellKill = new DataGridViewExTextBoxCell();
                                                        dgv.Rows[iRowIndex].Cells[10] = cellKill;
                                                    }

                                                    break;
                                                case true:
                                                    if (dgv[9, iRowIndex].EditType == null)
                                                    {
                                                        DataGridViewExTextBoxCell cellStart = new DataGridViewExTextBoxCell();
                                                        dgv.Rows[iRowIndex].Cells[9] = cellStart;
                                                    }
                                                    if (dgv[10, iRowIndex].EditType != null)
                                                    {
                                                        DataGridViewButtonCell CellKill = new DataGridViewButtonCell();
                                                        dgv.Rows[iRowIndex].Cells[10] = CellKill;
                                                        dgv.Rows[iRowIndex].Cells[10].Value = Constants.DEF_KILL_TEXT;
                                                        dgv.Rows[iRowIndex].Cells[10].Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                                                        switch (sLocalNo)
                                                        {
                                                            case Constants.DEF_SERVER_LOCAL:
                                                                dgv.Rows[iRowIndex].Cells[10].Tag = md.ProcessName;
                                                                break;
                                                            default:
                                                                string sKillCommand = string.Format("L1_B_PROCESS_KILL_{0}_{1}_COMMAND", sLocalNo, iCommandIndex);
                                                                dgv.Rows[iRowIndex].Cells[10].Tag = sKillCommand;
                                                                break;
                                                        }
                                                    }
                                                    break;
                                            }
                                        }
                                        break;
                                }
                                iRowIndex++;
                                iCommandIndex++;
                            }
                            iLocalNo++;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        /// <summary>
        /// 
        /// </summary>
        private byte ChildProcessCheck(string sProcessName, string sChildProcessName, string sChildProcessName2)
        {
            IntPtr hDest = FindWindow(null, sProcessName);

            if (hDest != IntPtr.Zero)
            {
                IntPtr hd01 = FindWindowEx(IntPtr.Zero, IntPtr.Zero, null, sChildProcessName);
                IntPtr hd02 = FindWindowEx(IntPtr.Zero, IntPtr.Zero, null, sChildProcessName2);

                if (hd01 != IntPtr.Zero && hd02 != IntPtr.Zero)
                    return 0;
                if (hd01 == IntPtr.Zero && hd02 == IntPtr.Zero)
                    return 2;
                else
                    return 1;
            }
            else
                return 2;
        }
        /// <summary>
        /// 
        /// </summary>
        private void OnGetUsage()
        {
            while (true)
            {
                m_OldTime = Common.DateTimeClone();

                List<MonitoringData> listMD = new List<MonitoringData>();
                Thread.Sleep(1);
                bool bTotalCheck = false;

                foreach (string sProcessName in m_hashProcessDatas.Keys)
                {
                    Process[] processlist = Process.GetProcessesByName(sProcessName);


                    MonitoringData md = new MonitoringData()
                    {
                        ProcessLocal = Constants.DEF_SERVER_LOCAL,
                        ProcessPath = m_hashProcessDatas[sProcessName][1],
                        ProcessCommand = string.IsNullOrEmpty(m_hashProcessDatas[sProcessName][2])
                                        ? 1 : Convert.ToInt32(m_hashProcessDatas[sProcessName][2]),
                        ChildProcessName1 = m_hashProcessDatas[sProcessName][3],
                        ChildProcessName2 = m_hashProcessDatas[sProcessName][4],
                    };

                    if (!bTotalCheck)
                    {
                        string sTotalCpu = (string)Common.Clone(Common.TotalCPU(Constants.DEF_TOTAL_TEXT));
                        md.TotalCPU = sTotalCpu;

                        string sFreeRam = (string)Common.Clone(Common.OnRamUsage());
                        md.TotalRAM = sFreeRam;
                        bTotalCheck = true;
                    }

                    if (processlist.Length > 0)
                    {
                        //ChildProcessName이 존재 할 때
                        if (md.ChildProcessName2 != string.Empty)
                        {
                            IntPtr hWndChild1 = FindWindow(null, md.ChildProcessName1);
                            IntPtr hWndChild2 = FindWindow(null, md.ChildProcessName2);

                            if (!hWndChild1.Equals(IntPtr.Zero) && !!hWndChild2.Equals(IntPtr.Zero))
                            {
                                md.ProcessState = Constants.DEF_OK_TEXT;
                            }
                            else
                            {
                                md.ProcessState = Constants.DEF_OFF_TEXT;
                            }

                        }
                        else
                        {
                            md.ProcessState = Constants.DEF_OK_TEXT;
                        }

                        md.ProcessName = sProcessName;
                        md.ProcessCPU = Common.TotalCPU(sProcessName);
                        md.ProcessRAM = Common.ProcessRAM(sProcessName);
                        md.GdiCount = Common.GetGuiResourcesGDICount(processlist[0]);

                        if (!m_hashProcessToVersion.ContainsKey(sProcessName))
                        {
                            FileVersionInfo fiv = FileVersionInfo.GetVersionInfo(md.ProcessPath);
                            md.FileVersion = fiv.FileVersion;
                            md.ProductVersion = fiv.ProductVersion;

                            List<string> listVersion = new List<string>()
                            {
                                md.FileVersion,
                                md.ProductVersion
                            };

                            m_hashProcessToVersion.Add(sProcessName, listVersion);
                        }
                        else
                        {
                            md.FileVersion = m_hashProcessToVersion[sProcessName][0];
                            md.ProductVersion = m_hashProcessToVersion[sProcessName][1];
                        }
                    }
                    else if (!string.IsNullOrEmpty(sProcessName))
                    {
                        md.ProcessState = Constants.DEF_OFF_TEXT;
                        md.ProcessName = sProcessName;
                        md.ProcessCPU = string.Empty;
                        md.ProcessRAM = string.Empty;

                        m_hashProcessToVersion.Remove(sProcessName);

                        if (m_bProcessAutoStart && !m_listOffProcess.Contains(m_hashProcessDatas[sProcessName][0]))
                            if (md.ProcessCommand == 1)
                                m_listOffProcess.Add(md.ProcessPath);
                    }
                    md.LocalName = m_sServerName;
                    md.ScanTime = DateTime.Now.ToString();
                    listMD.Add(md);
                }

                if (!m_hashLocalNameToMd.ContainsKey(Constants.DEF_SERVER_LOCAL))
                    m_hashLocalNameToMd.Add(Constants.DEF_SERVER_LOCAL, listMD);
                else
                    m_hashLocalNameToMd[Constants.DEF_SERVER_LOCAL] = listMD;


                DataGridViewCrossThread(m_DgvSumView);

                MonitoringDataWrite(Constants.DEF_SERVER_TEXT, listMD);

                int iTime = Common.OnTimeDiff(m_OldTime, 1000);

                Thread.Sleep(iTime < 0 ? 0 : iTime);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        private void OnShareDataChanged()
        {
            try
            {
                Dictionary<string, PlcAddr> hashMonitoring = new Dictionary<string, PlcAddr>();

                foreach (PlcAddr addr in m_ShareTrx.hashShareKeyToShareData.Values)
                {
                    if (addr.ValueType == PlcValueType.BIT)
                    {
                        hashMonitoring.Add(addr.AddrName, addr.Clone());
                        hashMonitoring[addr.AddrName].AddrName = (string)addr.AddrName.Clone();
                    }
                }

                string[] sArrayKeys = new string[hashMonitoring.Keys.Count];
                hashMonitoring.Keys.CopyTo(sArrayKeys, 0);

                while (true)
                {
                    Thread.Sleep(1);

                    foreach (string sKey in sArrayKeys)
                    {
                        PlcAddr addr = hashMonitoring[sKey];

                        bool bReadValue = (bool)Common.Clone(m_ShareMem.GetBit(addr));

                        if (hashMonitoring[sKey].vBit != bReadValue)
                        {
                            Thread.Sleep(1);
                            hashMonitoring[sKey].vBit = bReadValue;
                            m_queueShareDataBunchData.Enqueue(addr);

                            lock (m_Object)
                                Monitor.Pulse(m_Object);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());

                m_threadShareDataChanged = new Thread(new ThreadStart(OnShareDataChanged))
                { IsBackground = true };

                m_threadShareDataChanged.Start();
            }
        }
        /// <summary>
        /// 
        /// </summary>
        private void OnShareMessageProcessorThread()
        {
            try
            {
                while (true)
                {
                    PlcAddr addr = null;

                    lock (m_Object)
                    {
                        if (m_queueShareDataBunchData.Count > 0)
                            addr = m_queueShareDataBunchData.Dequeue();
                        else
                            Monitor.Wait(m_Object);

                        if (addr != null) EventShareDataChanged(addr);
                    }
                }
            }
            catch (Exception ex)
            {
                m_LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        #endregion

        #region Class Util
        /// <summary>
        /// 
        /// </summary>
        private void MonitoringDataWrite(string sModule, List<MonitoringData> list)
        {
            try
            {
                bool bTotalCheck = false;

                //Server LogManager Init
                if (!m_LogManager.hashKeyToLog.ContainsKey(sModule))
                    m_LogManager.Initialize(sModule, string.Empty, Constants.DEF_LOG_TEXT, eLogTimeType.Hour);

                foreach (MonitoringData md in list)
                {
                    if (!bTotalCheck)
                    {
                        m_LogManager.WriteLog(sModule, string.Format("Total Cpu : {0}", md.TotalCPU));
                        m_LogManager.WriteLog(sModule, string.Format("FreeRam : {0}", md.TotalRAM));
                        bTotalCheck = true;
                    }
                    m_LogManager.WriteLog(sModule, string.Format("Process State : {0} / {1}", string.IsNullOrEmpty(md.ProcessName) ? "Error" : md.ProcessName, md.ProcessState));
                    m_LogManager.WriteLog(sModule, string.Format("Process Cpu : {0} / Ram : {1}", md.ProcessCPU, md.ProcessRAM));
                    m_LogManager.WriteLog(sModule, string.Format("Version File : {0} / Product : {1}", md.FileVersion, md.ProductVersion));
                    m_LogManager.WriteLog(sModule, string.Format("Gdi Count : {0}", md.GdiCount));
                }
            }
            catch (Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        #endregion

        #region Class Public
        /// <summary>
        /// 
        /// </summary>
        protected override void WndProc(ref Message m)
        {
            try
            {
                Thread.Sleep(1);

                switch (m.Msg)
                {
                    case WM_COPYDATA:
                        COPYDATASTRUCT cds = (COPYDATASTRUCT)m.GetLParam(typeof(COPYDATASTRUCT));
                        switch (cds.lpData)
                        {
                            case "APP_SHOW":
                                this.Opacity = 100;
                                if (this.WindowState == FormWindowState.Minimized)
                                    this.WindowState = FormWindowState.Normal;

                                this.Show();
                                this.Focus();
                                break;
                            case "APP_HIDE":
                                //this.Opacity = 0;
                                this.Hide();
                                break;
                        }
                        break;
                    default:
                        base.WndProc(ref m);
                        break;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        #endregion

        private void btnLoadSettingData_Click(object sender, EventArgs e)
        {
            string sSettingFilePath = Path.Combine(Application.StartupPath, "_ConfigFiles", Constants.DEF_SERVER_TEXT, Constants.DEF_SETTING_TEXT, "Setting.xml");

            if (!File.Exists(sSettingFilePath))
            {
                MessageBox.Show("Setting Xml 파일이 존재 하지 않습니다.\n시스템을 종료 합니다.");
                Environment.Exit(0);
            }

            XmlDataReader xmlreader = new XmlDataReader(sSettingFilePath, false);
            string sHaederKey = "ProcessWatcher/";

            _lastestVersionData = xmlreader.XmlReadDictionary(sHaederKey + "Lastest", Enum.GetNames(typeof(eLastestVersion)));
            _versionData = new List<LastestVersionData>();
            foreach (string[] sArrayValue in _lastestVersionData.Values)
            {
                string sProcessName = sArrayValue[0];
                string sVersion = sArrayValue[1];

                LastestVersionData data = new LastestVersionData()
                {
                    ProcessName = sProcessName,
                    Version = sVersion
                };

                _versionData.Add(data);
            }
        }

        #region Config

        XDocument doc;
        private void btnSave_Click(object sender, EventArgs e)
        {
            string sSettingFilePath = Path.Combine(Application.StartupPath, "_ConfigFiles", Constants.DEF_SERVER_TEXT, Constants.DEF_SETTING_TEXT, "Setting.xml");
            if (!System.IO.File.Exists(sSettingFilePath))
            {
                return;
            }
            
            doc = XDocument.Load(sSettingFilePath);

            switch ((sender as Button).Name)
            {
                case "btnSaveSetting": 
                    {
                        CreateOrUpdateNode("Settings", "Setting", eServerSettingKeys.ServerName.ToString(), tbConfigSettingServerName.Text.Trim(),
                             new string[] { eServerSettingKeys.AutoStart.ToString(), eServerSettingKeys.Restart.ToString(), eServerSettingKeys.LogDriver.ToString(), eServerSettingKeys.LogDays.ToString() },
                             new string[] { "", "", tbConfigSettingLogDriver.Text.Trim(), tbConfigSettingLogDays.Text.Trim() });

                        Save(sSettingFilePath);
                    }
                    break;
                case "btnSaveVersion":
                    {
                        CreateOrUpdateNode("Lastest", "Last", eLastestVersion.Name.ToString(), tbConfigVersionName.Text.Trim(),
                             new string[] { eLastestVersion.Name.ToString(), eLastestVersion.Version.ToString() },
                             new string[] { tbConfigVersionName.Text.Trim(), tbConfigVersionVersion.Text.Trim() });

                        Save(sSettingFilePath);
                    }
                    break;
                case "btnSaveProcess":
                    {
                        CreateOrUpdateNode("Pros", "Pro", eProcessKeys.Name.ToString(), tbConfigProcessName.Text.Trim(),
                             new string[] { eProcessKeys.Name.ToString(), eProcessKeys.Path.ToString(), eProcessKeys.Command.ToString(), eProcessKeys.ChildName1.ToString(), eProcessKeys.ChildName2.ToString() },
                             new string[] { tbConfigProcessName.Text.Trim(), tbConfigProcessPath.Text.Trim(), "", tbConfigProcessChild1.Text.Trim(), tbConfigProcessChild2.Text.Trim(), });

                        Save(sSettingFilePath);
                    }
                    break;
                case "btnSaveShareMemory":
                    {
                        CreateOrUpdateNode("Memorys", "Memory", eServerShareMemoryKeys.Name.ToString(), tbConfigShareMemoryName.Text.Trim(),
                             new string[] { eServerShareMemoryKeys.Name.ToString(), eServerShareMemoryKeys.Size.ToString() },
                             new string[] { "", "", tbConfigSettingLogDriver.Text.Trim(), tbConfigSettingLogDays.Text.Trim() });

                        Save(sSettingFilePath);
                    }
                    break;
                case "btnSaveClient":
                    {
                        CreateOrUpdateNode("Clients", "Clients", eServerClientProcessKeys.Name.ToString(), tbConfigClientName.Text.Trim(),
                             new string[] { eServerClientProcessKeys.Name.ToString(), eServerClientProcessKeys.Ip.ToString(), eServerClientProcessKeys.Process_1.ToString(), eServerClientProcessKeys.Process_2.ToString(), eServerClientProcessKeys.Process_3.ToString(), eServerClientProcessKeys.Process_4.ToString(), eServerClientProcessKeys.Process_5.ToString() },
                             new string[] { tbConfigClientName.Text.Trim(), tbConfigClientIP.Text.Trim(), tbConfigClientProcess1.Text.Trim(), tbConfigClientProcess2.Text.Trim(), tbConfigClientProcess3.Text.Trim(), tbConfigClientProcess4.Text.Trim(), tbConfigClientProcess5.Text.Trim() });

                        Save(sSettingFilePath);
                    }
                    break;
            }

            InitializeSetting();
            InitializeDataGridView();

            InitializeSettingUI();
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {
            string sSettingFilePath = Path.Combine(Application.StartupPath, "_ConfigFiles", Constants.DEF_SERVER_TEXT, Constants.DEF_SETTING_TEXT, "Setting.xml");

            doc = XDocument.Load(sSettingFilePath);

            switch ((sender as Button).Name)
            {
                case "btnRemoveSetting":
                    {
                        DeleteNode("Settings", "Setting", eServerSettingKeys.ServerName.ToString(), tbConfigSettingServerName.Text.Trim());

                        Save(sSettingFilePath);
                    }
                    break;
                case "btnRemoveVersion":
                    {
                        DeleteNode("Lastest", "Last", eLastestVersion.Name.ToString(), tbConfigVersionName.Text.Trim());

                        Save(sSettingFilePath);
                    }
                    break;
                case "btnRemoveProcess":
                    {
                        DeleteNode("Pros", "Pro", eProcessKeys.Name.ToString(), tbConfigProcessName.Text.Trim());

                        Save(sSettingFilePath);
                    }
                    break;
                case "btnRemoveShareMemory":
                    {
                        DeleteNode("Memorys", "Memory", eServerShareMemoryKeys.Name.ToString(), tbConfigShareMemoryName.Text.Trim());

                        Save(sSettingFilePath);
                    }
                    break;
                case "btnRemoveClient":
                    {
                        DeleteNode("Clients", "Clients", eServerClientProcessKeys.Name.ToString(), tbConfigClientName.Text.Trim());

                        Save(sSettingFilePath);
                    }
                    break;
            }

            InitializeSetting();
            InitializeDataGridView();

            InitializeSettingUI();
        }

        private void cbProcess_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (m_hashProcessDatas.ContainsKey(cbProcess.SelectedItem.ToString()))
            {
                string key = cbProcess.SelectedItem.ToString();

                tbConfigProcessName.Text = m_hashProcessDatas[key][0].ToString();
                tbConfigProcessPath.Text = m_hashProcessDatas[key][1].ToString();
                //command
                tbConfigProcessChild1.Text = m_hashProcessDatas[key][3].ToString();
                tbConfigProcessChild2.Text = m_hashProcessDatas[key][4].ToString();
            }
        }

        private void cbClient_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (_hashClientProcess.ContainsKey(cbClient.SelectedItem.ToString()))
            {
                string key = cbClient.SelectedItem.ToString();

                tbConfigClientName.Text = _hashClientProcess[key][0].ToString();
                tbConfigClientIP.Text = _hashClientProcess[key][1].ToString();
                tbConfigClientProcess1.Text = _hashClientProcess[key][2].ToString();
                tbConfigClientProcess2.Text = _hashClientProcess[key][3].ToString();
                tbConfigClientProcess3.Text = _hashClientProcess[key][4].ToString();
                tbConfigClientProcess4.Text = _hashClientProcess[key][5].ToString();
                tbConfigClientProcess5.Text = _hashClientProcess[key][6].ToString();
            }
        }

        private void cbShareMemory_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (m_ShareMemoryManager.ListShardMemory.ContainsKey(cbShareMemory.SelectedItem.ToString()))
            {
                string key = cbShareMemory.SelectedItem.ToString();

                tbConfigShareMemoryName.Text = m_ShareMemoryManager.ListShardMemory[key].MemoryName;
                tbConfigShareMemorySize.Text = m_ShareMemoryManager.ListShardMemory[key].MemorySize.ToString();
            }
        }

        private void cbVersion_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (_lastestVersionData.ContainsKey(cbVersion.SelectedItem.ToString()))
            {
                string key = cbVersion.SelectedItem.ToString();

                tbConfigVersionName.Text = _lastestVersionData[key][0].ToString();
                tbConfigVersionVersion.Text = _lastestVersionData[key][1].ToString();
            }
        }

        private void InitializeSettingUI()
        {
            tbConfigSettingServerName.Text = m_sServerName;
            rbConfigSettingRestartTrue.Checked = true;
            rbConfigSettingRestartFalse.Checked = true;
            tbConfigSettingLogDriver.Text = m_sLogDriver;
            tbConfigSettingLogDays.Text = m_iLogDays.ToString();
            rbConfigSettingAutoStartTrue.Checked = true;
            rbConfigSettingAutoStartFalse.Checked = true;

            cbProcess.Items.Clear();
            foreach (string key in m_hashProcessDatas.Keys)
                cbProcess.Items.Add(key);

            cbVersion.Items.Clear();
            foreach (string key in _lastestVersionData.Keys)
                cbVersion.Items.Add(key);

            cbShareMemory.Items.Clear();
            foreach (string key in m_ShareMemoryManager.ListShardMemory.Keys)
                cbShareMemory.Items.Add(key);

            cbClient.Items.Clear();
            foreach (string key in _hashClientProcess.Keys)
                cbClient.Items.Add(key);

            tbConfigVersionName.Text = string.Empty;
            tbConfigVersionVersion.Text = string.Empty;

            tbConfigProcessName.Text = string.Empty;
            tbConfigProcessPath.Text = string.Empty;
            rbConfigPrecessCommandTrue.Checked = true;
            rbConfigPrecessCommandFalse.Checked = true;
            tbConfigProcessChild1.Text = string.Empty;
            tbConfigProcessChild2.Text = string.Empty;

            tbConfigClientName.Text = string.Empty;
            tbConfigClientIP.Text = string.Empty;
            tbConfigClientProcess1 .Text = string.Empty;
            tbConfigClientProcess2 .Text = string.Empty;
            tbConfigClientProcess3 .Text = string.Empty;
            tbConfigClientProcess4 .Text = string.Empty;
            tbConfigClientProcess5.Text = string.Empty;

            tbConfigShareMemoryName.Text = string.Empty;
            tbConfigShareMemorySize.Text = "0";

        }

        private bool CreateOrUpdateNode(string elementName, string nodeName, string keyAttributeName, string keyAttributeValue, string[] keys, string[] values)
        {
            var nodesParent = doc.Root.Element(elementName); 
            if (nodesParent == null)
            {
                nodesParent = new XElement(elementName);
                doc.Root.AddFirst(nodesParent);
            }

            var node = nodesParent.Elements(nodeName)
                .FirstOrDefault(e => (string)e.Attribute(keyAttributeName) == keyAttributeValue);

            if (node != null)
            {
                for (int iPos = 0; iPos < keys.Length; iPos++)
                {
                    node.SetAttributeValue(keys[iPos], values[iPos]);
                }

                return false;
            }
            else
            {
                object[] attribute = new object[keys.Length];
                for (int iPos = 0; iPos < keys.Length; iPos++)
                {
                    attribute[iPos] = new XAttribute(keys[iPos], values[iPos]);
                }

                node = new XElement(nodeName, attribute);
                nodesParent.Add(node);

                return true;
            }
        }

        private void DeleteNode(string elementName, string nodeName, string keyAttributeName, string keyAttributeValue)
        {
            var nodesParent = doc.Root.Element(elementName);
            if (nodesParent == null)
            {
                nodesParent = new XElement(elementName);
                doc.Root.AddFirst(nodesParent);
            }

            var node = nodesParent.Elements(nodeName)
                .FirstOrDefault(e => (string)e.Attribute(keyAttributeName) == keyAttributeValue);

            if (node != null)
            {
                node.Remove();
            }
        }

        private void Save(string filePath)
        {
            doc.Save(filePath);
        }


        #endregion

    }
}
