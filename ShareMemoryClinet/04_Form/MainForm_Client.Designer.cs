﻿namespace ProcessWatcher
{
    partial class MainForm_Client
    {
        /// <summary>
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 디자이너에서 생성한 코드

        /// <summary>
        /// 디자이너 지원에 필요한 메서드입니다. 
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm_Client));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            this.m_TimerAutoStart = new System.Windows.Forms.Timer(this.components);
            this.m_TimerState = new System.Windows.Forms.Timer(this.components);
            this.m_tbState = new System.Windows.Forms.TextBox();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.열기ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.종료ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);
            this.m_btnStartUpAdd = new System.Windows.Forms.Button();
            this.m_btnStartUpRemove = new System.Windows.Forms.Button();
            this.m_btnHide = new System.Windows.Forms.Button();
            this.m_TimerInterface = new System.Windows.Forms.Timer(this.components);
            this.m_TimerProcessAutoStart = new System.Windows.Forms.Timer(this.components);
            this.m_DgvSumView = new System.Windows.Forms.DataGridView();
            this.sName = new DataGridViewExTextBoxColumn();
            this.bState = new DataGridViewExTextBoxColumn();
            this.bAutoStartState = new DataGridViewExTextBoxColumn();
            this.sCPU = new DataGridViewExTextBoxColumn();
            this.sRAM = new DataGridViewExTextBoxColumn();
            this.sFileVersion = new DataGridViewExTextBoxColumn();
            this.sProductVersion = new DataGridViewExTextBoxColumn();
            this.btnStart = new DataGridViewExTextBoxColumn();
            this.btnKill = new DataGridViewExTextBoxColumn();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.m_lbProcessAutoState = new System.Windows.Forms.Label();
            this.m_lbShareMemoryState = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.m_btnProcessAutoStart = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.m_cbScanTime = new System.Windows.Forms.ComboBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.m_btnAutoStart = new System.Windows.Forms.Button();
            this.m_btnStart = new System.Windows.Forms.Button();
            this.m_btnStop = new System.Windows.Forms.Button();
            this.lstLogger = new System.Windows.Forms.ListView();
            this.lstShMem = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader10 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader11 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader12 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader7 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader8 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader9 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader13 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.contextMenuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.m_DgvSumView)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.SuspendLayout();
            // 
            // m_TimerAutoStart
            // 
            this.m_TimerAutoStart.Interval = 5000;
            this.m_TimerAutoStart.Tick += new System.EventHandler(this.TimerAutoStart_Tick);
            // 
            // m_TimerState
            // 
            this.m_TimerState.Enabled = true;
            this.m_TimerState.Interval = 1000;
            this.m_TimerState.Tick += new System.EventHandler(this.TimerState_Tick);
            // 
            // m_tbState
            // 
            this.m_tbState.Enabled = false;
            this.m_tbState.Location = new System.Drawing.Point(12, 462);
            this.m_tbState.Name = "m_tbState";
            this.m_tbState.Size = new System.Drawing.Size(1040, 28);
            this.m_tbState.TabIndex = 8;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.열기ToolStripMenuItem,
            this.종료ToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(145, 64);
            // 
            // 열기ToolStripMenuItem
            // 
            this.열기ToolStripMenuItem.Name = "열기ToolStripMenuItem";
            this.열기ToolStripMenuItem.Size = new System.Drawing.Size(144, 30);
            this.열기ToolStripMenuItem.Text = "열기(&O)";
            this.열기ToolStripMenuItem.Click += new System.EventHandler(this.OntsmiShow_Click);
            // 
            // 종료ToolStripMenuItem
            // 
            this.종료ToolStripMenuItem.Name = "종료ToolStripMenuItem";
            this.종료ToolStripMenuItem.Size = new System.Drawing.Size(144, 30);
            this.종료ToolStripMenuItem.Text = "종료(&E)";
            this.종료ToolStripMenuItem.Click += new System.EventHandler(this.OntsmiExit_Click);
            // 
            // notifyIcon1
            // 
            this.notifyIcon1.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon1.Icon")));
            this.notifyIcon1.Text = "Esol ShareMemory Sync";
            this.notifyIcon1.Visible = true;
            this.notifyIcon1.MouseClick += new System.Windows.Forms.MouseEventHandler(this.OnMouseClick);
            this.notifyIcon1.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.OnOpenClick);
            // 
            // m_btnStartUpAdd
            // 
            this.m_btnStartUpAdd.Location = new System.Drawing.Point(5, 624);
            this.m_btnStartUpAdd.Name = "m_btnStartUpAdd";
            this.m_btnStartUpAdd.Size = new System.Drawing.Size(155, 40);
            this.m_btnStartUpAdd.TabIndex = 15;
            this.m_btnStartUpAdd.Text = "Add StartUp";
            this.m_btnStartUpAdd.UseVisualStyleBackColor = true;
            this.m_btnStartUpAdd.Click += new System.EventHandler(this.OnStartUpAdd);
            // 
            // m_btnStartUpRemove
            // 
            this.m_btnStartUpRemove.Location = new System.Drawing.Point(166, 624);
            this.m_btnStartUpRemove.Name = "m_btnStartUpRemove";
            this.m_btnStartUpRemove.Size = new System.Drawing.Size(155, 40);
            this.m_btnStartUpRemove.TabIndex = 16;
            this.m_btnStartUpRemove.Text = "Remove Startup";
            this.m_btnStartUpRemove.UseVisualStyleBackColor = true;
            this.m_btnStartUpRemove.Click += new System.EventHandler(this.OnStartUpRemove);
            // 
            // m_btnHide
            // 
            this.m_btnHide.Location = new System.Drawing.Point(327, 624);
            this.m_btnHide.Name = "m_btnHide";
            this.m_btnHide.Size = new System.Drawing.Size(105, 40);
            this.m_btnHide.TabIndex = 17;
            this.m_btnHide.Text = "Hide";
            this.m_btnHide.UseVisualStyleBackColor = true;
            this.m_btnHide.Click += new System.EventHandler(this.OnHide);
            // 
            // m_TimerProcessAutoStart
            // 
            this.m_TimerProcessAutoStart.Interval = 10000;
            this.m_TimerProcessAutoStart.Tick += new System.EventHandler(this.TimerProcessAutoStart_Tick);
            // 
            // m_DgvSumView
            // 
            this.m_DgvSumView.AllowUserToAddRows = false;
            this.m_DgvSumView.AllowUserToDeleteRows = false;
            this.m_DgvSumView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.m_DgvSumView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.sName,
            this.bState,
            this.bAutoStartState,
            this.sCPU,
            this.sRAM,
            this.sFileVersion,
            this.sProductVersion,
            this.btnStart,
            this.btnKill});
            this.m_DgvSumView.Location = new System.Drawing.Point(8, 62);
            this.m_DgvSumView.Name = "m_DgvSumView";
            this.m_DgvSumView.ReadOnly = true;
            this.m_DgvSumView.RowTemplate.Height = 30;
            this.m_DgvSumView.Size = new System.Drawing.Size(1455, 556);
            this.m_DgvSumView.TabIndex = 27;
            this.m_DgvSumView.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.OnCellClick);
            this.m_DgvSumView.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.OnDoubleClick);
            // 
            // sName
            // 
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.sName.DefaultCellStyle = dataGridViewCellStyle1;
            this.sName.Frozen = true;
            this.sName.HeaderText = "Name";
            this.sName.Name = "sName";
            this.sName.ReadOnly = true;
            this.sName.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.sName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            this.sName.Width = 200;
            // 
            // bState
            // 
            this.bState.Frozen = true;
            this.bState.HeaderText = "State";
            this.bState.Name = "bState";
            this.bState.ReadOnly = true;
            this.bState.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.bState.Width = 50;
            // 
            // bAutoStartState
            // 
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.bAutoStartState.DefaultCellStyle = dataGridViewCellStyle2;
            this.bAutoStartState.Frozen = true;
            this.bAutoStartState.HeaderText = "AutoStartMode";
            this.bAutoStartState.Name = "bAutoStartState";
            this.bAutoStartState.ReadOnly = true;
            this.bAutoStartState.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // sCPU
            // 
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.sCPU.DefaultCellStyle = dataGridViewCellStyle3;
            this.sCPU.Frozen = true;
            this.sCPU.HeaderText = "CPU";
            this.sCPU.Name = "sCPU";
            this.sCPU.ReadOnly = true;
            this.sCPU.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // sRAM
            // 
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.sRAM.DefaultCellStyle = dataGridViewCellStyle4;
            this.sRAM.Frozen = true;
            this.sRAM.HeaderText = "RAM";
            this.sRAM.Name = "sRAM";
            this.sRAM.ReadOnly = true;
            this.sRAM.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // sFileVersion
            // 
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.sFileVersion.DefaultCellStyle = dataGridViewCellStyle5;
            this.sFileVersion.Frozen = true;
            this.sFileVersion.HeaderText = "FileVersion";
            this.sFileVersion.Name = "sFileVersion";
            this.sFileVersion.ReadOnly = true;
            this.sFileVersion.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // sProductVersion
            // 
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.sProductVersion.DefaultCellStyle = dataGridViewCellStyle6;
            this.sProductVersion.Frozen = true;
            this.sProductVersion.HeaderText = "ProductVersion";
            this.sProductVersion.Name = "sProductVersion";
            this.sProductVersion.ReadOnly = true;
            this.sProductVersion.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // btnStart
            // 
            this.btnStart.Frozen = true;
            this.btnStart.HeaderText = "Start";
            this.btnStart.Name = "btnStart";
            this.btnStart.ReadOnly = true;
            this.btnStart.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // btnKill
            // 
            this.btnKill.Frozen = true;
            this.btnKill.HeaderText = "Kill";
            this.btnKill.Name = "btnKill";
            this.btnKill.ReadOnly = true;
            this.btnKill.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(12, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1480, 702);
            this.tabControl1.TabIndex = 28;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.m_lbProcessAutoState);
            this.tabPage1.Controls.Add(this.m_lbShareMemoryState);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Controls.Add(this.m_btnProcessAutoStart);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Controls.Add(this.m_cbScanTime);
            this.tabPage1.Controls.Add(this.m_DgvSumView);
            this.tabPage1.Controls.Add(this.m_btnHide);
            this.tabPage1.Controls.Add(this.m_btnStartUpRemove);
            this.tabPage1.Controls.Add(this.m_btnStartUpAdd);
            this.tabPage1.Location = new System.Drawing.Point(4, 28);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1472, 670);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Process Watcher";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // m_lbProcessAutoState
            // 
            this.m_lbProcessAutoState.AutoSize = true;
            this.m_lbProcessAutoState.Location = new System.Drawing.Point(207, 41);
            this.m_lbProcessAutoState.Name = "m_lbProcessAutoState";
            this.m_lbProcessAutoState.Size = new System.Drawing.Size(0, 18);
            this.m_lbProcessAutoState.TabIndex = 35;
            // 
            // m_lbShareMemoryState
            // 
            this.m_lbShareMemoryState.AutoSize = true;
            this.m_lbShareMemoryState.Location = new System.Drawing.Point(207, 13);
            this.m_lbShareMemoryState.Name = "m_lbShareMemoryState";
            this.m_lbShareMemoryState.Size = new System.Drawing.Size(0, 18);
            this.m_lbShareMemoryState.TabIndex = 32;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(8, 41);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(179, 18);
            this.label3.TabIndex = 33;
            this.label3.Text = "Process Auto State :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 13);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(181, 18);
            this.label4.TabIndex = 34;
            this.label4.Text = "ShareMemory State :";
            // 
            // m_btnProcessAutoStart
            // 
            this.m_btnProcessAutoStart.Location = new System.Drawing.Point(1373, 624);
            this.m_btnProcessAutoStart.Name = "m_btnProcessAutoStart";
            this.m_btnProcessAutoStart.Size = new System.Drawing.Size(90, 37);
            this.m_btnProcessAutoStart.TabIndex = 31;
            this.m_btnProcessAutoStart.Text = "Disable";
            this.m_btnProcessAutoStart.UseVisualStyleBackColor = true;
            this.m_btnProcessAutoStart.Click += new System.EventHandler(this.OnProcessAutoStart);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(1197, 633);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(169, 18);
            this.label2.TabIndex = 29;
            this.label2.Text = "Process AutoStart :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(960, 633);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(105, 18);
            this.label1.TabIndex = 30;
            this.label1.Text = "Scan Time :";
            this.label1.Visible = false;
            // 
            // m_cbScanTime
            // 
            this.m_cbScanTime.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.m_cbScanTime.Font = new System.Drawing.Font("굴림", 14F);
            this.m_cbScanTime.FormattingEnabled = true;
            this.m_cbScanTime.Items.AddRange(new object[] {
            "1s",
            "2s",
            "3s",
            "4s",
            "5s",
            "6s",
            "7s",
            "8s",
            "9s",
            "10s",
            "11s",
            "12s",
            "13s",
            "14s",
            "15s",
            "16s",
            "17s",
            "18s",
            "19s",
            "20s"});
            this.m_cbScanTime.Location = new System.Drawing.Point(1071, 624);
            this.m_cbScanTime.Name = "m_cbScanTime";
            this.m_cbScanTime.Size = new System.Drawing.Size(121, 36);
            this.m_cbScanTime.TabIndex = 28;
            this.m_cbScanTime.Visible = false;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.button1);
            this.tabPage2.Controls.Add(this.button2);
            this.tabPage2.Controls.Add(this.button3);
            this.tabPage2.Controls.Add(this.m_btnAutoStart);
            this.tabPage2.Controls.Add(this.m_btnStart);
            this.tabPage2.Controls.Add(this.m_btnStop);
            this.tabPage2.Controls.Add(this.lstLogger);
            this.tabPage2.Controls.Add(this.lstShMem);
            this.tabPage2.Location = new System.Drawing.Point(4, 28);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1472, 670);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "ShareMemory";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(281, 625);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(72, 40);
            this.button1.TabIndex = 37;
            this.button1.Text = "Hide";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.OnHide);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(125, 625);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(150, 40);
            this.button2.TabIndex = 36;
            this.button2.Text = "Remove Startup";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.OnStartUpRemove);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(5, 624);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(114, 40);
            this.button3.TabIndex = 35;
            this.button3.Text = "Add StartUp";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.OnStartUpAdd);
            // 
            // m_btnAutoStart
            // 
            this.m_btnAutoStart.Location = new System.Drawing.Point(1156, 624);
            this.m_btnAutoStart.Name = "m_btnAutoStart";
            this.m_btnAutoStart.Size = new System.Drawing.Size(167, 40);
            this.m_btnAutoStart.TabIndex = 34;
            this.m_btnAutoStart.Text = "AutoStart Enable";
            this.m_btnAutoStart.UseVisualStyleBackColor = true;
            this.m_btnAutoStart.Click += new System.EventHandler(this.OnAutoStartChange);
            // 
            // m_btnStart
            // 
            this.m_btnStart.Location = new System.Drawing.Point(1329, 624);
            this.m_btnStart.Name = "m_btnStart";
            this.m_btnStart.Size = new System.Drawing.Size(62, 40);
            this.m_btnStart.TabIndex = 33;
            this.m_btnStart.Text = "Start";
            this.m_btnStart.UseVisualStyleBackColor = true;
            this.m_btnStart.Click += new System.EventHandler(this.OnbtnStart_Click);
            // 
            // m_btnStop
            // 
            this.m_btnStop.Enabled = false;
            this.m_btnStop.Location = new System.Drawing.Point(1397, 624);
            this.m_btnStop.Name = "m_btnStop";
            this.m_btnStop.Size = new System.Drawing.Size(69, 40);
            this.m_btnStop.TabIndex = 32;
            this.m_btnStop.Text = "Stop";
            this.m_btnStop.UseVisualStyleBackColor = true;
            this.m_btnStop.Click += new System.EventHandler(this.OnbtnStop_Click);
            // 
            // lstLogger
            // 
            this.lstLogger.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lstLogger.HideSelection = false;
            this.lstLogger.Location = new System.Drawing.Point(8, 222);
            this.lstLogger.Name = "lstLogger";
            this.lstLogger.Size = new System.Drawing.Size(1458, 396);
            this.lstLogger.TabIndex = 31;
            this.lstLogger.UseCompatibleStateImageBehavior = false;
            this.lstLogger.View = System.Windows.Forms.View.Details;
            // 
            // lstShMem
            // 
            this.lstShMem.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lstShMem.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader4,
            this.columnHeader10,
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader11,
            this.columnHeader12,
            this.columnHeader5,
            this.columnHeader6,
            this.columnHeader7,
            this.columnHeader8,
            this.columnHeader9,
            this.columnHeader13});
            this.lstShMem.FullRowSelect = true;
            this.lstShMem.HideSelection = false;
            this.lstShMem.Location = new System.Drawing.Point(8, 6);
            this.lstShMem.Name = "lstShMem";
            this.lstShMem.Size = new System.Drawing.Size(1458, 210);
            this.lstShMem.TabIndex = 30;
            this.lstShMem.UseCompatibleStateImageBehavior = false;
            this.lstShMem.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "";
            this.columnHeader1.Width = 1;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "IP Address";
            this.columnHeader4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader4.Width = 110;
            // 
            // columnHeader10
            // 
            this.columnHeader10.Text = "Direction";
            this.columnHeader10.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader10.Width = 80;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "ShareMemory Name";
            this.columnHeader2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader2.Width = 200;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Size";
            this.columnHeader3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader3.Width = 70;
            // 
            // columnHeader11
            // 
            this.columnHeader11.Text = "Start Position";
            this.columnHeader11.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader11.Width = 90;
            // 
            // columnHeader12
            // 
            this.columnHeader12.Text = "End Position";
            this.columnHeader12.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader12.Width = 90;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "Connection Status";
            this.columnHeader5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader5.Width = 90;
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "Sync Count";
            this.columnHeader6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader6.Width = 90;
            // 
            // columnHeader7
            // 
            this.columnHeader7.Text = "Sync Agv";
            this.columnHeader7.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader7.Width = 90;
            // 
            // columnHeader8
            // 
            this.columnHeader8.Text = "Sync Min";
            this.columnHeader8.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader8.Width = 90;
            // 
            // columnHeader9
            // 
            this.columnHeader9.Text = "Sync Max";
            this.columnHeader9.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader9.Width = 90;
            // 
            // columnHeader13
            // 
            this.columnHeader13.Text = "Sec";
            this.columnHeader13.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // MainForm_Client
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(1495, 722);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.m_tbState);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "MainForm_Client";
            this.Text = "Process Watcher Client";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.OnFormClosing);
            this.DoubleClick += new System.EventHandler(this.OntsmiShow_Click);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.OnMainFormKeyDown);
            this.contextMenuStrip1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.m_DgvSumView)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Timer m_TimerAutoStart;
        private System.Windows.Forms.Timer m_TimerState;
        private System.Windows.Forms.TextBox m_tbState;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem 열기ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 종료ToolStripMenuItem;
        private System.Windows.Forms.NotifyIcon notifyIcon1;
        private System.Windows.Forms.Button m_btnStartUpAdd;
        private System.Windows.Forms.Button m_btnStartUpRemove;
        private System.Windows.Forms.Button m_btnHide;
        private System.Windows.Forms.Timer m_TimerInterface;
        private System.Windows.Forms.Timer m_TimerProcessAutoStart;
        private System.Windows.Forms.DataGridView m_DgvSumView;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.ListView lstLogger;
        private System.Windows.Forms.ListView lstShMem;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader10;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader11;
        private System.Windows.Forms.ColumnHeader columnHeader12;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.ColumnHeader columnHeader7;
        private System.Windows.Forms.ColumnHeader columnHeader8;
        private System.Windows.Forms.ColumnHeader columnHeader9;
        private System.Windows.Forms.ColumnHeader columnHeader13;
        private System.Windows.Forms.Button m_btnProcessAutoStart;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox m_cbScanTime;
        private System.Windows.Forms.Button m_btnAutoStart;
        private System.Windows.Forms.Button m_btnStart;
        private System.Windows.Forms.Button m_btnStop;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Label m_lbProcessAutoState;
        private System.Windows.Forms.Label m_lbShareMemoryState;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private DataGridViewExTextBoxColumn sName;
        private DataGridViewExTextBoxColumn bState;
        private DataGridViewExTextBoxColumn bAutoStartState;
        private DataGridViewExTextBoxColumn sCPU;
        private DataGridViewExTextBoxColumn sRAM;
        private DataGridViewExTextBoxColumn sFileVersion;
        private DataGridViewExTextBoxColumn sProductVersion;
        private DataGridViewExTextBoxColumn btnStart;
        private DataGridViewExTextBoxColumn btnKill;
    }
}

