﻿namespace ProcessWatcher
{
    partial class MainForm_Server
    {
        /// <summary>
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 디자이너에서 생성한 코드

        /// <summary>
        /// 디자이너 지원에 필요한 메서드입니다. 
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm_Server));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            this.m_lvShareMemory = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.m_btnStart = new System.Windows.Forms.Button();
            this.m_btnStop = new System.Windows.Forms.Button();
            this.m_TimerAutoStart = new System.Windows.Forms.Timer(this.components);
            this.m_lvLog = new System.Windows.Forms.ListView();
            this.m_btnStartUpAdd = new System.Windows.Forms.Button();
            this.m_btnStartUpRemove = new System.Windows.Forms.Button();
            this.m_btnHide = new System.Windows.Forms.Button();
            this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.시작프로그램등록ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.시작프로그램등록ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.시작프로그램해제ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.shareMemoryOpenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.shareMemoryOpenToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.shareMemoryCloseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.열기ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.종료ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.m_btnAutoStart = new System.Windows.Forms.Button();
            this.m_cbScanTime = new System.Windows.Forms.ComboBox();
            this.m_lbScanTime = new System.Windows.Forms.Label();
            this.m_btnAutoProcessState = new System.Windows.Forms.Button();
            this.m_TimerProcessAutoStart = new System.Windows.Forms.Timer(this.components);
            this.m_lbProcessAutoStart = new System.Windows.Forms.Label();
            this.tabControl2 = new System.Windows.Forms.TabControl();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnLoadSettingData = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.m_DgvSumView = new System.Windows.Forms.DataGridView();
            this.sName = new ProcessWatcher.DataGridViewExTextBoxColumn();
            this.sProcessName = new ProcessWatcher.DataGridViewExTextBoxColumn();
            this.bState = new ProcessWatcher.DataGridViewExTextBoxColumn();
            this.bAutoStartState = new ProcessWatcher.DataGridViewExTextBoxColumn();
            this.sCPU = new ProcessWatcher.DataGridViewExTextBoxColumn();
            this.sRAM = new ProcessWatcher.DataGridViewExTextBoxColumn();
            this.GdiCount = new ProcessWatcher.DataGridViewExTextBoxColumn();
            this.sFileVersion = new ProcessWatcher.DataGridViewExTextBoxColumn();
            this.sProductVersion = new ProcessWatcher.DataGridViewExTextBoxColumn();
            this.btnStart = new ProcessWatcher.DataGridViewExTextBoxColumn();
            this.btnKill = new ProcessWatcher.DataGridViewExTextBoxColumn();
            this.m_lbProcessAutoState = new System.Windows.Forms.Label();
            this.m_lbShareMemoryState = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.label11 = new System.Windows.Forms.Label();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.btnSaveShareMemory = new System.Windows.Forms.Button();
            this.btnRemoveShareMemory = new System.Windows.Forms.Button();
            this.cbShareMemory = new System.Windows.Forms.ComboBox();
            this.label14 = new System.Windows.Forms.Label();
            this.tbConfigShareMemoryName = new System.Windows.Forms.TextBox();
            this.tbConfigShareMemorySize = new System.Windows.Forms.TextBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel8 = new System.Windows.Forms.TableLayoutPanel();
            this.cbClient = new System.Windows.Forms.ComboBox();
            this.label29 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.tableLayoutPanel9 = new System.Windows.Forms.TableLayoutPanel();
            this.btnSaveClient = new System.Windows.Forms.Button();
            this.btnRemoveClient = new System.Windows.Forms.Button();
            this.label20 = new System.Windows.Forms.Label();
            this.tbConfigClientProcess5 = new System.Windows.Forms.TextBox();
            this.tbConfigClientName = new System.Windows.Forms.TextBox();
            this.tbConfigClientIP = new System.Windows.Forms.TextBox();
            this.tbConfigClientProcess1 = new System.Windows.Forms.TextBox();
            this.tbConfigClientProcess2 = new System.Windows.Forms.TextBox();
            this.tbConfigClientProcess3 = new System.Windows.Forms.TextBox();
            this.tbConfigClientProcess4 = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel13 = new System.Windows.Forms.TableLayoutPanel();
            this.rbConfigPrecessCommandFalse = new System.Windows.Forms.RadioButton();
            this.rbConfigPrecessCommandTrue = new System.Windows.Forms.RadioButton();
            this.cbProcess = new System.Windows.Forms.ComboBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.tableLayoutPanel7 = new System.Windows.Forms.TableLayoutPanel();
            this.btnSaveProcess = new System.Windows.Forms.Button();
            this.btnRemoveProcess = new System.Windows.Forms.Button();
            this.label16 = new System.Windows.Forms.Label();
            this.tbConfigProcessName = new System.Windows.Forms.TextBox();
            this.tbConfigProcessPath = new System.Windows.Forms.TextBox();
            this.tbConfigProcessChild1 = new System.Windows.Forms.TextBox();
            this.tbConfigProcessChild2 = new System.Windows.Forms.TextBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.label23 = new System.Windows.Forms.Label();
            this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
            this.btnSaveVersion = new System.Windows.Forms.Button();
            this.btnRemoveVersion = new System.Windows.Forms.Button();
            this.cbVersion = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.tbConfigVersionName = new System.Windows.Forms.TextBox();
            this.tbConfigVersionVersion = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel12 = new System.Windows.Forms.TableLayoutPanel();
            this.rbConfigSettingAutoStartFalse = new System.Windows.Forms.RadioButton();
            this.rbConfigSettingAutoStartTrue = new System.Windows.Forms.RadioButton();
            this.tableLayoutPanel11 = new System.Windows.Forms.TableLayoutPanel();
            this.rbConfigSettingRestartFalse = new System.Windows.Forms.RadioButton();
            this.rbConfigSettingRestartTrue = new System.Windows.Forms.RadioButton();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.tableLayoutPanel10 = new System.Windows.Forms.TableLayoutPanel();
            this.btnSaveSetting = new System.Windows.Forms.Button();
            this.tbConfigSettingServerName = new System.Windows.Forms.TextBox();
            this.tbConfigSettingLogDriver = new System.Windows.Forms.TextBox();
            this.tbConfigSettingLogDays = new System.Windows.Forms.TextBox();
            this.dataGridViewExTextBoxColumn1 = new ProcessWatcher.DataGridViewExTextBoxColumn();
            this.dataGridViewExTextBoxColumn2 = new ProcessWatcher.DataGridViewExTextBoxColumn();
            this.dataGridViewExTextBoxColumn3 = new ProcessWatcher.DataGridViewExTextBoxColumn();
            this.dataGridViewExTextBoxColumn4 = new ProcessWatcher.DataGridViewExTextBoxColumn();
            this.dataGridViewExTextBoxColumn5 = new ProcessWatcher.DataGridViewExTextBoxColumn();
            this.dataGridViewExTextBoxColumn6 = new ProcessWatcher.DataGridViewExTextBoxColumn();
            this.dataGridViewExTextBoxColumn7 = new ProcessWatcher.DataGridViewExTextBoxColumn();
            this.dataGridViewExTextBoxColumn8 = new ProcessWatcher.DataGridViewExTextBoxColumn();
            this.dataGridViewExTextBoxColumn9 = new ProcessWatcher.DataGridViewExTextBoxColumn();
            this.dataGridViewExTextBoxColumn10 = new ProcessWatcher.DataGridViewExTextBoxColumn();
            this.dataGridViewExTextBoxColumn11 = new ProcessWatcher.DataGridViewExTextBoxColumn();
            this.contextMenuStrip1.SuspendLayout();
            this.tabControl2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.m_DgvSumView)).BeginInit();
            this.tabPage4.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.tableLayoutPanel8.SuspendLayout();
            this.tableLayoutPanel9.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.tableLayoutPanel13.SuspendLayout();
            this.tableLayoutPanel7.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            this.tableLayoutPanel6.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel12.SuspendLayout();
            this.tableLayoutPanel11.SuspendLayout();
            this.tableLayoutPanel10.SuspendLayout();
            this.SuspendLayout();
            // 
            // m_lvShareMemory
            // 
            this.m_lvShareMemory.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3});
            this.m_lvShareMemory.HideSelection = false;
            this.m_lvShareMemory.Location = new System.Drawing.Point(6, 6);
            this.m_lvShareMemory.Name = "m_lvShareMemory";
            this.m_lvShareMemory.Size = new System.Drawing.Size(1460, 231);
            this.m_lvShareMemory.TabIndex = 1;
            this.m_lvShareMemory.UseCompatibleStateImageBehavior = false;
            this.m_lvShareMemory.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "";
            this.columnHeader1.Width = 1;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "ShareMemory Name";
            this.columnHeader2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader2.Width = 200;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Size";
            this.columnHeader3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader3.Width = 70;
            // 
            // m_btnStart
            // 
            this.m_btnStart.Location = new System.Drawing.Point(1253, 624);
            this.m_btnStart.Name = "m_btnStart";
            this.m_btnStart.Size = new System.Drawing.Size(105, 40);
            this.m_btnStart.TabIndex = 2;
            this.m_btnStart.Text = "Start";
            this.m_btnStart.UseVisualStyleBackColor = true;
            this.m_btnStart.Click += new System.EventHandler(this.OnStart);
            // 
            // m_btnStop
            // 
            this.m_btnStop.Location = new System.Drawing.Point(1363, 624);
            this.m_btnStop.Name = "m_btnStop";
            this.m_btnStop.Size = new System.Drawing.Size(105, 40);
            this.m_btnStop.TabIndex = 2;
            this.m_btnStop.Text = "Stop";
            this.m_btnStop.UseVisualStyleBackColor = true;
            this.m_btnStop.Click += new System.EventHandler(this.OnStop);
            // 
            // m_TimerAutoStart
            // 
            this.m_TimerAutoStart.Interval = 5000;
            this.m_TimerAutoStart.Tick += new System.EventHandler(this.TimerAutoStart);
            // 
            // m_lvLog
            // 
            this.m_lvLog.HideSelection = false;
            this.m_lvLog.Location = new System.Drawing.Point(6, 243);
            this.m_lvLog.Name = "m_lvLog";
            this.m_lvLog.Size = new System.Drawing.Size(1460, 375);
            this.m_lvLog.TabIndex = 3;
            this.m_lvLog.UseCompatibleStateImageBehavior = false;
            // 
            // m_btnStartUpAdd
            // 
            this.m_btnStartUpAdd.Location = new System.Drawing.Point(5, 624);
            this.m_btnStartUpAdd.Name = "m_btnStartUpAdd";
            this.m_btnStartUpAdd.Size = new System.Drawing.Size(155, 40);
            this.m_btnStartUpAdd.TabIndex = 15;
            this.m_btnStartUpAdd.Text = "Add StartUp";
            this.m_btnStartUpAdd.UseVisualStyleBackColor = true;
            this.m_btnStartUpAdd.Click += new System.EventHandler(this.OnStartUpAdd);
            // 
            // m_btnStartUpRemove
            // 
            this.m_btnStartUpRemove.Location = new System.Drawing.Point(166, 624);
            this.m_btnStartUpRemove.Name = "m_btnStartUpRemove";
            this.m_btnStartUpRemove.Size = new System.Drawing.Size(155, 40);
            this.m_btnStartUpRemove.TabIndex = 16;
            this.m_btnStartUpRemove.Text = "Remove Startup";
            this.m_btnStartUpRemove.UseVisualStyleBackColor = true;
            this.m_btnStartUpRemove.Click += new System.EventHandler(this.OnStartUpRemove);
            // 
            // m_btnHide
            // 
            this.m_btnHide.Location = new System.Drawing.Point(327, 624);
            this.m_btnHide.Name = "m_btnHide";
            this.m_btnHide.Size = new System.Drawing.Size(105, 40);
            this.m_btnHide.TabIndex = 17;
            this.m_btnHide.Text = "Hide";
            this.m_btnHide.UseVisualStyleBackColor = true;
            this.m_btnHide.Click += new System.EventHandler(this.OnHide);
            // 
            // notifyIcon1
            // 
            this.notifyIcon1.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon1.Icon")));
            this.notifyIcon1.Text = "Esol ShareMemory Sync";
            this.notifyIcon1.Visible = true;
            this.notifyIcon1.MouseClick += new System.Windows.Forms.MouseEventHandler(this.OnMouseClick);
            this.notifyIcon1.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.OnOpenClick);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.시작프로그램등록ToolStripMenuItem,
            this.shareMemoryOpenToolStripMenuItem,
            this.열기ToolStripMenuItem,
            this.종료ToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(199, 132);
            // 
            // 시작프로그램등록ToolStripMenuItem
            // 
            this.시작프로그램등록ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.시작프로그램등록ToolStripMenuItem1,
            this.시작프로그램해제ToolStripMenuItem});
            this.시작프로그램등록ToolStripMenuItem.Name = "시작프로그램등록ToolStripMenuItem";
            this.시작프로그램등록ToolStripMenuItem.Size = new System.Drawing.Size(198, 32);
            this.시작프로그램등록ToolStripMenuItem.Text = "StartUp";
            // 
            // 시작프로그램등록ToolStripMenuItem1
            // 
            this.시작프로그램등록ToolStripMenuItem1.Name = "시작프로그램등록ToolStripMenuItem1";
            this.시작프로그램등록ToolStripMenuItem1.Size = new System.Drawing.Size(248, 34);
            this.시작프로그램등록ToolStripMenuItem1.Text = "StartUp Add";
            this.시작프로그램등록ToolStripMenuItem1.Click += new System.EventHandler(this.OnStartUpAdd);
            // 
            // 시작프로그램해제ToolStripMenuItem
            // 
            this.시작프로그램해제ToolStripMenuItem.Name = "시작프로그램해제ToolStripMenuItem";
            this.시작프로그램해제ToolStripMenuItem.Size = new System.Drawing.Size(248, 34);
            this.시작프로그램해제ToolStripMenuItem.Text = "StartUp Remove";
            this.시작프로그램해제ToolStripMenuItem.Click += new System.EventHandler(this.OnStartUpRemove);
            // 
            // shareMemoryOpenToolStripMenuItem
            // 
            this.shareMemoryOpenToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.shareMemoryOpenToolStripMenuItem1,
            this.shareMemoryCloseToolStripMenuItem});
            this.shareMemoryOpenToolStripMenuItem.Name = "shareMemoryOpenToolStripMenuItem";
            this.shareMemoryOpenToolStripMenuItem.Size = new System.Drawing.Size(198, 32);
            this.shareMemoryOpenToolStripMenuItem.Text = "ShareMemory";
            // 
            // shareMemoryOpenToolStripMenuItem1
            // 
            this.shareMemoryOpenToolStripMenuItem1.Name = "shareMemoryOpenToolStripMenuItem1";
            this.shareMemoryOpenToolStripMenuItem1.Size = new System.Drawing.Size(279, 34);
            this.shareMemoryOpenToolStripMenuItem1.Text = "ShareMemory Open";
            this.shareMemoryOpenToolStripMenuItem1.Click += new System.EventHandler(this.OnStart);
            // 
            // shareMemoryCloseToolStripMenuItem
            // 
            this.shareMemoryCloseToolStripMenuItem.Name = "shareMemoryCloseToolStripMenuItem";
            this.shareMemoryCloseToolStripMenuItem.Size = new System.Drawing.Size(279, 34);
            this.shareMemoryCloseToolStripMenuItem.Text = "ShareMemory Close";
            this.shareMemoryCloseToolStripMenuItem.Click += new System.EventHandler(this.OnStop);
            // 
            // 열기ToolStripMenuItem
            // 
            this.열기ToolStripMenuItem.Name = "열기ToolStripMenuItem";
            this.열기ToolStripMenuItem.Size = new System.Drawing.Size(198, 32);
            this.열기ToolStripMenuItem.Text = "Show(&O)";
            this.열기ToolStripMenuItem.Click += new System.EventHandler(this.OnContextOpen);
            // 
            // 종료ToolStripMenuItem
            // 
            this.종료ToolStripMenuItem.Name = "종료ToolStripMenuItem";
            this.종료ToolStripMenuItem.Size = new System.Drawing.Size(198, 32);
            this.종료ToolStripMenuItem.Text = "Exit(&E)";
            this.종료ToolStripMenuItem.Click += new System.EventHandler(this.OnContextClose);
            // 
            // m_btnAutoStart
            // 
            this.m_btnAutoStart.Location = new System.Drawing.Point(1087, 624);
            this.m_btnAutoStart.Name = "m_btnAutoStart";
            this.m_btnAutoStart.Size = new System.Drawing.Size(160, 40);
            this.m_btnAutoStart.TabIndex = 2;
            this.m_btnAutoStart.Text = "AutoStart Enable";
            this.m_btnAutoStart.UseVisualStyleBackColor = true;
            this.m_btnAutoStart.Click += new System.EventHandler(this.OnAutoStartChange);
            // 
            // m_cbScanTime
            // 
            this.m_cbScanTime.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.m_cbScanTime.FormattingEnabled = true;
            this.m_cbScanTime.Items.AddRange(new object[] {
            "1s",
            "2s",
            "3s",
            "4s",
            "5s",
            "6s",
            "7s",
            "8s",
            "9s",
            "10s",
            "11s",
            "12s",
            "13s",
            "14s",
            "15s",
            "16s",
            "17s",
            "18s",
            "19s",
            "20s"});
            this.m_cbScanTime.Location = new System.Drawing.Point(1357, 632);
            this.m_cbScanTime.Name = "m_cbScanTime";
            this.m_cbScanTime.Size = new System.Drawing.Size(121, 26);
            this.m_cbScanTime.TabIndex = 20;
            this.m_cbScanTime.Visible = false;
            this.m_cbScanTime.SelectedIndexChanged += new System.EventHandler(this.OnComboSelectChange);
            // 
            // m_lbScanTime
            // 
            this.m_lbScanTime.AutoSize = true;
            this.m_lbScanTime.Location = new System.Drawing.Point(1246, 635);
            this.m_lbScanTime.Name = "m_lbScanTime";
            this.m_lbScanTime.Size = new System.Drawing.Size(105, 18);
            this.m_lbScanTime.TabIndex = 21;
            this.m_lbScanTime.Text = "Scan Time :";
            this.m_lbScanTime.Visible = false;
            // 
            // m_btnAutoProcessState
            // 
            this.m_btnAutoProcessState.Location = new System.Drawing.Point(1663, 624);
            this.m_btnAutoProcessState.Name = "m_btnAutoProcessState";
            this.m_btnAutoProcessState.Size = new System.Drawing.Size(89, 40);
            this.m_btnAutoProcessState.TabIndex = 22;
            this.m_btnAutoProcessState.Text = "Enable";
            this.m_btnAutoProcessState.UseVisualStyleBackColor = true;
            this.m_btnAutoProcessState.Click += new System.EventHandler(this.OnProcessAutoStart);
            // 
            // m_TimerProcessAutoStart
            // 
            this.m_TimerProcessAutoStart.Interval = 1000;
            this.m_TimerProcessAutoStart.Tick += new System.EventHandler(this.TimerProcessAutoStart_Tick);
            // 
            // m_lbProcessAutoStart
            // 
            this.m_lbProcessAutoStart.AutoSize = true;
            this.m_lbProcessAutoStart.Location = new System.Drawing.Point(1488, 636);
            this.m_lbProcessAutoStart.Name = "m_lbProcessAutoStart";
            this.m_lbProcessAutoStart.Size = new System.Drawing.Size(169, 18);
            this.m_lbProcessAutoStart.TabIndex = 23;
            this.m_lbProcessAutoStart.Text = "Process AutoStart :";
            // 
            // tabControl2
            // 
            this.tabControl2.Controls.Add(this.tabPage3);
            this.tabControl2.Controls.Add(this.tabPage4);
            this.tabControl2.Controls.Add(this.tabPage1);
            this.tabControl2.Location = new System.Drawing.Point(12, 12);
            this.tabControl2.Name = "tabControl2";
            this.tabControl2.SelectedIndex = 0;
            this.tabControl2.Size = new System.Drawing.Size(1765, 702);
            this.tabControl2.TabIndex = 25;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.label5);
            this.tabPage3.Controls.Add(this.label4);
            this.tabPage3.Controls.Add(this.label3);
            this.tabPage3.Controls.Add(this.panel3);
            this.tabPage3.Controls.Add(this.panel2);
            this.tabPage3.Controls.Add(this.panel1);
            this.tabPage3.Controls.Add(this.btnLoadSettingData);
            this.tabPage3.Controls.Add(this.button2);
            this.tabPage3.Controls.Add(this.button3);
            this.tabPage3.Controls.Add(this.button1);
            this.tabPage3.Controls.Add(this.m_DgvSumView);
            this.tabPage3.Controls.Add(this.m_lbProcessAutoState);
            this.tabPage3.Controls.Add(this.m_lbShareMemoryState);
            this.tabPage3.Controls.Add(this.label2);
            this.tabPage3.Controls.Add(this.m_lbProcessAutoStart);
            this.tabPage3.Controls.Add(this.label1);
            this.tabPage3.Controls.Add(this.m_btnAutoProcessState);
            this.tabPage3.Controls.Add(this.m_cbScanTime);
            this.tabPage3.Controls.Add(this.m_lbScanTime);
            this.tabPage3.Location = new System.Drawing.Point(4, 28);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(1757, 670);
            this.tabPage3.TabIndex = 0;
            this.tabPage3.Text = "Process Watcher";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(1441, 16);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(177, 18);
            this.label5.TabIndex = 36;
            this.label5.Text = "Version not matched";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(1329, 16);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(57, 18);
            this.label4.TabIndex = 35;
            this.label4.Text = "Offline";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(1214, 16);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(57, 18);
            this.label3.TabIndex = 34;
            this.label3.Text = "Online";
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.panel3.Location = new System.Drawing.Point(1408, 12);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(27, 26);
            this.panel3.TabIndex = 33;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Red;
            this.panel2.Location = new System.Drawing.Point(1296, 12);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(27, 26);
            this.panel2.TabIndex = 32;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.panel1.Location = new System.Drawing.Point(1180, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(27, 26);
            this.panel1.TabIndex = 31;
            // 
            // btnLoadSettingData
            // 
            this.btnLoadSettingData.Location = new System.Drawing.Point(327, 624);
            this.btnLoadSettingData.Name = "btnLoadSettingData";
            this.btnLoadSettingData.Size = new System.Drawing.Size(139, 40);
            this.btnLoadSettingData.TabIndex = 30;
            this.btnLoadSettingData.Text = "Load Setting";
            this.btnLoadSettingData.UseVisualStyleBackColor = true;
            this.btnLoadSettingData.Click += new System.EventHandler(this.btnLoadSettingData_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(166, 624);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(155, 40);
            this.button2.TabIndex = 29;
            this.button2.Text = "Remove Startup";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.OnStartUpRemove);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(5, 624);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(155, 40);
            this.button3.TabIndex = 28;
            this.button3.Text = "Add StartUp";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.OnStartUpAdd);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(497, 624);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(105, 40);
            this.button1.TabIndex = 27;
            this.button1.Text = "Hide";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.OnHide);
            // 
            // m_DgvSumView
            // 
            this.m_DgvSumView.AllowUserToAddRows = false;
            this.m_DgvSumView.AllowUserToDeleteRows = false;
            this.m_DgvSumView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.m_DgvSumView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.sName,
            this.sProcessName,
            this.bState,
            this.bAutoStartState,
            this.sCPU,
            this.sRAM,
            this.GdiCount,
            this.sFileVersion,
            this.sProductVersion,
            this.btnStart,
            this.btnKill});
            this.m_DgvSumView.Location = new System.Drawing.Point(6, 61);
            this.m_DgvSumView.Name = "m_DgvSumView";
            this.m_DgvSumView.ReadOnly = true;
            this.m_DgvSumView.RowHeadersWidth = 62;
            this.m_DgvSumView.RowTemplate.Height = 30;
            this.m_DgvSumView.Size = new System.Drawing.Size(1744, 557);
            this.m_DgvSumView.TabIndex = 26;
            this.m_DgvSumView.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.OnCellClick);
            this.m_DgvSumView.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.OnDoubleClick);
            // 
            // sName
            // 
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.sName.DefaultCellStyle = dataGridViewCellStyle1;
            this.sName.Frozen = true;
            this.sName.HeaderText = "Name";
            this.sName.MinimumWidth = 8;
            this.sName.Name = "sName";
            this.sName.ReadOnly = true;
            this.sName.Width = 150;
            // 
            // sProcessName
            // 
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.sProcessName.DefaultCellStyle = dataGridViewCellStyle2;
            this.sProcessName.Frozen = true;
            this.sProcessName.HeaderText = "ProcessName";
            this.sProcessName.MinimumWidth = 8;
            this.sProcessName.Name = "sProcessName";
            this.sProcessName.ReadOnly = true;
            this.sProcessName.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.sProcessName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            this.sProcessName.Width = 200;
            // 
            // bState
            // 
            this.bState.Frozen = true;
            this.bState.HeaderText = "State";
            this.bState.MinimumWidth = 8;
            this.bState.Name = "bState";
            this.bState.ReadOnly = true;
            this.bState.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.bState.Width = 50;
            // 
            // bAutoStartState
            // 
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.bAutoStartState.DefaultCellStyle = dataGridViewCellStyle3;
            this.bAutoStartState.Frozen = true;
            this.bAutoStartState.HeaderText = "AutoStartMode";
            this.bAutoStartState.MinimumWidth = 8;
            this.bAutoStartState.Name = "bAutoStartState";
            this.bAutoStartState.ReadOnly = true;
            this.bAutoStartState.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.bAutoStartState.Width = 150;
            // 
            // sCPU
            // 
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.sCPU.DefaultCellStyle = dataGridViewCellStyle4;
            this.sCPU.Frozen = true;
            this.sCPU.HeaderText = "CPU";
            this.sCPU.MinimumWidth = 8;
            this.sCPU.Name = "sCPU";
            this.sCPU.ReadOnly = true;
            this.sCPU.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.sCPU.Width = 150;
            // 
            // sRAM
            // 
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.sRAM.DefaultCellStyle = dataGridViewCellStyle5;
            this.sRAM.Frozen = true;
            this.sRAM.HeaderText = "RAM";
            this.sRAM.MinimumWidth = 8;
            this.sRAM.Name = "sRAM";
            this.sRAM.ReadOnly = true;
            this.sRAM.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.sRAM.Width = 150;
            // 
            // GdiCount
            // 
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.GdiCount.DefaultCellStyle = dataGridViewCellStyle6;
            this.GdiCount.Frozen = true;
            this.GdiCount.HeaderText = "GdiCount";
            this.GdiCount.MinimumWidth = 8;
            this.GdiCount.Name = "GdiCount";
            this.GdiCount.ReadOnly = true;
            this.GdiCount.Width = 150;
            // 
            // sFileVersion
            // 
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.sFileVersion.DefaultCellStyle = dataGridViewCellStyle7;
            this.sFileVersion.Frozen = true;
            this.sFileVersion.HeaderText = "FileVersion";
            this.sFileVersion.MinimumWidth = 8;
            this.sFileVersion.Name = "sFileVersion";
            this.sFileVersion.ReadOnly = true;
            this.sFileVersion.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.sFileVersion.Width = 150;
            // 
            // sProductVersion
            // 
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.sProductVersion.DefaultCellStyle = dataGridViewCellStyle8;
            this.sProductVersion.Frozen = true;
            this.sProductVersion.HeaderText = "ProductVersion";
            this.sProductVersion.MinimumWidth = 8;
            this.sProductVersion.Name = "sProductVersion";
            this.sProductVersion.ReadOnly = true;
            this.sProductVersion.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.sProductVersion.Width = 150;
            // 
            // btnStart
            // 
            this.btnStart.Frozen = true;
            this.btnStart.HeaderText = "Start";
            this.btnStart.MinimumWidth = 8;
            this.btnStart.Name = "btnStart";
            this.btnStart.ReadOnly = true;
            this.btnStart.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.btnStart.Width = 150;
            // 
            // btnKill
            // 
            this.btnKill.Frozen = true;
            this.btnKill.HeaderText = "Kill";
            this.btnKill.MinimumWidth = 8;
            this.btnKill.Name = "btnKill";
            this.btnKill.ReadOnly = true;
            this.btnKill.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.btnKill.Width = 150;
            // 
            // m_lbProcessAutoState
            // 
            this.m_lbProcessAutoState.AutoSize = true;
            this.m_lbProcessAutoState.Location = new System.Drawing.Point(207, 40);
            this.m_lbProcessAutoState.Name = "m_lbProcessAutoState";
            this.m_lbProcessAutoState.Size = new System.Drawing.Size(0, 18);
            this.m_lbProcessAutoState.TabIndex = 1;
            // 
            // m_lbShareMemoryState
            // 
            this.m_lbShareMemoryState.AutoSize = true;
            this.m_lbShareMemoryState.Location = new System.Drawing.Point(207, 12);
            this.m_lbShareMemoryState.Name = "m_lbShareMemoryState";
            this.m_lbShareMemoryState.Size = new System.Drawing.Size(0, 18);
            this.m_lbShareMemoryState.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(8, 40);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(179, 18);
            this.label2.TabIndex = 0;
            this.label2.Text = "Process Auto State :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(181, 18);
            this.label1.TabIndex = 0;
            this.label1.Text = "ShareMemory State :";
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.m_lvShareMemory);
            this.tabPage4.Controls.Add(this.m_btnStart);
            this.tabPage4.Controls.Add(this.m_btnAutoStart);
            this.tabPage4.Controls.Add(this.m_btnStop);
            this.tabPage4.Controls.Add(this.m_lvLog);
            this.tabPage4.Controls.Add(this.m_btnHide);
            this.tabPage4.Controls.Add(this.m_btnStartUpRemove);
            this.tabPage4.Controls.Add(this.m_btnStartUpAdd);
            this.tabPage4.Location = new System.Drawing.Point(4, 28);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(1757, 670);
            this.tabPage4.TabIndex = 1;
            this.tabPage4.Text = "ShareMemory";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.groupBox2);
            this.tabPage1.Controls.Add(this.groupBox4);
            this.tabPage1.Controls.Add(this.groupBox3);
            this.tabPage1.Controls.Add(this.groupBox5);
            this.tabPage1.Controls.Add(this.groupBox1);
            this.tabPage1.Location = new System.Drawing.Point(4, 28);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1757, 670);
            this.tabPage1.TabIndex = 2;
            this.tabPage1.Text = "Config";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.tableLayoutPanel2);
            this.groupBox2.Location = new System.Drawing.Point(1129, 19);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(365, 168);
            this.groupBox2.TabIndex = 3;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Share Memory";
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 60F));
            this.tableLayoutPanel2.Controls.Add(this.label11, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel3, 1, 3);
            this.tableLayoutPanel2.Controls.Add(this.cbShareMemory, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.label14, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.tbConfigShareMemoryName, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.tbConfigShareMemorySize, 1, 2);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 24);
            this.tableLayoutPanel2.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 4;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(359, 141);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label11.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold);
            this.label11.Location = new System.Drawing.Point(3, 35);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(137, 35);
            this.label11.TabIndex = 3;
            this.label11.Text = "Name";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 2;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.46296F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 49.53704F));
            this.tableLayoutPanel3.Controls.Add(this.btnSaveShareMemory, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.btnRemoveShareMemory, 0, 0);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(143, 105);
            this.tableLayoutPanel3.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 1;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(216, 36);
            this.tableLayoutPanel3.TabIndex = 6;
            // 
            // btnSaveShareMemory
            // 
            this.btnSaveShareMemory.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnSaveShareMemory.Location = new System.Drawing.Point(111, 3);
            this.btnSaveShareMemory.Name = "btnSaveShareMemory";
            this.btnSaveShareMemory.Size = new System.Drawing.Size(102, 30);
            this.btnSaveShareMemory.TabIndex = 2;
            this.btnSaveShareMemory.Text = "SAVE";
            this.btnSaveShareMemory.UseVisualStyleBackColor = true;
            this.btnSaveShareMemory.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnRemoveShareMemory
            // 
            this.btnRemoveShareMemory.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnRemoveShareMemory.Location = new System.Drawing.Point(3, 3);
            this.btnRemoveShareMemory.Name = "btnRemoveShareMemory";
            this.btnRemoveShareMemory.Size = new System.Drawing.Size(102, 30);
            this.btnRemoveShareMemory.TabIndex = 1;
            this.btnRemoveShareMemory.Text = "REMOVE";
            this.btnRemoveShareMemory.UseVisualStyleBackColor = true;
            this.btnRemoveShareMemory.Click += new System.EventHandler(this.btnRemove_Click);
            // 
            // cbShareMemory
            // 
            this.cbShareMemory.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cbShareMemory.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbShareMemory.FormattingEnabled = true;
            this.cbShareMemory.Location = new System.Drawing.Point(146, 3);
            this.cbShareMemory.Name = "cbShareMemory";
            this.cbShareMemory.Size = new System.Drawing.Size(210, 26);
            this.cbShareMemory.TabIndex = 8;
            this.cbShareMemory.SelectedIndexChanged += new System.EventHandler(this.cbShareMemory_SelectedIndexChanged);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label14.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold);
            this.label14.Location = new System.Drawing.Point(3, 70);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(137, 35);
            this.label14.TabIndex = 9;
            this.label14.Text = "Size";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tbConfigShareMemoryName
            // 
            this.tbConfigShareMemoryName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbConfigShareMemoryName.Location = new System.Drawing.Point(146, 38);
            this.tbConfigShareMemoryName.Name = "tbConfigShareMemoryName";
            this.tbConfigShareMemoryName.Size = new System.Drawing.Size(210, 28);
            this.tbConfigShareMemoryName.TabIndex = 12;
            // 
            // tbConfigShareMemorySize
            // 
            this.tbConfigShareMemorySize.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbConfigShareMemorySize.Location = new System.Drawing.Point(146, 73);
            this.tbConfigShareMemorySize.Name = "tbConfigShareMemorySize";
            this.tbConfigShareMemorySize.Size = new System.Drawing.Size(210, 28);
            this.tbConfigShareMemorySize.TabIndex = 13;
            this.tbConfigShareMemorySize.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbConfigOnlyNumbers_KeyPress);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.tableLayoutPanel8);
            this.groupBox4.Location = new System.Drawing.Point(758, 19);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(365, 352);
            this.groupBox4.TabIndex = 5;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Client";
            // 
            // tableLayoutPanel8
            // 
            this.tableLayoutPanel8.ColumnCount = 2;
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 60F));
            this.tableLayoutPanel8.Controls.Add(this.cbClient, 1, 0);
            this.tableLayoutPanel8.Controls.Add(this.label29, 0, 1);
            this.tableLayoutPanel8.Controls.Add(this.label30, 0, 2);
            this.tableLayoutPanel8.Controls.Add(this.label31, 0, 3);
            this.tableLayoutPanel8.Controls.Add(this.label32, 0, 4);
            this.tableLayoutPanel8.Controls.Add(this.label33, 0, 5);
            this.tableLayoutPanel8.Controls.Add(this.label34, 0, 6);
            this.tableLayoutPanel8.Controls.Add(this.tableLayoutPanel9, 1, 8);
            this.tableLayoutPanel8.Controls.Add(this.label20, 0, 7);
            this.tableLayoutPanel8.Controls.Add(this.tbConfigClientProcess5, 1, 7);
            this.tableLayoutPanel8.Controls.Add(this.tbConfigClientName, 1, 1);
            this.tableLayoutPanel8.Controls.Add(this.tbConfigClientIP, 1, 2);
            this.tableLayoutPanel8.Controls.Add(this.tbConfigClientProcess1, 1, 3);
            this.tableLayoutPanel8.Controls.Add(this.tbConfigClientProcess2, 1, 4);
            this.tableLayoutPanel8.Controls.Add(this.tbConfigClientProcess3, 1, 5);
            this.tableLayoutPanel8.Controls.Add(this.tbConfigClientProcess4, 1, 6);
            this.tableLayoutPanel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel8.Location = new System.Drawing.Point(3, 24);
            this.tableLayoutPanel8.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel8.Name = "tableLayoutPanel8";
            this.tableLayoutPanel8.RowCount = 9;
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111F));
            this.tableLayoutPanel8.Size = new System.Drawing.Size(359, 325);
            this.tableLayoutPanel8.TabIndex = 0;
            // 
            // cbClient
            // 
            this.cbClient.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cbClient.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbClient.FormattingEnabled = true;
            this.cbClient.Location = new System.Drawing.Point(146, 3);
            this.cbClient.Name = "cbClient";
            this.cbClient.Size = new System.Drawing.Size(210, 26);
            this.cbClient.TabIndex = 10;
            this.cbClient.SelectedIndexChanged += new System.EventHandler(this.cbClient_SelectedIndexChanged);
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label29.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold);
            this.label29.Location = new System.Drawing.Point(3, 36);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(137, 36);
            this.label29.TabIndex = 1;
            this.label29.Text = "Name";
            this.label29.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label30.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold);
            this.label30.Location = new System.Drawing.Point(3, 72);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(137, 36);
            this.label30.TabIndex = 2;
            this.label30.Text = "IP";
            this.label30.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label31.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold);
            this.label31.Location = new System.Drawing.Point(3, 108);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(137, 36);
            this.label31.TabIndex = 3;
            this.label31.Text = "Process 1";
            this.label31.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label32.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold);
            this.label32.Location = new System.Drawing.Point(3, 144);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(137, 36);
            this.label32.TabIndex = 4;
            this.label32.Text = "Process 2";
            this.label32.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label33.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold);
            this.label33.Location = new System.Drawing.Point(3, 180);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(137, 36);
            this.label33.TabIndex = 5;
            this.label33.Text = "Process 3";
            this.label33.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label34.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold);
            this.label34.Location = new System.Drawing.Point(3, 216);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(137, 36);
            this.label34.TabIndex = 0;
            this.label34.Text = "Process 4";
            this.label34.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tableLayoutPanel9
            // 
            this.tableLayoutPanel9.ColumnCount = 2;
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel9.Controls.Add(this.btnSaveClient, 0, 0);
            this.tableLayoutPanel9.Controls.Add(this.btnRemoveClient, 0, 0);
            this.tableLayoutPanel9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel9.Location = new System.Drawing.Point(143, 288);
            this.tableLayoutPanel9.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel9.Name = "tableLayoutPanel9";
            this.tableLayoutPanel9.RowCount = 1;
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel9.Size = new System.Drawing.Size(216, 37);
            this.tableLayoutPanel9.TabIndex = 6;
            // 
            // btnSaveClient
            // 
            this.btnSaveClient.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnSaveClient.Location = new System.Drawing.Point(111, 3);
            this.btnSaveClient.Name = "btnSaveClient";
            this.btnSaveClient.Size = new System.Drawing.Size(102, 31);
            this.btnSaveClient.TabIndex = 2;
            this.btnSaveClient.Text = "SAVE";
            this.btnSaveClient.UseVisualStyleBackColor = true;
            this.btnSaveClient.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnRemoveClient
            // 
            this.btnRemoveClient.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnRemoveClient.Location = new System.Drawing.Point(3, 3);
            this.btnRemoveClient.Name = "btnRemoveClient";
            this.btnRemoveClient.Size = new System.Drawing.Size(102, 31);
            this.btnRemoveClient.TabIndex = 1;
            this.btnRemoveClient.Text = "REMOVE";
            this.btnRemoveClient.UseVisualStyleBackColor = true;
            this.btnRemoveClient.Click += new System.EventHandler(this.btnRemove_Click);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label20.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold);
            this.label20.Location = new System.Drawing.Point(3, 252);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(137, 36);
            this.label20.TabIndex = 11;
            this.label20.Text = "Process 5";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tbConfigClientProcess5
            // 
            this.tbConfigClientProcess5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbConfigClientProcess5.Location = new System.Drawing.Point(146, 255);
            this.tbConfigClientProcess5.Name = "tbConfigClientProcess5";
            this.tbConfigClientProcess5.Size = new System.Drawing.Size(210, 28);
            this.tbConfigClientProcess5.TabIndex = 12;
            // 
            // tbConfigClientName
            // 
            this.tbConfigClientName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbConfigClientName.Location = new System.Drawing.Point(146, 39);
            this.tbConfigClientName.Name = "tbConfigClientName";
            this.tbConfigClientName.Size = new System.Drawing.Size(210, 28);
            this.tbConfigClientName.TabIndex = 13;
            // 
            // tbConfigClientIP
            // 
            this.tbConfigClientIP.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbConfigClientIP.Location = new System.Drawing.Point(146, 75);
            this.tbConfigClientIP.Name = "tbConfigClientIP";
            this.tbConfigClientIP.Size = new System.Drawing.Size(210, 28);
            this.tbConfigClientIP.TabIndex = 14;
            // 
            // tbConfigClientProcess1
            // 
            this.tbConfigClientProcess1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbConfigClientProcess1.Location = new System.Drawing.Point(146, 111);
            this.tbConfigClientProcess1.Name = "tbConfigClientProcess1";
            this.tbConfigClientProcess1.Size = new System.Drawing.Size(210, 28);
            this.tbConfigClientProcess1.TabIndex = 15;
            // 
            // tbConfigClientProcess2
            // 
            this.tbConfigClientProcess2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbConfigClientProcess2.Location = new System.Drawing.Point(146, 147);
            this.tbConfigClientProcess2.Name = "tbConfigClientProcess2";
            this.tbConfigClientProcess2.Size = new System.Drawing.Size(210, 28);
            this.tbConfigClientProcess2.TabIndex = 16;
            // 
            // tbConfigClientProcess3
            // 
            this.tbConfigClientProcess3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbConfigClientProcess3.Location = new System.Drawing.Point(146, 183);
            this.tbConfigClientProcess3.Name = "tbConfigClientProcess3";
            this.tbConfigClientProcess3.Size = new System.Drawing.Size(210, 28);
            this.tbConfigClientProcess3.TabIndex = 17;
            // 
            // tbConfigClientProcess4
            // 
            this.tbConfigClientProcess4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbConfigClientProcess4.Location = new System.Drawing.Point(146, 219);
            this.tbConfigClientProcess4.Name = "tbConfigClientProcess4";
            this.tbConfigClientProcess4.Size = new System.Drawing.Size(210, 28);
            this.tbConfigClientProcess4.TabIndex = 18;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.tableLayoutPanel4);
            this.groupBox3.Location = new System.Drawing.Point(387, 19);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(365, 276);
            this.groupBox3.TabIndex = 4;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Process";
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 2;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 60F));
            this.tableLayoutPanel4.Controls.Add(this.tableLayoutPanel13, 1, 3);
            this.tableLayoutPanel4.Controls.Add(this.cbProcess, 1, 0);
            this.tableLayoutPanel4.Controls.Add(this.label17, 0, 1);
            this.tableLayoutPanel4.Controls.Add(this.label18, 0, 2);
            this.tableLayoutPanel4.Controls.Add(this.label27, 0, 3);
            this.tableLayoutPanel4.Controls.Add(this.label28, 0, 4);
            this.tableLayoutPanel4.Controls.Add(this.tableLayoutPanel7, 1, 6);
            this.tableLayoutPanel4.Controls.Add(this.label16, 0, 5);
            this.tableLayoutPanel4.Controls.Add(this.tbConfigProcessName, 1, 1);
            this.tableLayoutPanel4.Controls.Add(this.tbConfigProcessPath, 1, 2);
            this.tableLayoutPanel4.Controls.Add(this.tbConfigProcessChild1, 1, 4);
            this.tableLayoutPanel4.Controls.Add(this.tbConfigProcessChild2, 1, 5);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(3, 24);
            this.tableLayoutPanel4.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 7;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28572F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28572F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28572F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28572F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28572F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(359, 249);
            this.tableLayoutPanel4.TabIndex = 0;
            // 
            // tableLayoutPanel13
            // 
            this.tableLayoutPanel13.ColumnCount = 2;
            this.tableLayoutPanel13.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel13.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel13.Controls.Add(this.rbConfigPrecessCommandFalse, 1, 0);
            this.tableLayoutPanel13.Controls.Add(this.rbConfigPrecessCommandTrue, 0, 0);
            this.tableLayoutPanel13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel13.Location = new System.Drawing.Point(143, 105);
            this.tableLayoutPanel13.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel13.Name = "tableLayoutPanel13";
            this.tableLayoutPanel13.RowCount = 1;
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel13.Size = new System.Drawing.Size(216, 35);
            this.tableLayoutPanel13.TabIndex = 16;
            // 
            // rbConfigPrecessCommandFalse
            // 
            this.rbConfigPrecessCommandFalse.AutoSize = true;
            this.rbConfigPrecessCommandFalse.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rbConfigPrecessCommandFalse.Location = new System.Drawing.Point(111, 3);
            this.rbConfigPrecessCommandFalse.Name = "rbConfigPrecessCommandFalse";
            this.rbConfigPrecessCommandFalse.Size = new System.Drawing.Size(102, 29);
            this.rbConfigPrecessCommandFalse.TabIndex = 1;
            this.rbConfigPrecessCommandFalse.TabStop = true;
            this.rbConfigPrecessCommandFalse.Text = "OFF";
            this.rbConfigPrecessCommandFalse.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.rbConfigPrecessCommandFalse.UseVisualStyleBackColor = true;
            // 
            // rbConfigPrecessCommandTrue
            // 
            this.rbConfigPrecessCommandTrue.AutoSize = true;
            this.rbConfigPrecessCommandTrue.Checked = true;
            this.rbConfigPrecessCommandTrue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rbConfigPrecessCommandTrue.Location = new System.Drawing.Point(3, 3);
            this.rbConfigPrecessCommandTrue.Name = "rbConfigPrecessCommandTrue";
            this.rbConfigPrecessCommandTrue.Size = new System.Drawing.Size(102, 29);
            this.rbConfigPrecessCommandTrue.TabIndex = 0;
            this.rbConfigPrecessCommandTrue.TabStop = true;
            this.rbConfigPrecessCommandTrue.Text = "ON";
            this.rbConfigPrecessCommandTrue.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.rbConfigPrecessCommandTrue.UseVisualStyleBackColor = true;
            // 
            // cbProcess
            // 
            this.cbProcess.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cbProcess.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbProcess.FormattingEnabled = true;
            this.cbProcess.Location = new System.Drawing.Point(146, 3);
            this.cbProcess.Name = "cbProcess";
            this.cbProcess.Size = new System.Drawing.Size(210, 26);
            this.cbProcess.TabIndex = 9;
            this.cbProcess.SelectedIndexChanged += new System.EventHandler(this.cbProcess_SelectedIndexChanged);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label17.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold);
            this.label17.Location = new System.Drawing.Point(3, 35);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(137, 35);
            this.label17.TabIndex = 1;
            this.label17.Text = "Name";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label18.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold);
            this.label18.Location = new System.Drawing.Point(3, 70);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(137, 35);
            this.label18.TabIndex = 2;
            this.label18.Text = "Path";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label27.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold);
            this.label27.Location = new System.Drawing.Point(3, 105);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(137, 35);
            this.label27.TabIndex = 5;
            this.label27.Text = "Command";
            this.label27.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label28.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold);
            this.label28.Location = new System.Drawing.Point(3, 140);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(137, 35);
            this.label28.TabIndex = 0;
            this.label28.Text = "Child Name 1";
            this.label28.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tableLayoutPanel7
            // 
            this.tableLayoutPanel7.ColumnCount = 2;
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel7.Controls.Add(this.btnSaveProcess, 0, 0);
            this.tableLayoutPanel7.Controls.Add(this.btnRemoveProcess, 0, 0);
            this.tableLayoutPanel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel7.Location = new System.Drawing.Point(143, 210);
            this.tableLayoutPanel7.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel7.Name = "tableLayoutPanel7";
            this.tableLayoutPanel7.RowCount = 1;
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel7.Size = new System.Drawing.Size(216, 39);
            this.tableLayoutPanel7.TabIndex = 6;
            // 
            // btnSaveProcess
            // 
            this.btnSaveProcess.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnSaveProcess.Location = new System.Drawing.Point(111, 3);
            this.btnSaveProcess.Name = "btnSaveProcess";
            this.btnSaveProcess.Size = new System.Drawing.Size(102, 33);
            this.btnSaveProcess.TabIndex = 2;
            this.btnSaveProcess.Text = "SAVE";
            this.btnSaveProcess.UseVisualStyleBackColor = true;
            this.btnSaveProcess.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnRemoveProcess
            // 
            this.btnRemoveProcess.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnRemoveProcess.Location = new System.Drawing.Point(3, 3);
            this.btnRemoveProcess.Name = "btnRemoveProcess";
            this.btnRemoveProcess.Size = new System.Drawing.Size(102, 33);
            this.btnRemoveProcess.TabIndex = 1;
            this.btnRemoveProcess.Text = "REMOVE";
            this.btnRemoveProcess.UseVisualStyleBackColor = true;
            this.btnRemoveProcess.Click += new System.EventHandler(this.btnRemove_Click);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label16.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold);
            this.label16.Location = new System.Drawing.Point(3, 175);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(137, 35);
            this.label16.TabIndex = 10;
            this.label16.Text = "Child Name 2";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tbConfigProcessName
            // 
            this.tbConfigProcessName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbConfigProcessName.Location = new System.Drawing.Point(146, 38);
            this.tbConfigProcessName.Name = "tbConfigProcessName";
            this.tbConfigProcessName.Size = new System.Drawing.Size(210, 28);
            this.tbConfigProcessName.TabIndex = 11;
            // 
            // tbConfigProcessPath
            // 
            this.tbConfigProcessPath.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbConfigProcessPath.Location = new System.Drawing.Point(146, 73);
            this.tbConfigProcessPath.Name = "tbConfigProcessPath";
            this.tbConfigProcessPath.Size = new System.Drawing.Size(210, 28);
            this.tbConfigProcessPath.TabIndex = 12;
            // 
            // tbConfigProcessChild1
            // 
            this.tbConfigProcessChild1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbConfigProcessChild1.Location = new System.Drawing.Point(146, 143);
            this.tbConfigProcessChild1.Name = "tbConfigProcessChild1";
            this.tbConfigProcessChild1.Size = new System.Drawing.Size(210, 28);
            this.tbConfigProcessChild1.TabIndex = 14;
            // 
            // tbConfigProcessChild2
            // 
            this.tbConfigProcessChild2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbConfigProcessChild2.Location = new System.Drawing.Point(146, 178);
            this.tbConfigProcessChild2.Name = "tbConfigProcessChild2";
            this.tbConfigProcessChild2.Size = new System.Drawing.Size(210, 28);
            this.tbConfigProcessChild2.TabIndex = 15;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.tableLayoutPanel5);
            this.groupBox5.Location = new System.Drawing.Point(13, 265);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(365, 168);
            this.groupBox5.TabIndex = 2;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Version";
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.ColumnCount = 2;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 60F));
            this.tableLayoutPanel5.Controls.Add(this.label23, 0, 1);
            this.tableLayoutPanel5.Controls.Add(this.tableLayoutPanel6, 1, 3);
            this.tableLayoutPanel5.Controls.Add(this.cbVersion, 1, 0);
            this.tableLayoutPanel5.Controls.Add(this.label12, 0, 2);
            this.tableLayoutPanel5.Controls.Add(this.tbConfigVersionName, 1, 1);
            this.tableLayoutPanel5.Controls.Add(this.tbConfigVersionVersion, 1, 2);
            this.tableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel5.Location = new System.Drawing.Point(3, 24);
            this.tableLayoutPanel5.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 4;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(359, 141);
            this.tableLayoutPanel5.TabIndex = 0;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label23.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold);
            this.label23.Location = new System.Drawing.Point(3, 35);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(137, 35);
            this.label23.TabIndex = 3;
            this.label23.Text = "Name";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tableLayoutPanel6
            // 
            this.tableLayoutPanel6.ColumnCount = 2;
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.46296F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 49.53704F));
            this.tableLayoutPanel6.Controls.Add(this.btnSaveVersion, 1, 0);
            this.tableLayoutPanel6.Controls.Add(this.btnRemoveVersion, 0, 0);
            this.tableLayoutPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel6.Location = new System.Drawing.Point(143, 105);
            this.tableLayoutPanel6.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel6.Name = "tableLayoutPanel6";
            this.tableLayoutPanel6.RowCount = 1;
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel6.Size = new System.Drawing.Size(216, 36);
            this.tableLayoutPanel6.TabIndex = 6;
            // 
            // btnSaveVersion
            // 
            this.btnSaveVersion.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnSaveVersion.Location = new System.Drawing.Point(111, 3);
            this.btnSaveVersion.Name = "btnSaveVersion";
            this.btnSaveVersion.Size = new System.Drawing.Size(102, 30);
            this.btnSaveVersion.TabIndex = 1;
            this.btnSaveVersion.Text = "SAVE";
            this.btnSaveVersion.UseVisualStyleBackColor = true;
            this.btnSaveVersion.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnRemoveVersion
            // 
            this.btnRemoveVersion.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnRemoveVersion.Location = new System.Drawing.Point(3, 3);
            this.btnRemoveVersion.Name = "btnRemoveVersion";
            this.btnRemoveVersion.Size = new System.Drawing.Size(102, 30);
            this.btnRemoveVersion.TabIndex = 0;
            this.btnRemoveVersion.Text = "REMOVE";
            this.btnRemoveVersion.UseVisualStyleBackColor = true;
            this.btnRemoveVersion.Click += new System.EventHandler(this.btnRemove_Click);
            // 
            // cbVersion
            // 
            this.cbVersion.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cbVersion.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbVersion.FormattingEnabled = true;
            this.cbVersion.Location = new System.Drawing.Point(146, 3);
            this.cbVersion.Name = "cbVersion";
            this.cbVersion.Size = new System.Drawing.Size(210, 26);
            this.cbVersion.TabIndex = 7;
            this.cbVersion.SelectedIndexChanged += new System.EventHandler(this.cbVersion_SelectedIndexChanged);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label12.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold);
            this.label12.Location = new System.Drawing.Point(3, 70);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(137, 35);
            this.label12.TabIndex = 8;
            this.label12.Text = "Version";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tbConfigVersionName
            // 
            this.tbConfigVersionName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbConfigVersionName.Location = new System.Drawing.Point(146, 38);
            this.tbConfigVersionName.Name = "tbConfigVersionName";
            this.tbConfigVersionName.Size = new System.Drawing.Size(210, 28);
            this.tbConfigVersionName.TabIndex = 9;
            // 
            // tbConfigVersionVersion
            // 
            this.tbConfigVersionVersion.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbConfigVersionVersion.Location = new System.Drawing.Point(146, 73);
            this.tbConfigVersionVersion.Name = "tbConfigVersionVersion";
            this.tbConfigVersionVersion.Size = new System.Drawing.Size(210, 28);
            this.tbConfigVersionVersion.TabIndex = 10;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.tableLayoutPanel1);
            this.groupBox1.Location = new System.Drawing.Point(16, 19);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(365, 240);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Setting";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 60F));
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel12, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel11, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.label6, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.label7, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.label8, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.label9, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.label10, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel10, 1, 5);
            this.tableLayoutPanel1.Controls.Add(this.tbConfigSettingServerName, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.tbConfigSettingLogDriver, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.tbConfigSettingLogDays, 1, 3);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 24);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 6;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(359, 213);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // tableLayoutPanel12
            // 
            this.tableLayoutPanel12.ColumnCount = 2;
            this.tableLayoutPanel12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel12.Controls.Add(this.rbConfigSettingAutoStartFalse, 1, 0);
            this.tableLayoutPanel12.Controls.Add(this.rbConfigSettingAutoStartTrue, 0, 0);
            this.tableLayoutPanel12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel12.Location = new System.Drawing.Point(143, 140);
            this.tableLayoutPanel12.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel12.Name = "tableLayoutPanel12";
            this.tableLayoutPanel12.RowCount = 1;
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel12.Size = new System.Drawing.Size(216, 35);
            this.tableLayoutPanel12.TabIndex = 12;
            // 
            // rbConfigSettingAutoStartFalse
            // 
            this.rbConfigSettingAutoStartFalse.AutoSize = true;
            this.rbConfigSettingAutoStartFalse.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rbConfigSettingAutoStartFalse.Location = new System.Drawing.Point(111, 3);
            this.rbConfigSettingAutoStartFalse.Name = "rbConfigSettingAutoStartFalse";
            this.rbConfigSettingAutoStartFalse.Size = new System.Drawing.Size(102, 29);
            this.rbConfigSettingAutoStartFalse.TabIndex = 1;
            this.rbConfigSettingAutoStartFalse.TabStop = true;
            this.rbConfigSettingAutoStartFalse.Text = "OFF";
            this.rbConfigSettingAutoStartFalse.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.rbConfigSettingAutoStartFalse.UseVisualStyleBackColor = true;
            // 
            // rbConfigSettingAutoStartTrue
            // 
            this.rbConfigSettingAutoStartTrue.AutoSize = true;
            this.rbConfigSettingAutoStartTrue.Checked = true;
            this.rbConfigSettingAutoStartTrue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rbConfigSettingAutoStartTrue.Location = new System.Drawing.Point(3, 3);
            this.rbConfigSettingAutoStartTrue.Name = "rbConfigSettingAutoStartTrue";
            this.rbConfigSettingAutoStartTrue.Size = new System.Drawing.Size(102, 29);
            this.rbConfigSettingAutoStartTrue.TabIndex = 0;
            this.rbConfigSettingAutoStartTrue.TabStop = true;
            this.rbConfigSettingAutoStartTrue.Text = "ON";
            this.rbConfigSettingAutoStartTrue.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.rbConfigSettingAutoStartTrue.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel11
            // 
            this.tableLayoutPanel11.ColumnCount = 2;
            this.tableLayoutPanel11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel11.Controls.Add(this.rbConfigSettingRestartFalse, 1, 0);
            this.tableLayoutPanel11.Controls.Add(this.rbConfigSettingRestartTrue, 0, 0);
            this.tableLayoutPanel11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel11.Location = new System.Drawing.Point(143, 35);
            this.tableLayoutPanel11.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel11.Name = "tableLayoutPanel11";
            this.tableLayoutPanel11.RowCount = 1;
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel11.Size = new System.Drawing.Size(216, 35);
            this.tableLayoutPanel11.TabIndex = 11;
            // 
            // rbConfigSettingRestartFalse
            // 
            this.rbConfigSettingRestartFalse.AutoSize = true;
            this.rbConfigSettingRestartFalse.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rbConfigSettingRestartFalse.Location = new System.Drawing.Point(111, 3);
            this.rbConfigSettingRestartFalse.Name = "rbConfigSettingRestartFalse";
            this.rbConfigSettingRestartFalse.Size = new System.Drawing.Size(102, 29);
            this.rbConfigSettingRestartFalse.TabIndex = 1;
            this.rbConfigSettingRestartFalse.Text = "OFF";
            this.rbConfigSettingRestartFalse.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.rbConfigSettingRestartFalse.UseVisualStyleBackColor = true;
            // 
            // rbConfigSettingRestartTrue
            // 
            this.rbConfigSettingRestartTrue.AutoSize = true;
            this.rbConfigSettingRestartTrue.Checked = true;
            this.rbConfigSettingRestartTrue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rbConfigSettingRestartTrue.Location = new System.Drawing.Point(3, 3);
            this.rbConfigSettingRestartTrue.Name = "rbConfigSettingRestartTrue";
            this.rbConfigSettingRestartTrue.Size = new System.Drawing.Size(102, 29);
            this.rbConfigSettingRestartTrue.TabIndex = 0;
            this.rbConfigSettingRestartTrue.TabStop = true;
            this.rbConfigSettingRestartTrue.Text = "ON";
            this.rbConfigSettingRestartTrue.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.rbConfigSettingRestartTrue.UseVisualStyleBackColor = true;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label6.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold);
            this.label6.Location = new System.Drawing.Point(3, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(137, 35);
            this.label6.TabIndex = 0;
            this.label6.Text = "Server Name";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label7.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold);
            this.label7.Location = new System.Drawing.Point(3, 35);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(137, 35);
            this.label7.TabIndex = 1;
            this.label7.Text = "Restart";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label8.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold);
            this.label8.Location = new System.Drawing.Point(3, 70);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(137, 35);
            this.label8.TabIndex = 2;
            this.label8.Text = "Log Path";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label9.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold);
            this.label9.Location = new System.Drawing.Point(3, 105);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(137, 35);
            this.label9.TabIndex = 3;
            this.label9.Text = "Log Days";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label10.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold);
            this.label10.Location = new System.Drawing.Point(3, 140);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(137, 35);
            this.label10.TabIndex = 4;
            this.label10.Text = "Auto Start";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tableLayoutPanel10
            // 
            this.tableLayoutPanel10.ColumnCount = 2;
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel10.Controls.Add(this.btnSaveSetting, 1, 0);
            this.tableLayoutPanel10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel10.Location = new System.Drawing.Point(143, 175);
            this.tableLayoutPanel10.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel10.Name = "tableLayoutPanel10";
            this.tableLayoutPanel10.RowCount = 1;
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel10.Size = new System.Drawing.Size(216, 38);
            this.tableLayoutPanel10.TabIndex = 7;
            // 
            // btnSaveSetting
            // 
            this.btnSaveSetting.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnSaveSetting.Location = new System.Drawing.Point(111, 3);
            this.btnSaveSetting.Name = "btnSaveSetting";
            this.btnSaveSetting.Size = new System.Drawing.Size(102, 32);
            this.btnSaveSetting.TabIndex = 2;
            this.btnSaveSetting.Text = "SAVE";
            this.btnSaveSetting.UseVisualStyleBackColor = true;
            this.btnSaveSetting.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // tbConfigSettingServerName
            // 
            this.tbConfigSettingServerName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbConfigSettingServerName.Location = new System.Drawing.Point(146, 3);
            this.tbConfigSettingServerName.Name = "tbConfigSettingServerName";
            this.tbConfigSettingServerName.ReadOnly = true;
            this.tbConfigSettingServerName.Size = new System.Drawing.Size(210, 28);
            this.tbConfigSettingServerName.TabIndex = 8;
            // 
            // tbConfigSettingLogDriver
            // 
            this.tbConfigSettingLogDriver.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbConfigSettingLogDriver.Location = new System.Drawing.Point(146, 73);
            this.tbConfigSettingLogDriver.Name = "tbConfigSettingLogDriver";
            this.tbConfigSettingLogDriver.Size = new System.Drawing.Size(210, 28);
            this.tbConfigSettingLogDriver.TabIndex = 9;
            // 
            // tbConfigSettingLogDays
            // 
            this.tbConfigSettingLogDays.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbConfigSettingLogDays.Location = new System.Drawing.Point(146, 108);
            this.tbConfigSettingLogDays.Name = "tbConfigSettingLogDays";
            this.tbConfigSettingLogDays.Size = new System.Drawing.Size(210, 28);
            this.tbConfigSettingLogDays.TabIndex = 10;
            this.tbConfigSettingLogDays.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbConfigOnlyNumbers_KeyPress);
            // 
            // dataGridViewExTextBoxColumn1
            // 
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dataGridViewExTextBoxColumn1.DefaultCellStyle = dataGridViewCellStyle9;
            this.dataGridViewExTextBoxColumn1.Frozen = true;
            this.dataGridViewExTextBoxColumn1.HeaderText = "Name";
            this.dataGridViewExTextBoxColumn1.MinimumWidth = 8;
            this.dataGridViewExTextBoxColumn1.Name = "dataGridViewExTextBoxColumn1";
            this.dataGridViewExTextBoxColumn1.Width = 150;
            // 
            // dataGridViewExTextBoxColumn2
            // 
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dataGridViewExTextBoxColumn2.DefaultCellStyle = dataGridViewCellStyle10;
            this.dataGridViewExTextBoxColumn2.Frozen = true;
            this.dataGridViewExTextBoxColumn2.HeaderText = "ProcessName";
            this.dataGridViewExTextBoxColumn2.MinimumWidth = 8;
            this.dataGridViewExTextBoxColumn2.Name = "dataGridViewExTextBoxColumn2";
            this.dataGridViewExTextBoxColumn2.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewExTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            this.dataGridViewExTextBoxColumn2.Width = 200;
            // 
            // dataGridViewExTextBoxColumn3
            // 
            this.dataGridViewExTextBoxColumn3.Frozen = true;
            this.dataGridViewExTextBoxColumn3.HeaderText = "State";
            this.dataGridViewExTextBoxColumn3.MinimumWidth = 8;
            this.dataGridViewExTextBoxColumn3.Name = "dataGridViewExTextBoxColumn3";
            this.dataGridViewExTextBoxColumn3.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewExTextBoxColumn3.Width = 50;
            // 
            // dataGridViewExTextBoxColumn4
            // 
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dataGridViewExTextBoxColumn4.DefaultCellStyle = dataGridViewCellStyle11;
            this.dataGridViewExTextBoxColumn4.Frozen = true;
            this.dataGridViewExTextBoxColumn4.HeaderText = "AutoStartMode";
            this.dataGridViewExTextBoxColumn4.MinimumWidth = 8;
            this.dataGridViewExTextBoxColumn4.Name = "dataGridViewExTextBoxColumn4";
            this.dataGridViewExTextBoxColumn4.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewExTextBoxColumn4.Width = 150;
            // 
            // dataGridViewExTextBoxColumn5
            // 
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dataGridViewExTextBoxColumn5.DefaultCellStyle = dataGridViewCellStyle12;
            this.dataGridViewExTextBoxColumn5.Frozen = true;
            this.dataGridViewExTextBoxColumn5.HeaderText = "CPU";
            this.dataGridViewExTextBoxColumn5.MinimumWidth = 8;
            this.dataGridViewExTextBoxColumn5.Name = "dataGridViewExTextBoxColumn5";
            this.dataGridViewExTextBoxColumn5.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewExTextBoxColumn5.Width = 150;
            // 
            // dataGridViewExTextBoxColumn6
            // 
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dataGridViewExTextBoxColumn6.DefaultCellStyle = dataGridViewCellStyle13;
            this.dataGridViewExTextBoxColumn6.Frozen = true;
            this.dataGridViewExTextBoxColumn6.HeaderText = "RAM";
            this.dataGridViewExTextBoxColumn6.MinimumWidth = 8;
            this.dataGridViewExTextBoxColumn6.Name = "dataGridViewExTextBoxColumn6";
            this.dataGridViewExTextBoxColumn6.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewExTextBoxColumn6.Width = 150;
            // 
            // dataGridViewExTextBoxColumn7
            // 
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dataGridViewExTextBoxColumn7.DefaultCellStyle = dataGridViewCellStyle14;
            this.dataGridViewExTextBoxColumn7.Frozen = true;
            this.dataGridViewExTextBoxColumn7.HeaderText = "GdiCount";
            this.dataGridViewExTextBoxColumn7.MinimumWidth = 8;
            this.dataGridViewExTextBoxColumn7.Name = "dataGridViewExTextBoxColumn7";
            this.dataGridViewExTextBoxColumn7.Width = 150;
            // 
            // dataGridViewExTextBoxColumn8
            // 
            dataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dataGridViewExTextBoxColumn8.DefaultCellStyle = dataGridViewCellStyle15;
            this.dataGridViewExTextBoxColumn8.Frozen = true;
            this.dataGridViewExTextBoxColumn8.HeaderText = "FileVersion";
            this.dataGridViewExTextBoxColumn8.MinimumWidth = 8;
            this.dataGridViewExTextBoxColumn8.Name = "dataGridViewExTextBoxColumn8";
            this.dataGridViewExTextBoxColumn8.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewExTextBoxColumn8.Width = 150;
            // 
            // dataGridViewExTextBoxColumn9
            // 
            dataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dataGridViewExTextBoxColumn9.DefaultCellStyle = dataGridViewCellStyle16;
            this.dataGridViewExTextBoxColumn9.Frozen = true;
            this.dataGridViewExTextBoxColumn9.HeaderText = "ProductVersion";
            this.dataGridViewExTextBoxColumn9.MinimumWidth = 8;
            this.dataGridViewExTextBoxColumn9.Name = "dataGridViewExTextBoxColumn9";
            this.dataGridViewExTextBoxColumn9.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewExTextBoxColumn9.Width = 150;
            // 
            // dataGridViewExTextBoxColumn10
            // 
            this.dataGridViewExTextBoxColumn10.Frozen = true;
            this.dataGridViewExTextBoxColumn10.HeaderText = "Start";
            this.dataGridViewExTextBoxColumn10.MinimumWidth = 8;
            this.dataGridViewExTextBoxColumn10.Name = "dataGridViewExTextBoxColumn10";
            this.dataGridViewExTextBoxColumn10.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewExTextBoxColumn10.Width = 150;
            // 
            // dataGridViewExTextBoxColumn11
            // 
            this.dataGridViewExTextBoxColumn11.Frozen = true;
            this.dataGridViewExTextBoxColumn11.HeaderText = "Kill";
            this.dataGridViewExTextBoxColumn11.MinimumWidth = 8;
            this.dataGridViewExTextBoxColumn11.Name = "dataGridViewExTextBoxColumn11";
            this.dataGridViewExTextBoxColumn11.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewExTextBoxColumn11.Width = 150;
            // 
            // MainForm_Server
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(1778, 726);
            this.Controls.Add(this.tabControl2);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "MainForm_Server";
            this.Text = "Process Watcher Server";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.OnFormClosing);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.OnMainFormKeyDown);
            this.contextMenuStrip1.ResumeLayout(false);
            this.tabControl2.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.m_DgvSumView)).EndInit();
            this.tabPage4.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.tableLayoutPanel8.ResumeLayout(false);
            this.tableLayoutPanel8.PerformLayout();
            this.tableLayoutPanel9.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel4.PerformLayout();
            this.tableLayoutPanel13.ResumeLayout(false);
            this.tableLayoutPanel13.PerformLayout();
            this.tableLayoutPanel7.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.tableLayoutPanel5.ResumeLayout(false);
            this.tableLayoutPanel5.PerformLayout();
            this.tableLayoutPanel6.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.tableLayoutPanel12.ResumeLayout(false);
            this.tableLayoutPanel12.PerformLayout();
            this.tableLayoutPanel11.ResumeLayout(false);
            this.tableLayoutPanel11.PerformLayout();
            this.tableLayoutPanel10.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.ListView m_lvShareMemory;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.Button m_btnStart;
        private System.Windows.Forms.Button m_btnStop;
        private System.Windows.Forms.Timer m_TimerAutoStart;
        private System.Windows.Forms.ListView m_lvLog;
        private System.Windows.Forms.Button m_btnStartUpAdd;
        private System.Windows.Forms.Button m_btnStartUpRemove;
        private System.Windows.Forms.Button m_btnHide;
        private System.Windows.Forms.NotifyIcon notifyIcon1;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem 열기ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 종료ToolStripMenuItem;
        private System.Windows.Forms.Button m_btnAutoStart;
        private System.Windows.Forms.ToolStripMenuItem 시작프로그램등록ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem shareMemoryOpenToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 시작프로그램등록ToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem 시작프로그램해제ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem shareMemoryOpenToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem shareMemoryCloseToolStripMenuItem;
        private System.Windows.Forms.ComboBox m_cbScanTime;
        private System.Windows.Forms.Label m_lbScanTime;
        private System.Windows.Forms.Button m_btnAutoProcessState;
        private System.Windows.Forms.Timer m_TimerProcessAutoStart;
        private System.Windows.Forms.Label m_lbProcessAutoStart;
        private System.Windows.Forms.TabControl tabControl2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Label m_lbProcessAutoState;
        private System.Windows.Forms.Label m_lbShareMemoryState;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.DataGridView m_DgvSumView;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private DataGridViewExTextBoxColumn sName;
        private DataGridViewExTextBoxColumn sProcessName;
        private DataGridViewExTextBoxColumn bState;
        private DataGridViewExTextBoxColumn bAutoStartState;
        private DataGridViewExTextBoxColumn sCPU;
        private DataGridViewExTextBoxColumn sRAM;
        private DataGridViewExTextBoxColumn GdiCount;
        private DataGridViewExTextBoxColumn sFileVersion;
        private DataGridViewExTextBoxColumn sProductVersion;
        private DataGridViewExTextBoxColumn btnStart;
        private DataGridViewExTextBoxColumn btnKill;
        private System.Windows.Forms.Button btnLoadSettingData;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel8;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel9;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel7;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.ComboBox cbShareMemory;
        private System.Windows.Forms.ComboBox cbClient;
        private System.Windows.Forms.ComboBox cbProcess;
        private System.Windows.Forms.ComboBox cbVersion;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel10;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button btnSaveShareMemory;
        private System.Windows.Forms.Button btnRemoveShareMemory;
        private System.Windows.Forms.Button btnSaveClient;
        private System.Windows.Forms.Button btnRemoveClient;
        private System.Windows.Forms.Button btnSaveProcess;
        private System.Windows.Forms.Button btnRemoveProcess;
        private System.Windows.Forms.Button btnSaveVersion;
        private System.Windows.Forms.Button btnRemoveVersion;
        private System.Windows.Forms.Button btnSaveSetting;
        private DataGridViewExTextBoxColumn dataGridViewExTextBoxColumn1;
        private DataGridViewExTextBoxColumn dataGridViewExTextBoxColumn2;
        private DataGridViewExTextBoxColumn dataGridViewExTextBoxColumn3;
        private DataGridViewExTextBoxColumn dataGridViewExTextBoxColumn4;
        private DataGridViewExTextBoxColumn dataGridViewExTextBoxColumn5;
        private DataGridViewExTextBoxColumn dataGridViewExTextBoxColumn6;
        private DataGridViewExTextBoxColumn dataGridViewExTextBoxColumn7;
        private DataGridViewExTextBoxColumn dataGridViewExTextBoxColumn8;
        private DataGridViewExTextBoxColumn dataGridViewExTextBoxColumn9;
        private DataGridViewExTextBoxColumn dataGridViewExTextBoxColumn10;
        private DataGridViewExTextBoxColumn dataGridViewExTextBoxColumn11;
        private System.Windows.Forms.TextBox tbConfigShareMemoryName;
        private System.Windows.Forms.TextBox tbConfigShareMemorySize;
        private System.Windows.Forms.TextBox tbConfigClientProcess5;
        private System.Windows.Forms.TextBox tbConfigClientName;
        private System.Windows.Forms.TextBox tbConfigClientIP;
        private System.Windows.Forms.TextBox tbConfigClientProcess1;
        private System.Windows.Forms.TextBox tbConfigClientProcess2;
        private System.Windows.Forms.TextBox tbConfigClientProcess3;
        private System.Windows.Forms.TextBox tbConfigClientProcess4;
        private System.Windows.Forms.TextBox tbConfigProcessName;
        private System.Windows.Forms.TextBox tbConfigProcessPath;
        private System.Windows.Forms.TextBox tbConfigProcessChild1;
        private System.Windows.Forms.TextBox tbConfigProcessChild2;
        private System.Windows.Forms.TextBox tbConfigVersionName;
        private System.Windows.Forms.TextBox tbConfigVersionVersion;
        private System.Windows.Forms.TextBox tbConfigSettingServerName;
        private System.Windows.Forms.TextBox tbConfigSettingLogDriver;
        private System.Windows.Forms.TextBox tbConfigSettingLogDays;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel13;
        private System.Windows.Forms.RadioButton rbConfigPrecessCommandFalse;
        private System.Windows.Forms.RadioButton rbConfigPrecessCommandTrue;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel12;
        private System.Windows.Forms.RadioButton rbConfigSettingAutoStartFalse;
        private System.Windows.Forms.RadioButton rbConfigSettingAutoStartTrue;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel11;
        private System.Windows.Forms.RadioButton rbConfigSettingRestartFalse;
        private System.Windows.Forms.RadioButton rbConfigSettingRestartTrue;
    }
}

