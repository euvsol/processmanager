﻿#region Class using
using System;
using System.IO;
using System.Linq;
using System.Drawing;
using System.Threading;
using System.Diagnostics;
using System.Windows.Forms;
using System.Collections.Generic;
using Esol.Framework;
using Esol.ShareMemory;
using Esol.ShareTrxControl;
using Esol.LogControl;
#endregion

namespace ProcessWatcher
{
    public partial class MainForm_Client : Form
    {
        #region Class Constants
        /// <summary>
        /// 
        /// </summary>
        public const string DEF_FDC_BIT = "L2_B_FDC";
        #endregion

        #region Class Members
        /// <summary>
        /// 
        /// </summary>
        private object m_Object;
        /// <summary>
        /// 
        /// </summary>
        private int m_iLocal;
        /// <summary>
        /// 
        /// </summary>
        private int m_iLogDays;
        /// <summary>
        /// 
        /// </summary>
        private int m_iLocalGap;
        /// <summary>
        /// 
        /// </summary>
        private int m_iShareMemorySize;
        /// <summary>
        /// 
        /// </summary>
        private bool m_bSyncRead;
        /// <summary>
        /// 
        /// </summary>
        private bool m_bSyncWrite;
        /// <summary>
        /// 
        /// </summary>
        private bool m_bAutoStart;
        /// <summary>
        /// 
        /// </summary>
        private bool m_bServerState;
        /// <summary>
        /// 
        /// </summary>
        private bool m_bProcessAutoStart;
        /// <summary>
        /// 
        /// </summary>
        private double m_dMaxTime;
        /// <summary>
        /// 
        /// </summary>
        private double m_dMinTime;
        /// <summary>
        /// 
        /// </summary>
        private string m_sServerIp;
        /// <summary>
        /// 
        /// </summary>
        private string m_sLogDriver;
        /// <summary>
        /// 
        /// </summary>
        private string m_sLocalName;
        /// <summary>
        /// 
        /// </summary>
        private string m_sShareMemoryName;
        /// <summary>
        /// 
        /// </summary>
        private DateTime m_OldTime;
        /// <summary>
        /// 
        /// </summary>
        private PlcAddr m_FdcAddress;
        /// <summary>
        /// 
        /// </summary>
        private ShareMemoryClientManager m_SessionMgr;
        /// <summary>
        /// 
        /// </summary>
        private Thread m_threadUsage;
        /// <summary>
        /// 
        /// </summary>
        private Thread m_threadShareDataChanged;
        /// <summary>
        /// 
        /// </summary>
        private Thread m_threadShareData;
        /// <summary>
        /// 
        /// </summary>
        private VirtualMemory m_ShareMem;
        /// <summary>
        /// 
        /// </summary>
        private ShareTrx m_ShareTrx;
        /// <summary>
        /// 
        /// </summary>
        private List<string> m_listOffProcess;
        /// <summary>
        /// 
        /// </summary>
        private Dictionary<string, List<string>> m_hashProcessToVersion;
        /// <summary>
        /// 
        /// </summary>
        private Queue<PlcAddr> m_queueShareDataBunchData;
        /// <summary>
        /// 
        /// </summary>
        public event ShareEventHandler EventShareDataChanged;
        /// <summary>
        /// 
        /// </summary>
        public delegate void ShareEventHandler(PlcAddr Addr);
        /// <summary>
        /// 
        /// </summary>
        private Configuration m_Configuration;
        /// <summary>
        /// 
        /// </summary>
        private LogManager m_LogManager;
        /// <summary>
        /// 
        /// </summary>
        private Dictionary<string, string[]> m_hashProcessDatas;
        /// <summary>
        /// 
        /// </summary>
        private List<MonitoringData> m_listProcessDatas;
        /// <summary>
        /// 
        /// </summary>
        private Dictionary<int, string> m_hashProcessNoToName;
        #endregion

        #region Class Properites
        /// <summary>
        /// 
        /// </summary>
        public VirtualMemory ShareMem
        {
            get
            {
                return m_ShareMem;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public ShareTrx ShareTrx
        {
            get
            {
                return m_ShareTrx;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public int LocalNo
        {
            get
            {
                return m_iLocal;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public LogManager LogManager
        {
            get
            {
                return m_LogManager;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public string LogDriver
        {
            get
            {
                return m_sLogDriver;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public Dictionary<string, string[]> HashProcessDatas
        {
            get
            {
                return m_hashProcessDatas;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public Dictionary<int, string> HashProcessNoToName
        {
            get
            {
                return m_hashProcessNoToName;
            }
        }
        #endregion

        #region Class Initialize
        /// <summary>
        /// 
        /// </summary>
        public MainForm_Client()
        {
            try
            {
                InitializeComponent();
                InitiMembers();
                InitializeSetting();
                InitializeLogManager();
                InitializeShareMemory();
                InitializeDataGridView();
                ThreadInitialize();

                m_Configuration = new Configuration(this, null);

                m_Configuration.EventProcessAutoStart += new Configuration.ProcessAutoStartEventHandler(OnHostProcessAutoStart);

                m_TimerProcessAutoStart.Start();

                if (m_bAutoStart)
                    m_TimerAutoStart.Start();

                KeyPreview = true;
                m_cbScanTime.SelectedIndex = 0;

                string sVersion = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
                this.Text = string.Format("{0} ver.{1}", this.Text, sVersion);

                DataGridViewCrossThread(m_DgvSumView, m_listProcessDatas);
            }
            catch { }
        }
        /// <summary>
        /// 종료버튼 비활성화
        /// </summary>
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams myCp = base.CreateParams;
                myCp.ClassStyle = myCp.ClassStyle | Constants.CP_NOCLOSE_BUTTON;
                return myCp;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        private void InitiMembers()
        {
            CheckForIllegalCrossThreadCalls = false;
            m_hashProcessToVersion = new Dictionary<string, List<string>>();
            m_hashProcessDatas = new Dictionary<string, string[]>();
            m_hashProcessNoToName = new Dictionary<int, string>();
            m_listProcessDatas = new List<MonitoringData>();
            m_listOffProcess = new List<string>();
            m_Object = new object();
            m_bAutoStart = false;
            m_bServerState = false;
            m_dMaxTime = double.MinValue;
            m_dMinTime = double.MaxValue;
            m_SessionMgr = new ShareMemoryClientManager();
        }
        /// <summary>
        /// 
        /// </summary>
        private void InitializeLogManager()
        {
            string sLogPath = Constants.DEF_LOG_FILE_PATH.Replace("D:", string.Format("{0}:", m_sLogDriver));

            m_LogManager = new LogManager(sLogPath, m_iLogDays);

            m_LogManager.Initialize(Constants.DEF_CLIENT_TEXT, string.Empty, Constants.DEF_LOG_TEXT, eLogTimeType.Hour);
            m_LogManager.Initialize("ERROR", string.Empty, Constants.DEF_LOG_TEXT, eLogTimeType.Hour);
        }
        /// <summary>
        /// 
        /// </summary>
        private void InitializeSetting()
        {
            try
            {
                string sSettingFilePath = Path.Combine(Application.StartupPath, "_ConfigFiles", Constants.DEF_CLIENT_TEXT, Constants.DEF_SETTING_TEXT, "Setting.xml");

                if (!File.Exists(sSettingFilePath))
                {
                    MessageBox.Show("Setting Xml 파일이 존재 하지 않습니다.\n시스템을 종료 합니다.");
                    Environment.Exit(0);
                }

                XmlDataReader xmlreader = new XmlDataReader(sSettingFilePath, false);
                string sHaederKey = "ProcessWatcher/";

                List<string[]> listSetting = xmlreader.XmlReadList(sHaederKey + "Settings", Enum.GetNames(typeof(eClientSettingKeys)));

                m_bProcessAutoStart = listSetting[0][0] == "1" ? true : false;
                m_btnProcessAutoStart.Text = m_bProcessAutoStart ? Constants.DEF_ENABLE_TEXT : Constants.DEF_DISABLE_TEXT;

                m_bAutoStart = listSetting[0][1] == "1" ? true : false;

                m_sLogDriver = listSetting[0][2];
                if (m_sLogDriver.Length != 1 || string.IsNullOrEmpty(m_sLogDriver))
                    m_sLogDriver = "C";

                m_iLogDays = string.IsNullOrEmpty(listSetting[0][3]) ? 30 : Convert.ToInt32(listSetting[0][3]);

                m_iLocal = string.IsNullOrEmpty(listSetting[0][5]) ? 2 : Convert.ToInt32(listSetting[0][5]);
                m_sLocalName = string.IsNullOrEmpty(listSetting[0][4]) ? "L" + m_iLocal : listSetting[0][4];

                m_iLocalGap = m_iLocal - 2;

                //Server Process Setting
                m_hashProcessDatas = xmlreader.XmlReadDictionary(sHaederKey + "Pros", Enum.GetNames(typeof(eProcessKeys)));

                foreach (string[] sArrayValue in m_hashProcessDatas.Values)
                {
                    string sProcessName = sArrayValue[0];
                    string sProcessPath = sArrayValue[1];
                    int iProcessCommand = string.IsNullOrEmpty(sArrayValue[2]) ? 1 : Convert.ToInt32(sArrayValue[2]);

                    MonitoringData md = new MonitoringData()
                    {
                        ProcessName = sProcessName,
                        ProcessPath = sProcessPath,
                        ProcessCommand = iProcessCommand
                    };

                    string[] sArrayProcessPath = sProcessPath.Split('\\');
                    string[] sArrayProcessPathCheck = sArrayProcessPath.Last().Split('.');

                    string sFileName = sArrayProcessPath.Last();
                    sFileName = sFileName.Substring(0, sFileName.Length - (sArrayProcessPathCheck.Last().Length + 1));

                    if (!File.Exists(sProcessPath))
                    {
                        DialogResult dr = MessageBox.Show(string.Format("[{0}] {1}", sProcessName, Constants.DEF_NOT_FILE_PATH), Constants.DEF_NOTIFICATION_MESSAGE, MessageBoxButtons.YesNo, MessageBoxIcon.Information);

                        if (dr != DialogResult.Yes)
                            Environment.Exit(0);
                    }

                    if (sProcessName.ToLower() != sFileName.ToLower())
                    {
                        DialogResult dr = MessageBox.Show(string.Format("[{0}] Process Name과 파일명이 상이 합니다.\n정상 작동되지 않습니다.\n계속 진행 하겠습니까?", sProcessName), Constants.DEF_NOTIFICATION_MESSAGE, MessageBoxButtons.YesNo, MessageBoxIcon.Information);

                        if (dr != DialogResult.Yes)
                            Environment.Exit(0);
                    }

                    m_listProcessDatas.Add(md);
                }

                //ShareMemory Setting

                Dictionary<string, string[]> hashShareMemory = xmlreader.XmlReadDictionary(sHaederKey + "Memorys", Enum.GetNames(typeof(eClientShareMemoryKeys)));

                foreach (string[] sShareValue in hashShareMemory.Values)
                {
                    string sName = sShareValue[1];
                    string sIp = sShareValue[2];
                    int.TryParse(sShareValue[3], out int iSize);
                    int.TryParse(sShareValue[4], out int iSyncStart);
                    int.TryParse(sShareValue[5], out int iSyncCommand);
                    int.TryParse(sShareValue[6], out int iSyncTime);

                    //JHKIM 20220803 CIM Test용 임시
                    if (sName.ToUpper().Contains("PROCESS") || sName == "ESOL.SREM.MEM")
                    {
                        if (iSyncCommand == 1)
                        {
                            m_bSyncWrite = true;

                            iSyncStart += m_iLocalGap * Constants.DEF_CLIENT_ADDRESS_SHIFT;
                            m_sServerIp = sIp;
                        }
                        else if (iSyncCommand == 0)
                            m_bSyncRead = true;

                        m_sShareMemoryName = sName;

                        m_iShareMemorySize = iSyncStart + iSize;
                    }

                    ShareMemorySyncSession session = new ShareMemorySyncSession(sIp, 7160)
                    {
                        ShareMemoryName = sName,
                        ShareMemorySize = iSize,
                        SyncCommand = iSyncCommand,
                        SyncStart = iSyncStart,
                        SyncTime = iSyncTime
                    };

                    m_SessionMgr.LstSession.Add(session);
                }


                string sLogPath = Constants.DEF_LOG_FILE_PATH.Replace("D:", string.Format("{0}:", m_sLogDriver));

                Logger.FileLogger = new SimpleFileLoggerMark6(sLogPath, "ShareMemoryClient", 2000, 1024 * 1024 * 20, false, lstLogger);
                Logger.FileLogger.AppendLine(LogLevel.Info, "InitializeSetting");

                lstShMem.Items.Clear();
                foreach (ShareMemorySyncSession sh in m_SessionMgr.LstSession)
                {
                    int iEndPoint = sh.SyncStart + sh.ShareMemorySize - 1;
                    lstShMem.Items.Add(
                        new ListViewItem(
                            new string[] {
                            "",
                            sh.IP,
                            sh.SyncCommand == 1? "Send" : "Recv",
                            sh.ShareMemoryName,
                            sh.ShareMemorySize.ToString(),
                            sh.SyncStart.ToString(),
                            iEndPoint.ToString(),
                            "-",
                            "-",
                            "-",
                            "-",
                            "-",
                            "-",
                    })
                        { Tag = sh });
                }
            }
            catch (Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());

                MessageBox.Show("Setting Xml 파일 잘 못 기입된 값이 있습니다.\n시스템을 종료 합니다.");
                Environment.Exit(0);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        private void InitializeDataGridView()
        {
            try
            {
                for (int i = 0; i < m_DgvSumView.Columns.Count; i++)
                    m_DgvSumView.Columns[i].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;

                m_DgvSumView.Rows.Add("Server Connect", string.Empty, m_sServerIp);
                //m_DgvSumView.Rows[0].Cells[2].Style.BackColor = Color.Red;
                //var cell = (DataGridViewTextBoxCellEx)m_DgvSumView[2, 1];
                //cell.ColumnSpan = 7;
                m_DgvSumView.Rows.Add("PC Name", string.Empty, string.Empty, string.Empty, string.Empty, m_sLocalName);
                //m_DgvSumView.Rows[1].Cells[2].Style.BackColor = Color.Red;
                //var PcCell = (DataGridViewTextBoxCellEx)m_DgvSumView[5, 2];
                //PcCell.ColumnSpan = 4;
                m_DgvSumView.Rows[1].DefaultCellStyle.BackColor = Color.LightGray;

                int iCount = 1;
                foreach (string[] sProcessName in m_hashProcessDatas.Values)
                {
                    m_hashProcessNoToName.Add(iCount++, sProcessName[0]);
                    m_DgvSumView.Rows.Add(sProcessName[0], string.Empty);
                    m_DgvSumView.Rows[m_DgvSumView.Rows.Count - 1].Cells[1].Style.BackColor = Color.Red;
                }
            }
            catch (Exception ex)
            {
                m_LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        /// <summary>
        /// 
        /// </summary>
        private void ThreadInitialize()
        {
            m_queueShareDataBunchData = new Queue<PlcAddr>();

            m_threadUsage = new Thread(new ThreadStart(OnGetUsage))
            { IsBackground = true };

            m_threadShareDataChanged = new Thread(new ThreadStart(OnShareDataChanged))
            { IsBackground = true };

            m_threadShareData = new Thread(new ThreadStart(OnShareMessageProcessorThread))
            { IsBackground = true };

            m_threadUsage.Start();
            m_threadShareData.Start();
            m_threadShareDataChanged.Start();
        }
        /// <summary>
        /// 
        /// </summary>
        private void InitializeShareMemory()
        {
            try
            {
                if (m_bSyncRead && m_bSyncWrite)
                    m_ShareMem = new VirtualMemory(m_sShareMemoryName, m_iShareMemorySize);
                else
                {
                    if (!m_bSyncWrite && !m_bSyncRead)
                        MessageBox.Show("Process Manager Read / Write ShareMemory 영역이 없어 종료 합니다.");
                    else if (!m_bSyncWrite)
                        MessageBox.Show("Process Manager Write ShareMemory 영역이 없어 종료 합니다.");
                    else if (!m_bSyncRead)
                        MessageBox.Show("Process Manager Read ShareMemory 영역이 없어 종료 합니다.");

                    Environment.Exit(0);
                }

                m_ShareMem.Open();

                m_ShareTrx = new ShareTrx(m_ShareMem);

                string sPath = System.Windows.Forms.Application.StartupPath;

                m_ShareTrx.InitializeDataBit(sPath + "\\" + Constants.DEF_BIT_FILE_PATH);
                m_ShareTrx.InitializeDataWord(sPath + "\\" + Constants.DEF_WORD_FILE_PATH);
                m_ShareTrx.InitializeDataStruct(sPath + "\\" + Constants.DEF_STRUCT_FILE_PATH);
                m_ShareTrx.InitializeDataTrx(sPath + "\\" + Constants.DEF_TRX_FILE_PATH);

                foreach (string sKey in m_ShareTrx.hashShareKeyToShareData.Keys)
                {
                    string[] sNames = sKey.Split('_');
                    if (sNames[0] != Constants.DEF_SERVER_LOCAL)
                        m_ShareTrx.hashShareKeyToShareData[sKey].Addr += Constants.DEF_CLIENT_ADDRESS_SHIFT * m_iLocalGap;
                    else
                        m_ShareTrx.hashShareKeyToShareData[sKey].Addr += 20 * m_iLocalGap;
                }

                foreach (string sKey in m_ShareTrx.hashShareStruct.Keys)
                {
                    for (int i = 0; i < m_ShareTrx.hashShareStruct[sKey].Count; i++)
                    {
                        string[] sNames = sKey.Split('_');
                        if (sNames[0] != Constants.DEF_SERVER_LOCAL)
                            m_ShareTrx.hashShareStruct[sKey][i].Addr += Constants.DEF_CLIENT_ADDRESS_SHIFT * m_iLocalGap;
                    }
                }
                m_FdcAddress = m_ShareTrx.hashShareKeyToShareData[DEF_FDC_BIT];
            }
            catch (Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        #endregion

        #region Class Event
        /// <summary>
        /// 
        /// </summary>
        private void OnbtnStart_Click(object sender, EventArgs e)
        {
            m_SessionMgr.Start();
            m_btnStart.Enabled = false;
            m_btnStop.Enabled = true;
            m_lbShareMemoryState.Text = "True";
        }
        /// <summary>
        /// 
        /// </summary>
        private void OnbtnStop_Click(object sender, EventArgs e)
        {
            m_SessionMgr.Stop();
            m_btnStart.Enabled = true;
            m_btnStop.Enabled = false;
            m_lbShareMemoryState.Text = "False";
        }
        /// <summary>
        /// 
        /// </summary>
        private void OnHide(object sender, EventArgs e)
        {
            if (m_btnStart.Enabled)
            {
                DialogResult dr = MessageBox.Show(Constants.DEF_SHARE_MEMORY_HIDE_MESSAGE, Constants.DEF_NOTIFICATION_MESSAGE, MessageBoxButtons.OKCancel, MessageBoxIcon.Information);

                if (dr == DialogResult.OK)
                    this.Hide();
            }
            else
                this.Hide();
        }
        /// <summary>
        /// 
        /// </summary>
        private void OnMouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
                contextMenuStrip1.Show(MousePosition.X, MousePosition.Y);
        }
        /// <summary>
        /// 
        /// </summary>
        private void OnOpenClick(object sender, MouseEventArgs e)
        {
            this.Visible = true;
            this.ShowInTaskbar = true;
            this.Show();
            this.WindowState = FormWindowState.Normal;
        }
        /// <summary>
        /// 
        /// </summary>
        private void OnFormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                string sMessage = string.Empty;

                if (!m_btnStart.Enabled)
                    sMessage = Constants.DEF_SHARE_MEMORY_START_CLOSE_MESSAGE;
                else
                    sMessage = Constants.DEF_SHRARE_MEMORY_CLOSE_MESSAGE;

                DialogResult dr = MessageBox.Show(sMessage, Constants.DEF_NOTIFICATION_MESSAGE, MessageBoxButtons.OKCancel, MessageBoxIcon.Information);

                if (dr == DialogResult.OK)
                {
                    Logger.FileLogger.Dispose();
                    m_SessionMgr.Stop();
                    m_TimerAutoStart.Stop();
                    //m_TimerInterface.Stop();
                    m_TimerState.Stop();
                    m_threadUsage.Abort();
                }
                else if (e != null)
                    e.Cancel = true;
            }
            catch (Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());

                Environment.Exit(0);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        private void OnStartUpAdd(object sender, EventArgs e)
        {
            StartupAddRemove(Constants.DEF_SHARE_MEMORY_CLIENT, Application.ExecutablePath);
        }
        /// <summary>
        /// 
        /// </summary>
        private void OnStartUpRemove(object sender, EventArgs e)
        {
            StartupAddRemove(Constants.DEF_SHARE_MEMORY_CLIENT, null);
        }
        /// <summary>
        /// 
        /// </summary>
        private void OnAutoStartChange(object sender, EventArgs e)
        {
            switch (m_bAutoStart)
            {
                case true:
                    m_bAutoStart = false;
                    m_btnAutoStart.Text = Constants.DEF_AUTO_START_DISABLE;
                    m_lbProcessAutoState.Text = Constants.DEF_AUTO_START_DISABLE;
                    break;
                case false:
                    m_bAutoStart = true;
                    m_btnAutoStart.Text = Constants.DEF_AUTO_START_ENABLE;
                    m_lbProcessAutoState.Text = Constants.DEF_AUTO_START_ENABLE;
                    break;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        private void OnProcessAutoStart(object sender, EventArgs e)
        {
            OnProcessAutoStartChange();
        }
        /// <summary>
        /// 
        /// </summary>
        private void OnMainFormKeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F1)
                Common.OnHelpFileCheck();
        }
        /// <summary>
        /// 
        /// </summary>
        private void OnHostProcessAutoStart()
        {
            OnProcessAutoStartChange();
            m_ShareMem.SetByte(m_ShareTrx.hashShareKeyToShareData["L2_W_PROCESS_AUTO_START_ACK"], 1);
        }
        /// <summary>
        /// 
        /// </summary>
        private void OnProcessAutoStartChange()
        {
            switch (m_btnProcessAutoStart.Text)
            {
                case Constants.DEF_ENABLE_TEXT:
                    m_bProcessAutoStart = false;
                    m_btnProcessAutoStart.Text = Constants.DEF_DISABLE_TEXT;
                    break;
                case Constants.DEF_DISABLE_TEXT:
                    m_bProcessAutoStart = true;
                    m_btnProcessAutoStart.Text = Constants.DEF_ENABLE_TEXT;
                    break;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        private void OnCellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.RowIndex != -1 && (e.ColumnIndex == 7 || e.ColumnIndex == 8))
                {
                    switch (e.ColumnIndex)
                    {
                        case 7:
                            string sPath = m_DgvSumView.Rows[e.RowIndex].Cells[e.ColumnIndex].Tag.ToString();
                            if (File.Exists(sPath))
                                Process.Start(sPath);
                            else
                                MessageBox.Show("경로에 파일이 없습니다.");
                            break;
                        case 8:
                            string sProcessName = m_DgvSumView.Rows[e.RowIndex].Cells[e.ColumnIndex].Tag.ToString();
                            Process[] processlist = Process.GetProcessesByName(sProcessName);
                            if (processlist.Length > 0)
                                processlist[0].Kill();
                            else
                                MessageBox.Show(string.Format("{0} 종료 되어 있습니다.", sProcessName));
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
            }
        }

        private void OnDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.RowIndex == -1 || e.ColumnIndex != 2)
                    return;

                if (m_DgvSumView.Rows[e.RowIndex].Cells[e.ColumnIndex].Tag == null)
                {
                    MessageBox.Show(" 단독 Process Auto Start Mode는 Program에서 변경 할 수 없습니다.");
                    return;
                }

                string sValue = m_DgvSumView.Rows[e.RowIndex].Cells[e.ColumnIndex].Tag.ToString();


                OnProcessAutoStart(null, null);
            }
            catch (Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        #endregion

        #region Class Timer
        /// <summary>
        /// 
        /// </summary>
        private void TimerAutoStart_Tick(object sender, EventArgs e)
        {
            if (!m_bAutoStart)
                return;

            if (m_btnStart.Enabled)
                OnbtnStart_Click(null, null);
        }
        /// <summary>
        /// 
        /// </summary>
        private void TimerState_Tick(object sender, EventArgs e)
        {
            m_tbState.Text = GetCurrentStatus();
            UpdateSync();
        }
        /// <summary>
        /// 
        /// </summary>
        private void TimerProcessAutoStart_Tick(object sender, EventArgs e)
        {
            try
            {
                bool bCheck = false;

                foreach (string sPath in m_listOffProcess)
                {
                    var _key = m_hashProcessDatas.FirstOrDefault(x => x.Value[0] == sPath).Key;

                    Mutex mutex = new Mutex(true, _key, out bool bRun);


                    if (bRun && m_btnProcessAutoStart.Text != Constants.DEF_DISABLE_TEXT)
                    {
                        if (File.Exists(sPath))
                        {
                            Process.Start(sPath);
                            bCheck = true;
                        }
                    }
                }

                if ((m_listOffProcess.Count != 0 && bCheck) || m_btnProcessAutoStart.Text == Constants.DEF_DISABLE_TEXT)
                    m_listOffProcess.Clear();
            }
            catch (Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        #endregion

        #region Class Private
        /// <summary>
        /// 
        /// </summary>
        private void UpdateSync()
        {
            try
            {
                for (int iPos = 0; iPos < lstShMem.Items.Count; iPos++)
                {
                    if (!(lstShMem.Items[iPos].Tag is ShareMemorySyncSession ServerSession))
                        continue;

                    List<SyncResult> listSyncResult = null;

                    lock (ServerSession.ListSyncResult)
                    {
                        listSyncResult = ServerSession.ListSyncResult;
                        ServerSession.ListSyncResult = new List<SyncResult>();
                    }

                    listSyncResult.RemoveAll((f) => { return f.RspTime < DateTime.Now.AddSeconds(-1); });

                    double dCount = listSyncResult.Count;
                    double dMax = dCount > 0 ? listSyncResult.Max(f => (f.RspTime - f.ReqTime).TotalMilliseconds) : 0;
                    double dMin = dCount > 0 ? listSyncResult.Min(f => (f.RspTime - f.ReqTime).TotalMilliseconds) : 0;
                    double dAverage = dCount > 0 ? listSyncResult.Average(f => (f.RspTime - f.ReqTime).TotalMilliseconds) : 0;

                    m_dMaxTime = m_dMaxTime < dMax ? dMax : m_dMaxTime;
                    m_dMinTime = m_dMinTime > dMin ? dMin : m_dMinTime;

                    m_bServerState = ServerSession.IsConnected;
                    lstShMem.Items[iPos].SubItems[7].Text = ServerSession.IsConnected ? "Connected" : "Disconnected";
                    lstShMem.Items[iPos].SubItems[8].Text = Math.Round(dCount, 2).ToString();
                    lstShMem.Items[iPos].SubItems[9].Text = Math.Round(dAverage, 2).ToString();
                    lstShMem.Items[iPos].SubItems[10].Text = Math.Round(m_dMinTime, 2).ToString();
                    lstShMem.Items[iPos].SubItems[11].Text = Math.Round(m_dMaxTime, 2).ToString();
                    lstShMem.Items[iPos].SubItems[12].Text = ServerSession.SyncSec.ToString();

                    if (dMax > 5000)
                        foreach (SyncResult sr in listSyncResult)
                            LogManager.WriteLog(Constants.DEF_SHARE_MEMORY_CLIENT, string.Format("Sync Count Zero, Start = {0}, End ={1}, time ={2} ", sr.ReqTime, sr.RspTime, (sr.RspTime - sr.ReqTime).TotalMilliseconds));

                    LogManager.WriteLog(Constants.DEF_SHARE_MEMORY_CLIENT, string.Format("Name = {0}, Start ={1}, Size ={2}, Min ={3}, Max ={4}, Avg ={5}, Count ={6}", ServerSession.ShareMemoryName, ServerSession.SyncStart, ServerSession.ShareMemorySize, dMin, dMax, dAverage, dCount));

                    if (dCount == 0 && ServerSession.IsConnected == true && (Esol.Framework.PcDateTime.Now - ServerSession.ConnectDateTime).TotalSeconds > 5)
                    {
                        LogManager.WriteLog(Constants.DEF_SHARE_MEMORY_CLIENT, string.Format("Sync Count Zero, Name = {0}, Start ={1}, Size ={2}", ServerSession.ShareMemoryName, ServerSession.SyncStart, ServerSession.ShareMemorySize));
                        ServerSession.Disconnect();
                    }
                }
            }
            catch (Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        /// <summary>
        /// 
        /// </summary>
        private string GetCurrentStatus()
        {
            Process p = Process.GetCurrentProcess();
            string s = string.Format(" Thread {0}, Handle {1}, Use Memory {2}kb, Peak Use Memory {3}kb, Ver {4}, ESOL", p.Threads.Count, p.HandleCount, p.WorkingSet64 / 1024, p.PeakWorkingSet64 / 1024, Application.ProductVersion);
            p.Close();
            p.Dispose();
            return s;
        }
        /// <summary>
        /// 
        /// </summary>
        private void StartupAddRemove(string sProgramName, string sExecutablePath)
        {
            Common.OnStartUpAddRemove(sProgramName, sExecutablePath);
        }
        /// <summary>
        /// 
        /// </summary>
        private void OnScanTimeSelectChanged(object sender, EventArgs e)
        {
            //m_iCpuRamUsageScanTime = (m_cbScanTime.SelectedIndex + 1) * 1000;
        }
        #endregion

        #region Class NofiyIcon Event
        /// <summary>
        /// 
        /// </summary>
        private void OntsmiShow_Click(object sender, EventArgs e)
        {
            OnOpenClick(null, null);
        }
        /// <summary>
        /// 
        /// </summary>
        private void OntsmiExit_Click(object sender, EventArgs e)
        {
            string sMessage = m_btnStart.Enabled ? Constants.DEF_SHRARE_MEMORY_CLOSE_MESSAGE : Constants.DEF_SHARE_MEMORY_START_CLOSE_MESSAGE;

            DialogResult dr = MessageBox.Show(sMessage, Constants.DEF_NOTIFICATION_MESSAGE, MessageBoxButtons.OKCancel, MessageBoxIcon.Information);

            if (dr == DialogResult.OK)
            {
                Logger.FileLogger.Dispose();
                m_SessionMgr.Stop();

                Environment.Exit(0);
            }
        }
        #endregion

        #region Class thread
        /// <summary>
        /// 
        /// </summary>
        private delegate void ReloadDataGridView(DataGridView dgv, List<MonitoringData> listMD);
        /// <summary>
        /// 
        /// </summary>
        private void DataGridViewCrossThread(DataGridView dgv, List<MonitoringData> listMD)
        {
            try
            {
                if (dgv.InvokeRequired)
                {
                    ReloadDataGridView reload = new ReloadDataGridView(DataGridViewCrossThread);
                    this.Invoke(reload, new object[] { dgv, listMD });
                }
                else
                {
                    if (!m_btnStop.Enabled)
                    {
                        for (int i = 0; i < dgv.Rows.Count; i++)
                            dgv.Rows[i].Cells[1].Value = string.Empty;

                        return;
                    }

                    int iCount = 2;
                    int iShareCount = 0;
                    dgv.Rows[0].Cells[1].Style.BackColor = m_bServerState ? Color.Green : Color.Red;
                    dgv.Rows[1].Cells[1].Style.BackColor = Color.Green;
                    dgv.Rows[1].Cells[2].Value = listMD[0].AutoStartMode == 1 ? "True" : "False";
                    dgv.Rows[1].Cells[2].Tag = "true";
                    dgv.Rows[1].Cells[3].Value = listMD[0].TotalCPU;
                    dgv.Rows[1].Cells[4].Value = listMD[0].TotalRAM;

                    List<PlcAddr> list = m_ShareTrx.hashShareStruct[Constants.DEF_PROCESS_STRUCT];
                    m_ShareMem.SetByte(list[iShareCount++], Convert.ToByte(m_hashProcessNoToName.Count));
                    m_ShareMem.SetAscii(list[iShareCount++], listMD[0].LocalName);
                    m_ShareMem.SetAscii(list[iShareCount++], listMD[0].TotalCPU);
                    m_ShareMem.SetAscii(list[iShareCount++], listMD[0].TotalRAM);
                    m_ShareMem.SetByte(list[iShareCount++], listMD[0].AutoStartMode);

                    foreach (MonitoringData md in listMD)
                    {
                        if (string.IsNullOrEmpty(md.ProcessName))
                            continue;

                        DataGridViewButtonCell CellBtn = new DataGridViewButtonCell();
                        DataGridViewExTextBoxCell CellTb = new DataGridViewExTextBoxCell();
                        DataGridViewExTextBoxCell CellTb2 = new DataGridViewExTextBoxCell();
                        if (md.ProcessCommand == 0)
                        {
                            if (dgv.Rows[iCount].Cells[7].EditType == null)
                                dgv.Rows[iCount].Cells[7] = CellTb;

                            if (dgv.Rows[iCount].Cells[8].EditType == null)
                                dgv.Rows[iCount].Cells[8] = CellTb2;
                        }
                        switch (string.IsNullOrEmpty(md.ProcessCPU))
                        {
                            case true:
                                dgv.Rows[iCount].Cells[1].Style.BackColor = Color.Red;

                                if (dgv.Rows[iCount].Cells[7].EditType != null)
                                {
                                    dgv.Rows[iCount].Cells[7] = CellBtn;
                                    dgv.Rows[iCount].Cells[7].Tag = md.ProcessPath;
                                    dgv.Rows[iCount].Cells[7].Value = Constants.DEF_START_TEXT;
                                    dgv.Rows[iCount].Cells[7].Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                                }

                                if (dgv.Rows[iCount].Cells[8].EditType == null)
                                    dgv.Rows[iCount].Cells[8] = CellTb;
                                break;
                            case false:
                                dgv.Rows[iCount].Cells[1].Style.BackColor = Color.Green;

                                if (dgv.Rows[iCount].Cells[7].EditType == null)
                                    dgv.Rows[iCount].Cells[7] = CellTb;

                                if (dgv.Rows[iCount].Cells[8].EditType != null)
                                {
                                    dgv.Rows[iCount].Cells[8] = CellBtn;
                                    dgv.Rows[iCount].Cells[8].Tag = md.ProcessName;
                                    dgv.Rows[iCount].Cells[8].Value = Constants.DEF_KILL_TEXT;
                                    dgv.Rows[iCount].Cells[8].Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                                }
                                break;
                        }
                        dgv.Rows[iCount].Cells[2].Value = m_hashProcessDatas[md.ProcessName][2] == "1" ? true : false;
                        dgv.Rows[iCount].Cells[3].Value = md.ProcessCPU;
                        dgv.Rows[iCount].Cells[4].Value = md.ProcessRAM;
                        dgv.Rows[iCount].Cells[5].Value = md.FileVersion;
                        dgv.Rows[iCount++].Cells[6].Value = md.ProductVersion;

                        string sProcessState = string.IsNullOrEmpty(md.ProcessName)
                            ? string.Empty : string.Format("{0} : {1}", md.ProcessName, md.ProcessState);

                        m_ShareMem.SetAscii(list[iShareCount++], sProcessState);
                        m_ShareMem.SetAscii(list[iShareCount++], md.ProcessCPU);
                        m_ShareMem.SetAscii(list[iShareCount++], md.ProcessRAM);
                        //Version
                        m_ShareMem.SetAscii(list[iShareCount++], md.FileVersion);
                        m_ShareMem.SetAscii(list[iShareCount++], md.ProductVersion);
                        //Command
                        byte iCommand = Convert.ToByte(m_hashProcessDatas[md.ProcessName][2]);
                        m_ShareMem.SetByte(list[iShareCount++], iCommand);
                    }
                    
                    if (m_bServerState)
                    {
                        m_ShareMem.SetBit(m_FdcAddress, true);

                        Thread.Sleep(500);
                        m_ShareMem.SetBit(m_FdcAddress, false);
                    }
                }
            }
            catch (Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        /// <summary>
        /// 
        /// </summary>
        private void OnGetUsage()
        {
            try
            {
                while (true)
                {
                    m_OldTime = Common.DateTimeClone();
                    List<MonitoringData> listMD = new List<MonitoringData>();

                    bool bCheck = false;

                    foreach (string sProcessName in m_hashProcessDatas.Keys)
                    {
                        MonitoringData md = new MonitoringData()
                        {
                            ProcessLocal = string.Format("L{0}", m_iLocal),
                            ProcessPath = m_hashProcessDatas[sProcessName][1]
                        };

                        if (!bCheck)
                        {
                            md.TotalCPU = Common.TotalCPU(Constants.DEF_TOTAL_TEXT);
                            md.TotalRAM = Common.OnRamUsage();
                            md.AutoStartMode = Convert.ToByte(m_bProcessAutoStart == true ? 1 : 0);
                            md.LocalName = m_sLocalName;
                            md.ProcessCommand = string.IsNullOrEmpty(m_hashProcessDatas[sProcessName][2]) ? 0 : Convert.ToInt32(m_hashProcessDatas[sProcessName][2]);
                            bCheck = true;
                        }

                        Process[] processlist = Process.GetProcessesByName(sProcessName);
                        md.ProcessName = sProcessName;
                        switch (processlist.Length)
                        {
                            case 0:
                                md.ProcessState = Constants.DEF_OFF_TEXT;
                                md.ProcessCPU = string.Empty;
                                md.ProcessRAM = string.Empty;
                                md.FileVersion = string.Empty;
                                md.ProductVersion = string.Empty;
                                m_hashProcessToVersion.Remove(sProcessName);

                                if (m_bProcessAutoStart && !m_listOffProcess.Contains(sProcessName))
                                    if (!m_listOffProcess.Contains(md.ProcessPath))
                                        m_listOffProcess.Add(md.ProcessPath);

                                if (m_hashProcessDatas.ContainsKey(sProcessName) && m_hashProcessDatas[sProcessName][2] == "1")
                                    if (!m_listOffProcess.Contains(md.ProcessPath))
                                        m_listOffProcess.Add(md.ProcessPath);
                                break;
                            default:
                                md.ProcessState = Constants.DEF_OK_TEXT;
                                md.ProcessCPU = Common.TotalCPU(sProcessName);
                                md.ProcessRAM = Common.ProcessRAM(sProcessName);

                                if (!m_hashProcessToVersion.ContainsKey(sProcessName))
                                {
                                    FileVersionInfo fiv = FileVersionInfo.GetVersionInfo(m_hashProcessDatas[sProcessName][1]);
                                    md.FileVersion = fiv.FileVersion;
                                    md.ProductVersion = fiv.ProductVersion;

                                    List<string> listVersion = new List<string>()
                                    {
                                        md.FileVersion,
                                        md.ProductVersion
                                    };
                                    m_hashProcessToVersion.Add(sProcessName, listVersion);
                                }
                                else
                                {
                                    md.FileVersion = m_hashProcessToVersion[sProcessName][0];
                                    md.ProductVersion = m_hashProcessToVersion[sProcessName][1];
                                }
                                break;
                        }
                        listMD.Add(md);
                    }

                    DataGridViewCrossThread(m_DgvSumView, listMD);

                    int iTime = Common.OnTimeDiff(m_OldTime, 1000);
                    Thread.Sleep(iTime < 0 ? 0 : iTime);
                }
            }
            catch (Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        /// <summary>
        /// 
        /// </summary>
        private void OnShareDataChanged()
        {
            try
            {
                Dictionary<string, PlcAddr> hashMonitoring = new Dictionary<string, PlcAddr>();

                foreach (PlcAddr addr in m_ShareTrx.hashShareKeyToShareData.Values)
                {
                    if (addr.ValueType == PlcValueType.BIT)
                        hashMonitoring.Add(addr.AddrName, addr);
                }

                string[] sArrayKeys = new string[hashMonitoring.Keys.Count];
                hashMonitoring.Keys.CopyTo(sArrayKeys, 0);

                while (true)
                {
                    Thread.Sleep(1);

                    foreach (string sKey in sArrayKeys)
                    {
                        PlcAddr addr = hashMonitoring[sKey];

                        Thread.Sleep(1);

                        bool bReadValue = (bool)Common.Clone(m_ShareMem.GetBit(addr));

                        if (hashMonitoring[sKey].vBit != bReadValue)
                        {
                            hashMonitoring[sKey].vBit = (bool)Common.Clone(bReadValue);
                            m_queueShareDataBunchData.Enqueue(addr);

                            lock (m_Object)
                                Monitor.Pulse(m_Object);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());

                m_threadShareDataChanged = new Thread(new ThreadStart(OnShareDataChanged))
                { IsBackground = true };

                m_threadShareDataChanged.Start();
            }
        }
        /// <summary>
        /// 
        /// </summary>
        private void OnShareMessageProcessorThread()
        {
            try
            {
                while (true)
                {
                    PlcAddr addr = null;

                    lock (m_Object)
                    {
                        if (m_queueShareDataBunchData.Count > 0)
                            addr = m_queueShareDataBunchData.Dequeue();
                        else
                            Monitor.Wait(m_Object);

                        if (addr != null) EventShareDataChanged(addr);
                    }
                }
            }
            catch (Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        #endregion
    }
}
