﻿using System;
using System.Threading;
using System.Windows.Forms;
using Esol.Framework;
using System.IO;

namespace ProcessWatcher
{
    static class Program
    {
        /// <summary>
        /// 해당 응용 프로그램의 주 진입점입니다.
        /// </summary>
        [STAThread]
        static void Main()
        {
            string sSettingFilePath = Path.Combine(Application.StartupPath, "_ConfigFiles", "_StartUp.ini");
            IniReader ini = new IniReader(sSettingFilePath);

            string sMode = ini.GetString("Setting", "Mode");

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Mutex dup = new Mutex(true, "ProcessWatcher", out bool createNew);

            if (createNew)
            {
                switch(sMode)
                {
                    case "1":
                        Application.Run(new MainForm_Server());
                        break;
                    default:
                        Application.Run(new MainForm_Client());
                        break;
                }
            }
            else
                MessageBox.Show("이미 프로그램이 실행중 입니다.");
        }
    }
}
