﻿namespace ProcessWatcher
{
    public class LastestVersionData
    {
        private string m_sProcessName = string.Empty;
        private string m_sVersion = string.Empty;

        public string ProcessName
        {
            get { return m_sProcessName; }
            set { m_sProcessName = value; }
        }

        public string Version
        {
            get { return m_sVersion; }
            set { m_sVersion = value; }
        }
    }

    public class MonitoringData
    {
        #region Class Members
        /// <summary>
        /// 
        /// </summary>
        private string m_sTotalCpu = string.Empty;
        /// <summary>
        /// 
        /// </summary>
        private string m_sTotalRam = string.Empty;
        /// <summary>
        /// 
        /// </summary>
        private string m_sProcessState = string.Empty;
        /// <summary>
        /// 
        /// </summary>
        private string m_sProcessCpu = string.Empty;
        /// <summary>
        /// 
        /// </summary>
        private string m_sProcessRam = string.Empty;
        /// <summary>
        /// 
        /// </summary>
        private string m_sScanTime = string.Empty;
        /// <summary>
        /// 
        /// </summary>
        private string m_sProcessName = string.Empty;
        /// <summary>
        /// 
        /// </summary>
        private string m_sProcessPath = string.Empty;
        /// <summary>
        /// 
        /// </summary>
        private string m_sProcessLocal = string.Empty;
        /// <summary>
        /// `
        /// </summary>
        private string m_sLocalName = string.Empty;
        /// <summary>
        /// 
        /// </summary>
        private byte m_iAutoStartMode = 0;
        /// <summary>
        /// 
        /// </summary>
        private string m_sFileVersion = string.Empty;
        /// <summary>
        /// 
        /// </summary>
        private string m_sProductVersion = string.Empty;
        /// <summary>
        /// 
        /// </summary>
        private int m_iProcessCommand;
        /// <summary>
        /// 
        /// </summary>
        private uint m_uGdiCount;
        #endregion

        #region Class Properties
        /// <summary>
        /// 
        /// </summary>
        public string TotalCPU
        {
            get
            {
                return m_sTotalCpu;
            }
            set
            {
                m_sTotalCpu = value;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public string TotalRAM
        {
            get
            {
                return m_sTotalRam;
            }
            set
            {
                m_sTotalRam = value;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public string ProcessState
        {
            get
            {
                return m_sProcessState;
            }
            set
            {
                m_sProcessState = value;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public string ProcessCPU
        {
            get
            {
                return m_sProcessCpu;
            }
            set
            {
                m_sProcessCpu = value;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public string ProcessRAM
        {
            get
            {
                return m_sProcessRam;
            }
            set
            {
                m_sProcessRam = value;
            }
        }/// <summary>
         /// 
         /// </summary>
        public string ScanTime
        {
            get
            {
                return m_sScanTime;
            }
            set
            {
                m_sScanTime = value;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public string ProcessName
        {
            get
            {
                return m_sProcessName;
            }
            set
            {
                m_sProcessName = value;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public string ProcessPath
        {
            get
            {
                return m_sProcessPath;
            }
            set
            {
                m_sProcessPath = value;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public string ProcessLocal
        {
            get
            {
                return m_sProcessLocal;
            }
            set
            {
                m_sProcessLocal = value;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public byte AutoStartMode
        {
            get
            {
                return m_iAutoStartMode;
            }
            set
            {
                m_iAutoStartMode = value;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public string LocalName
        {
            get
            {
                return m_sLocalName;
            }
            set
            {
                m_sLocalName = value;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public string FileVersion
        {
            get
            {
                return m_sFileVersion;
            }
            set
            {
                m_sFileVersion = value;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public string ProductVersion
        {
            get
            {
                return m_sProductVersion;
            }
            set
            {
                m_sProductVersion = value;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public int ProcessCommand
        {
            get
            {
                return m_iProcessCommand;
            }
            set
            {
                m_iProcessCommand = value;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public uint GdiCount
        {
            get
            {
                return m_uGdiCount;
            }
            set
            {
                m_uGdiCount = value;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public string ChildProcessName1
        {
            get;
            set;
        }
        /// <summary>
        /// 
        /// </summary>
        public string ChildProcessName2
        {
            get;
            set;
        }
        #endregion
    }
}
