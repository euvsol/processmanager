﻿#region Class using
using System;
using System.Collections.Generic;
using System.Threading;
using System.Diagnostics;
using Esol.ShareMemory;
using Esol.ShareTrxControl;
using Esol.LogControl;
#endregion

namespace ProcessWatcher
{
    public class Configuration
    {
        #region Class Constants
        /// <summary>
        /// 
        /// </summary>
        private const int DEF_TIMER_SCAN_TIME = 1000;
        /// <summary>
        /// 
        /// </summary>
        private const int DEF_BIT_TIME_OUT = 5;
        #endregion

        #region Class Members
        /// <summary>
        /// 
        /// </summary>
        private VirtualMemory m_ShareMem;
        /// <summary>
        /// 
        /// </summary>
        private ShareTrx m_ShareTrx;
        /// <summary>
        /// 
        /// </summary>
        private MainForm_Client m_MainForm_Client;
        /// <summary>
        /// 
        /// </summary>
        private MainForm_Server m_MainForm_Server;
        /// <summary>
        /// 
        /// </summary>
        private Timer m_timer;
        /// <summary>
        /// 
        /// </summary>
        private readonly object m_syncObject;
        /// <summary>
        /// 
        /// </summary>
        private readonly int m_iLocal;
        /// <summary>
        /// 
        /// </summary>
        private Dictionary<string, DateTime> m_hashBitTime;
        /// <summary>
        /// 
        /// </summary>
        private Esol.ShareTrxControl.Logger m_Logger;
        /// <summary>
        /// 
        /// </summary>
        public event FdcEventHandler EventFdcDataChanged;
        /// <summary>
        /// 
        /// </summary>
        public delegate void FdcEventHandler(Trx trx, bool bChange);
        /// <summary>
        /// 
        /// </summary>
        private readonly bool m_bServerMode;
        /// <summary>
        /// 
        /// </summary>
        private LogManager m_LogManager;
        /// <summary>
        /// 
        /// </summary>
        private Dictionary<string, DateTime> m_hashProcessDataToTime;
        /// <summary>
        /// 
        /// </summary>
        public event ProcessAutoStartEventHandler EventProcessAutoStart;
        /// <summary>
        /// 
        /// </summary>
        public delegate void ProcessAutoStartEventHandler();
        /// <summary>
        /// 
        /// </summary>
        private readonly string m_sLogDriver;
        #endregion

        #region Class Properties
        public LogManager LogManager
        {
            get
            {
                switch (m_bServerMode)
                {
                    case true:
                        m_LogManager = m_MainForm_Server.LogManager;
                        break;
                    case false:
                        m_LogManager = m_MainForm_Client.LogManager;
                        break;
                }

                return m_LogManager;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public Dictionary<string, DateTime> ProcessDataToTime
        {
            get
            {
                return m_hashProcessDataToTime;
            }
        }
        #endregion

        #region Class Initialize
        /// <summary>
        /// 
        /// </summary>
        public Configuration(MainForm_Client mfClient, MainForm_Server mfServer)
        {
            m_syncObject = new object();
            m_hashBitTime = new Dictionary<string, DateTime>();
            m_hashProcessDataToTime = new Dictionary<string, DateTime>();
            m_Logger = new Esol.ShareTrxControl.Logger() { bProcessManager = true};

            if (mfClient != null)
            {
                m_bServerMode = false;
                m_MainForm_Client = mfClient;
                m_ShareMem = mfClient.ShareMem;
                m_ShareTrx = mfClient.ShareTrx;
                m_iLocal = mfClient.LocalNo;
                m_sLogDriver = mfClient.LogDriver;

                m_MainForm_Client.EventShareDataChanged += new MainForm_Client.ShareEventHandler(OnShareDataChanged);
            }
            else if (mfServer != null)
            {
                m_bServerMode = true;
                m_MainForm_Server = mfServer;
                m_iLocal = 1;
                m_ShareMem = mfServer.ShareMem;
                m_ShareTrx = mfServer.ShareTrx;
                m_sLogDriver = mfServer.LogDriver;

                m_MainForm_Server.EventShareDataChanged += new MainForm_Server.ShareEventHandler(OnShareDataChanged);
            }

            m_Logger.Initialize(m_ShareTrx, m_sLogDriver);
            m_timer = new Timer(new TimerCallback(Timer));
            m_timer.Change(Timeout.Infinite, Timeout.Infinite);
            ReChargeTimer();
            m_Logger.Start();
        }
        #endregion

        #region Class Private
        /// <summary>
        /// 
        /// </summary>
        private void OnShareDataChanged(PlcAddr addr)
        {
            try
            {
                CimShareEventFilter(addr);

                string[] sLocals = addr.AddrName.Split('_');

                switch (m_bServerMode)
                {
                    case true:
                        if (sLocals[0] == Constants.DEF_SERVER_LOCAL)
                            TimeOutBitCompare(addr);
                        break;
                    case false:
                        if (sLocals[0] != Constants.DEF_SERVER_LOCAL)
                            TimeOutBitCompare(addr);
                        break;
                }
            }
            catch (Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        /// <summary>
        /// 
        /// </summary>
        private void CimShareEventFilter(PlcAddr PlcMessage)
        {
            try
            {
                bool bGetBit = m_ShareMem.GetBit(PlcMessage);
                string sTrxId = string.Format("{0},{1}", PlcMessage.AddrName, bGetBit == true ? 1 : 0);

                if (m_ShareTrx.hashShareTrx.ContainsKey(sTrxId))
                {
                    Trx trx = m_ShareTrx.hashShareTrx[sTrxId];

                    switch (bGetBit)
                    {
                        case true:
                            if (m_bServerMode && sTrxId.Contains("FDC"))
                            {
                                bool bChange = false;
                                string[] arrayLocal = PlcMessage.AddrName.Split('_');

                                if (!m_hashProcessDataToTime.ContainsKey(arrayLocal[0]))
                                    m_hashProcessDataToTime.Add(arrayLocal[0], DateTime.Now);
                                else
                                    m_hashProcessDataToTime[arrayLocal[0]] = DateTime.Now;

                                EventFdcDataChanged(trx, bChange);
                            }
                            else if (!m_bServerMode && !sTrxId.Contains("FDC"))
                            {
                                if (sTrxId.Contains(Constants.DEF_RESTART_TEXT.ToUpper()))
                                {
                                    OnWindowsRestartCommand(trx);
                                    break;
                                }
                                else if (sTrxId.Contains("AUTO_START"))
                                    EventProcessAutoStart();
                                else if (sTrxId.Contains(Constants.DEF_START_TEXT))
                                    OnStartCommand(trx);
                                else if (sTrxId.Contains(Constants.DEF_KILL_TEXT))
                                    OnKillCommand(trx);

                            }
                            else if (m_bServerMode && sTrxId.Contains("REPLY"))
                            {
                                if (sTrxId.Contains(Constants.DEF_START_TEXT) ||
                                    sTrxId.Contains(Constants.DEF_KILL_TEXT))
                                {
                                    List<string> listReadItem = trx.ReadItem;
                                    byte iReadDtata = m_ShareMem.GetByte(m_ShareTrx.hashShareKeyToShareData[listReadItem[0]]);

                                    switch (iReadDtata)
                                    {
                                        case 1:
                                            //System.Windows.Forms.MessageBox.Show("정상적으로 수행 되었습니다.");
                                            break;
                                        case 2:
                                            System.Windows.Forms.MessageBox.Show("중복 수행 입니다.");
                                            break;
                                        case 3:
                                            System.Windows.Forms.MessageBox.Show("명령을 수행 할 수 없는 상태 입니다.");
                                            break;
                                    }
                                }
                            }
                            break;
                    }
                    m_Logger.LogWrite(PlcMessage);
                }
            }
            catch (Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        /// <summary>
        /// 
        /// </summary>
        private void TimeOutBitCompare(PlcAddr addr)
        {
            try
            {
                lock (m_syncObject)
                {
                    if (m_ShareMem.GetBit(addr))
                        m_hashBitTime[addr.AddrName] = DateTime.Now;
                    else
                    {
                        if (m_hashBitTime.ContainsKey(addr.AddrName))
                            m_hashBitTime.Remove(addr.AddrName);
                    }
                }
            }
            catch (Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        /// <summary>
        /// 
        /// </summary>
        private void Timer(object sender)
        {
            try
            {
                lock (m_syncObject)
                {
                    if (m_hashBitTime.Count < 1) return;

                    string[] sKeys = new string[m_hashBitTime.Count];

                    m_hashBitTime.Keys.CopyTo(sKeys, 0);

                    for (int i = 0; i < sKeys.Length; i++)
                    {
                        DateTime bitTime = m_hashBitTime[sKeys[i]].AddSeconds(DEF_BIT_TIME_OUT);

                        if (DateTime.Now.Ticks - bitTime.Ticks >= 0)
                        {
                            PlcAddr plcaddr = m_ShareTrx.hashShareKeyToShareData[sKeys[i]];
                            m_ShareMem.SetBit(plcaddr, false);
                        }
                    }
                }
            }
            catch { }
            finally { ReChargeTimer(); }
        }
        /// <summary>
        /// 
        /// </summary>
        private void ReChargeTimer()
        {
            m_timer.Change(DEF_TIMER_SCAN_TIME, Timeout.Infinite);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <summary>
        /// 
        /// </summary>
        private void OnStartCommand(Trx trx)
        {
            try
            {
                string[] sSplitDatas = trx.sTriggerId.Split('_');
                int iCommandNo = Convert.ToInt32(sSplitDatas[4]);
                string sAckWord = string.Format("L2_W_PROCESS_START_{0}_ACK", sSplitDatas[4]);

                PlcAddr addrAck = m_ShareTrx.hashShareKeyToShareData[sAckWord];

                if (m_MainForm_Client.HashProcessNoToName.ContainsKey(iCommandNo))
                {
                    string sProcessName = m_MainForm_Client.HashProcessNoToName[iCommandNo];

                    if (string.IsNullOrEmpty(sProcessName) || m_MainForm_Client.HashProcessDatas[sProcessName][2] == "0")
                    {
                        m_ShareMem.SetByte(addrAck, 3);
                        return;
                    }

                    Process[] processlist = Process.GetProcessesByName(sProcessName);

                    if (processlist.Length != 0)
                    {
                        m_ShareMem.SetByte(addrAck, 2);
                        return;
                    }

                    Process.Start(m_MainForm_Client.HashProcessDatas[sProcessName][1]);
                    m_ShareMem.SetByte(addrAck, 1);
                }
            }
            catch (Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        /// <summary>
        /// 
        /// </summary>
        private void OnKillCommand(Trx trx)
        {

            try
            {
                string[] sSplitDatas = trx.sTriggerId.Split('_');
                int iCommandNo = Convert.ToInt32(sSplitDatas[4]);
                string sAckWord = string.Format("L2_W_PROCESS_KILL_{0}_ACK", sSplitDatas[4]);
                PlcAddr addrAck = m_ShareTrx.hashShareKeyToShareData[sAckWord];

                if (m_MainForm_Client.HashProcessNoToName.ContainsKey(iCommandNo))
                {
                    string sProcessName = m_MainForm_Client.HashProcessNoToName[iCommandNo];

                    if (string.IsNullOrEmpty(sProcessName) || m_MainForm_Client.HashProcessDatas[sProcessName][2] == "0")
                    {
                        m_ShareMem.SetByte(addrAck, 3);
                        return;
                    }

                    Process[] processlist = Process.GetProcessesByName(sProcessName);

                    if (processlist.Length == 0)
                    {
                        m_ShareMem.SetByte(addrAck, 2);
                        return;
                    }

                    processlist[0].Kill();
                    m_ShareMem.SetByte(addrAck, 1);
                }

            }
            catch (Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        /// <summary>
        /// 
        /// </summary>
        private void OnWindowsRestartCommand(Trx trx)
        {
            try
            {
                PlcAddr addrRestart = m_ShareTrx.hashShareKeyToShareData["L2_W_PROCESS_RESTART_ACK"];
                m_ShareMem.SetByte(addrRestart, 1);
                Process.Start("shutdown.exe", "-r -t 30");
            }
            catch (Exception ex)
            {
                LogManager.ErrorWriteLog(ex.ToString());
            }
        }
        #endregion
    }
}
