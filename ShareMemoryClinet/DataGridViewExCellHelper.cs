﻿using System.Drawing;
using System.Windows.Forms;

namespace ProcessWatcher
{
    internal static class DataGridViewExCellHelper
    {
        public static Rectangle GetSpannedCellClipBounds<TCell>(TCell ownerCell, Rectangle cellBounds, bool singleVerticalBorderAdded, bool singleHorizontalBorderAdded) where TCell : DataGridViewCell, ISpannedCell
        {
            DataGridView dataGridView = ownerCell.DataGridView;
            Rectangle result = cellBounds;
            for (int i = ownerCell.ColumnIndex; i < ownerCell.ColumnIndex + ownerCell.ColumnSpan; i++)
            {
                DataGridViewColumn dataGridViewColumn = dataGridView.Columns[i];
                if (!dataGridViewColumn.Visible)
                {
                    continue;
                }

                if (dataGridViewColumn.Frozen || i > dataGridView.FirstDisplayedScrollingColumnIndex)
                {
                    break;
                }

                if (i == dataGridView.FirstDisplayedScrollingColumnIndex)
                {
                    result.Width -= dataGridView.FirstDisplayedScrollingColumnHiddenWidth;
                    if (dataGridView.RightToLeft != RightToLeft.Yes)
                    {
                        result.X += dataGridView.FirstDisplayedScrollingColumnHiddenWidth;
                    }

                    break;
                }

                result.Width -= dataGridViewColumn.Width;
                if (dataGridView.RightToLeft != RightToLeft.Yes)
                {
                    result.X += dataGridViewColumn.Width;
                }
            }

            for (int j = ownerCell.RowIndex; j < ownerCell.RowIndex + ownerCell.RowSpan; j++)
            {
                DataGridViewRow dataGridViewRow = dataGridView.Rows[j];
                if (dataGridViewRow.Visible)
                {
                    if (dataGridViewRow.Frozen || j >= dataGridView.FirstDisplayedScrollingRowIndex)
                    {
                        break;
                    }

                    result.Y += dataGridViewRow.Height;
                    result.Height -= dataGridViewRow.Height;
                }
            }

            if (dataGridView.BorderStyle != 0)
            {
                Rectangle clientRectangle = dataGridView.ClientRectangle;
                clientRectangle.Width--;
                clientRectangle.Height--;
                if (dataGridView.RightToLeft == RightToLeft.Yes)
                {
                    clientRectangle.X++;
                    clientRectangle.Y++;
                }

                result.Intersect(clientRectangle);
            }

            return result;
        }

        public static Rectangle GetSpannedCellBoundsFromChildCellBounds<TCell>(TCell childCell, Rectangle childCellBounds, bool singleVerticalBorderAdded, bool singleHorizontalBorderAdded) where TCell : DataGridViewCell, ISpannedCell
        {
            DataGridView dataGridView = childCell.DataGridView;
            TCell cell = (childCell.OwnerCell as TCell) ?? childCell;
            Rectangle result = childCellBounds;
            int firstVisibleColumnIndex = GetFirstVisibleColumnIndex(dataGridView, cell.ColumnIndex, cell.ColumnSpan);
            if (dataGridView.Columns[firstVisibleColumnIndex].Frozen)
            {
                result.X = dataGridView.GetColumnDisplayRectangle(firstVisibleColumnIndex, cutOverflow: false).X;
            }
            else
            {
                int num = 0;
                for (int i = firstVisibleColumnIndex; i < childCell.ColumnIndex; i++)
                {
                    DataGridViewColumn dataGridViewColumn = dataGridView.Columns[i];
                    if (dataGridViewColumn.Visible)
                    {
                        num += dataGridViewColumn.Width;
                    }
                }

                result.X = ((dataGridView.RightToLeft == RightToLeft.Yes) ? (result.X + num) : (result.X - num));
            }

            int firstVisibleRowIndex = GetFirstVisibleRowIndex(dataGridView, cell.RowIndex, cell.RowSpan);
            if (dataGridView.Rows[firstVisibleRowIndex].Frozen)
            {
                result.Y = dataGridView.GetRowDisplayRectangle(firstVisibleRowIndex, cutOverflow: false).Y;
            }
            else
            {
                int num2 = 0;
                for (int i = firstVisibleRowIndex; i < childCell.RowIndex; i++)
                {
                    DataGridViewRow dataGridViewRow = dataGridView.Rows[i];
                    if (dataGridViewRow.Visible)
                    {
                        num2 += dataGridViewRow.Height;
                    }
                }

                result.Y -= num2;
            }

            int num3 = 0;
            for (int i = cell.ColumnIndex; i < cell.ColumnIndex + cell.ColumnSpan; i++)
            {
                DataGridViewColumn dataGridViewColumn = dataGridView.Columns[i];
                if (dataGridViewColumn.Visible)
                {
                    num3 += dataGridViewColumn.Width;
                }
            }

            if (dataGridView.RightToLeft == RightToLeft.Yes)
            {
                result.X = result.Right - num3;
            }

            result.Width = num3;
            int num4 = 0;
            for (int i = cell.RowIndex; i < cell.RowIndex + cell.RowSpan; i++)
            {
                DataGridViewRow dataGridViewRow = dataGridView.Rows[i];
                if (dataGridViewRow.Visible)
                {
                    num4 += dataGridViewRow.Height;
                }
            }

            result.Height = num4;
            if (singleVerticalBorderAdded && InFirstDisplayedColumn(cell))
            {
                result.Width++;
                if (dataGridView.RightToLeft != RightToLeft.Yes)
                {
                    if (childCell.ColumnIndex != dataGridView.FirstDisplayedScrollingColumnIndex)
                    {
                        result.X--;
                    }
                }
                else if (childCell.ColumnIndex == dataGridView.FirstDisplayedScrollingColumnIndex)
                {
                    result.X--;
                }
            }

            if (singleHorizontalBorderAdded && InFirstDisplayedRow(cell))
            {
                result.Height++;
                if (childCell.RowIndex != dataGridView.FirstDisplayedScrollingRowIndex)
                {
                    result.Y--;
                }
            }

            return result;
        }

        public static DataGridViewAdvancedBorderStyle AdjustCellBorderStyle<TCell>(TCell cell) where TCell : DataGridViewCell, ISpannedCell
        {
            DataGridViewAdvancedBorderStyle dataGridViewAdvancedBorderStylePlaceholder = new DataGridViewAdvancedBorderStyle();
            DataGridView dataGridView = cell.DataGridView;
            return cell.AdjustCellBorderStyle(dataGridView.AdvancedCellBorderStyle, dataGridViewAdvancedBorderStylePlaceholder, DataGridViewHelper.SingleVerticalBorderAdded(dataGridView), DataGridViewHelper.SingleHorizontalBorderAdded(dataGridView), InFirstDisplayedColumn(cell), InFirstDisplayedRow(cell));
        }

        public static bool InFirstDisplayedColumn<TCell>(TCell cell) where TCell : DataGridViewCell, ISpannedCell
        {
            DataGridView dataGridView = cell.DataGridView;
            return dataGridView.FirstDisplayedScrollingColumnIndex >= cell.ColumnIndex && dataGridView.FirstDisplayedScrollingColumnIndex < cell.ColumnIndex + cell.ColumnSpan;
        }

        public static bool InFirstDisplayedRow<TCell>(TCell cell) where TCell : DataGridViewCell, ISpannedCell
        {
            DataGridView dataGridView = cell.DataGridView;
            return dataGridView.FirstDisplayedScrollingRowIndex >= cell.RowIndex && dataGridView.FirstDisplayedScrollingRowIndex < cell.RowIndex + cell.RowSpan;
        }

        private static int GetFirstVisibleColumnIndex(DataGridView dataGridView, int startIndex, int span)
        {
            for (int i = startIndex; i < startIndex + span; i++)
            {
                if (dataGridView.Columns[i].Visible)
                {
                    return i;
                }
            }

            return -1;
        }

        private static int GetFirstVisibleRowIndex(DataGridView dataGridView, int startIndex, int span)
        {
            for (int i = startIndex; i < startIndex + span; i++)
            {
                if (dataGridView.Rows[i].Visible)
                {
                    return i;
                }
            }

            return -1;
        }
    }

    internal static class DataGridViewHelper
    {
        public static bool SingleHorizontalBorderAdded(DataGridView dataGridView)
        {
            return !dataGridView.ColumnHeadersVisible && (dataGridView.AdvancedCellBorderStyle.All == DataGridViewAdvancedCellBorderStyle.Single || dataGridView.CellBorderStyle == DataGridViewCellBorderStyle.SingleHorizontal);
        }

        public static bool SingleVerticalBorderAdded(DataGridView dataGridView)
        {
            return !dataGridView.RowHeadersVisible && (dataGridView.AdvancedCellBorderStyle.All == DataGridViewAdvancedCellBorderStyle.Single || dataGridView.CellBorderStyle == DataGridViewCellBorderStyle.SingleVertical);
        }
    }
}
