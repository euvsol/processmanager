﻿#region Class using
using System;
using System.Threading;
using Esol.Framework;
using EsolSharedMemoryPacket;
using Esol.LogControl;
#endregion

namespace ProcessWatcher
{
    public class ShareMemoryServerSession : TcpServerSession
    {
        #region Class Memeber
        /// <summary>
        /// 
        /// </summary>
        public ShardMem m_ShardMem;
        /// <summary>
        /// 
        /// </summary>
        private LogManager m_Logmanager;
        /// <summary>
        /// 
        /// </summary>
        private SeverManager m_SeverManager;
        /// <summary>
        /// 
        /// </summary>
        private Timer m_ConnectionCheck;
        /// <summary>
        /// 
        /// </summary>
        private int m_iRecvWriteReq;
        /// <summary>
        /// 
        /// </summary>
        private int m_iRecvReadReq;
        #endregion

        #region Class Properties
        /// <summary>
        /// 
        /// </summary>
        private ShareMemoryManager ShareMemoryManager
        {
            get
            {
                return m_SeverManager.ShareMemoryManager;
            }
            set
            {
                m_SeverManager.ShareMemoryManager = value;
            }
        }
        #endregion

        #region Class Initialize
        /// <summary>
        /// 
        /// </summary>
        public ShareMemoryServerSession(AsyncSocketClient newClient, SeverManager severManager, LogManager logmanager)
            : base(newClient)
        {
            m_Logmanager = logmanager;
            m_SeverManager = severManager;
            m_ConnectionCheck = new Timer(CheckConnection, null, 5000, 5000);

            this.OnClose += new DNetCloseEventHandler(SyncSession_OnClose);
            this.OnReceive += new DNetReceiveEventHandler(SyncSession_OnReceive);
            this.OnConnet += new DNetConnectEventHandler(SyncSession_OnConnet);
            this.OnSend += new DNetSendEventHandler(SyncSession_OnSend);
            this.OnError += new DNetErrorEventHandler(SMSvrSession_OnError);
        }
        #endregion

        #region Class Evenet
        /// <summary>
        /// 
        /// </summary>
        private void SMSvrSession_OnError(object sender, DNetErrorEventArgs e)
        {
            m_SeverManager.CloseSession(this);
        }
        /// <summary>
        /// 
        /// </summary>
        private void SyncSession_OnSend(object sender, DNetSendEventArgs e)
        {

        }
        /// <summary>
        /// 
        /// </summary>
        private void SyncSession_OnConnet(object sender, DNetConnectionEventArgs e)
        {
        }
        /// <summary>
        /// 
        /// </summary>
        public void CheckConnection(object state)
        {
            this.SendPacket(new PkSyncTest());
        }
        /// <summary>
        /// 
        /// </summary>
        private void SyncSession_OnReceive(object sender, DNetReceiveEventArgs e)
        {
            if (e.RecvPacket is PkSyncReadReq)
            {
                PkSyncReadReq pk = e.RecvPacket as PkSyncReadReq;
                OnPkSyncReadReq(pk);
            }
            else if (e.RecvPacket is PkSyncWriteReq)
            {
                PkSyncWriteReq pk = e.RecvPacket as PkSyncWriteReq;
                OnPkSyncWriteReq(pk);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        private void SyncSession_OnClose(object sender, DNetConnectionEventArgs e)
        {
            m_SeverManager.CloseSession(this);
        }
        #endregion

        #region Class Private
        /// <summary>
        /// 
        /// </summary>
        private void OnPkSyncWriteReq(PkSyncWriteReq pk)
        {
            if (ShareMemoryManager.ListShardMemory.ContainsKey(pk.Name))
            {
                ShareMemoryManager.ListShardMemory[pk.Name].WriteBytes(pk.Start, pk.Length, pk.WriteBytes);

                double iInterval = (DateTime.Now - ShareMemoryManager.ListShardMemory[pk.Name].LastRWTime).TotalMilliseconds;

                if (Math.Abs(iInterval) > 500)
                    m_Logmanager.WriteLog(Constants.DEF_SHARE_MEMORY_CLIENT, string.Format("TIME OVER WRITE NAME =[{0}], START=[{1}], LENGTH=[{2}], TIME ={3}, INTERVAL ={4}", pk.Name, pk.Start, pk.Length, DateTime.Now, iInterval));

                PkSyncWriteRsp pkRsp = new PkSyncWriteRsp()
                {
                    ReqTime = pk.ReqTime,
                    Name = pk.Name,
                    Start = pk.Start,
                    Length = pk.Length,
                    Result = 0
                };

                this.SendPacket(pkRsp);

                if (m_iRecvWriteReq++ > 10000)
                {
                    m_Logmanager.WriteLog(Constants.DEF_SHARE_MEMORY_CLIENT, string.Format("WRITE RSP NAME =[{0}], START=[{1}], LENGTH=[{2}]", pk.Name, pk.Start, pk.Length));
                    m_iRecvWriteReq = 0;
                }
            }
            else
            {
                this.SendPacket(new PkSyncWriteRsp()
                {
                    ReqTime = pk.ReqTime,
                    Name = pk.Name,
                    Start = pk.Start,
                    Length = pk.Length,
                    Result = 1,

                });
                m_Logmanager.WriteLog(Constants.DEF_SHARE_MEMORY_CLIENT, string.Format("READ RSP NAME =[{0}], START=[{1}], LENGTH=[{2}]", pk.Name, pk.Start, pk.Length));
            }
        }
        /// <summary>
        /// 
        /// </summary>
        private void OnPkSyncReadReq(PkSyncReadReq pk)
        {
            byte[] readBytes = new byte[pk.Length];

            if (ShareMemoryManager.ListShardMemory.ContainsKey(pk.Name))
            {
                ShareMemoryManager.ListShardMemory[pk.Name].ReadBytes(pk.Start, pk.Length, out readBytes);

                double iInterval = (DateTime.Now - ShareMemoryManager.ListShardMemory[pk.Name].LastRWTime).TotalMilliseconds;

                if (Math.Abs(iInterval) > 500)
                    m_Logmanager.WriteLog(Constants.DEF_SHARE_MEMORY_CLIENT, string.Format("TIME OVER READ NAME =[{0}], START=[{1}], LENGTH=[{2}], TIME ={3}, INTERVAL ={4}", pk.Name, pk.Start, pk.Length, DateTime.Now, iInterval));

                ShareMemoryManager.ListShardMemory[pk.Name].LastRWTime = DateTime.Now;

                PkSyncReadRsp pkRsp = new PkSyncReadRsp()
                {
                    ReqTime = pk.ReqTime,
                    Name = pk.Name,
                    Start = pk.Start,
                    Length = pk.Length,
                    ReadBytes = readBytes,
                    Result = 0
                };

                this.SendPacket(pkRsp);

                if (m_iRecvReadReq++ > 10000)
                {
                    m_Logmanager.WriteLog(Constants.DEF_SHARE_MEMORY_CLIENT, string.Format("READ RSP NAME =[{0}], START=[{1}], LENGTH=[{2}]", pk.Name, pk.Start, pk.Length));
                    m_iRecvReadReq = 0;
                }
            }
            else
            {
                this.SendPacket(new PkSyncReadRsp()
                {
                    ReqTime = pk.ReqTime,
                    Name = pk.Name,
                    Start = pk.Start,
                    Length = pk.Length,
                    ReadBytes = readBytes,
                    Result = 1,
                });
            }
        }
        #endregion
    }
}
