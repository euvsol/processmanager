﻿#region Class using
using System;
using System.Collections.Generic;
using System.Timers;
using Esol.Framework;
using Esol.LogControl;
#endregion

namespace ProcessWatcher
{
    public class SeverManager
    {
        #region Class Member
        /// <summary>
        /// 
        /// </summary>
        private AsyncSocketServer m_Accepter;
        /// <summary>
        /// 
        /// </summary>
        private Timer m_TiemerAliveClient = new Timer(1000);
        /// <summary>
        /// 
        /// </summary>
        public List<TcpServerSession> m_listSessions = new List<TcpServerSession>();
        /// <summary>
        /// 
        /// </summary>
        public event EventHandler OnAccepted;
        /// <summary>
        /// 
        /// </summary>
        public event EventHandler OnClosed;
        /// <summary>
        /// 
        /// </summary>
        private int m_iPort;
        /// <summary>
        /// 
        /// </summary>
        private ShareMemoryManager m_ShareMemoryManager;
        /// <summary>
        /// 
        /// </summary>
        private LogManager m_Logmanager;
        #endregion

        #region Class Properties
        /// <summary>
        /// 
        /// </summary>
        public int Port
        {
            get
            {
                return m_iPort;
            }
            set
            {
                m_iPort = value;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public ShareMemoryManager ShareMemoryManager
        {
            get
            {
                return m_ShareMemoryManager;
            }
            set
            {
                m_ShareMemoryManager = value;
            }
        }
        #endregion

        #region Class public Event
        /// <summary>
        /// 
        /// </summary>
        public void Initialize(ShareMemoryManager ShareMemoryManger, LogManager logmanager)
        {
            m_Logmanager = logmanager;
            m_ShareMemoryManager = ShareMemoryManger;
        }
        /// <summary>
        /// 
        /// </summary>
        public void Start()
        {
            if (m_Accepter != null)
                Stop();

            m_Accepter = new AsyncSocketServer(Port);
            m_Accepter.OnAccept += new AsyncSocketAcceptEventHandler(Accepter_OnAccept);
            m_Accepter.Listen();

            m_TiemerAliveClient.Interval = 1000;
            m_TiemerAliveClient.Start();
        }
        /// <summary>
        /// 
        /// </summary>
        public void Stop()
        {
            if (m_Accepter != null)
                m_Accepter.Stop();

            if (m_TiemerAliveClient != null)
                m_TiemerAliveClient.Stop();
        }
        /// <summary>
        /// 연결 확인용
        /// </summary>
        private void TmrAliveClient_Elapsed(object sender, ElapsedEventArgs e)
        {

        }
        /// <summary>
        /// 
        /// </summary>
        private void Accepter_OnAccept(object sender, AsyncSocketAcceptEventArgs e)
        {
            AsyncSocketClient newClinet = new AsyncSocketClient(e.Worker.GetHashCode(), e.Worker);

            ShareMemoryServerSession session = new ShareMemoryServerSession(newClinet, this, m_Logmanager);

            lock (m_listSessions)
            {
                m_listSessions.Add(session);
            }

            session.StartRecv();

            OnAccepted?.Invoke(session, new EventArgs());
        }
        /// <summary>
        /// 
        /// </summary>
        public void CloseSession(ShareMemoryServerSession session)
        {
            lock (m_listSessions)
            {
                m_listSessions.Remove(session);
            }

            OnClosed?.Invoke(session, new EventArgs());
        }
        #endregion
    }
}
