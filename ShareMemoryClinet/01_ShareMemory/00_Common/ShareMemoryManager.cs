﻿#region Class using
using System.Collections.Generic;
using Esol.ShareMemory;
using Esol.LogControl;
#endregion

namespace ProcessWatcher
{
    public class ShareMemoryManager
    {
        #region Class Members
        /// <summary>
        /// 
        /// </summary>
        private SortedList<string, VirtualMemory> m_hashShardMemory;
        /// <summary>
        /// 
        /// </summary>
        private LogManager m_Logmanager;
        #endregion

        #region Class Properties
        /// <summary>
        /// 
        /// </summary>
        public SortedList<string, VirtualMemory> ListShardMemory
        {
            get
            {
                return m_hashShardMemory;
            }
            set
            {
                m_hashShardMemory = value;
            }
        }
        #endregion

        #region Class Initialize
        /// <summary>
        /// 
        /// </summary>
        public ShareMemoryManager(LogManager logmanager)
        {
            m_Logmanager = logmanager;
            m_hashShardMemory = new SortedList<string, VirtualMemory>();
        }
        #endregion

        #region Class Public Event
        /// <summary>
        /// 
        /// </summary>
        public void Open()
        {
            m_Logmanager.WriteLog(Constants.DEF_SHARE_MEMORY_CLIENT, "Shard Memory Manager Open");

            foreach (string sName in m_hashShardMemory.Keys)
            {
                m_Logmanager.WriteLog(Constants.DEF_SHARE_MEMORY_CLIENT,
                    string.Format("Shard Memory Open {0}, {1}", m_hashShardMemory[sName].Name, m_hashShardMemory[sName].MemorySize));
                m_hashShardMemory[sName].Open();
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public void Close()
        {
            foreach (string sName in m_hashShardMemory.Keys)
            {
                m_hashShardMemory[sName].Close();
                m_Logmanager.WriteLog(Constants.DEF_SHARE_MEMORY_CLIENT,
                    string.Format("Shard Memory Close {0}, {1}", m_hashShardMemory[sName].Name, m_hashShardMemory[sName].MemorySize));
            }

            m_Logmanager.WriteLog(Constants.DEF_SHARE_MEMORY_CLIENT, "Shard Memory Manager Closed");
        }
        #endregion
    }
}
