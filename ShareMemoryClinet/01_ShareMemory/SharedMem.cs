﻿using System;
using System.Collections.Generic;
using System.IO.MemoryMappedFiles;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace ProcessWatcher._01_ShareMemory
{
    class MappedFile
    {
        private MemoryMappedFile _mmf = null;
        private MemoryMappedViewAccessor _mmva = null;

        public int Create(int BufSize, string MapName)
        {
            _mmf = MemoryMappedFile.CreateOrOpen(MapName, BufSize, MemoryMappedFileAccess.ReadWrite);
            if (_mmf == null) return -1;

            _mmva = _mmf.CreateViewAccessor(0, BufSize);
            if (_mmva == null) return -2;

            return 0;
        }

        public int GetStringValue(ref string sValue, string sKey)
        {
            if (_mmva == null)
                return -1;

            if (!SharedMem.Knd.Data.MapData.ContainsKey(sKey))
                return -2;

            if (!int.TryParse(SharedMem.Knd.Data.MapData[sKey][(int)MapData.Address], out int addr))
                return -3;

            if (!int.TryParse(SharedMem.Knd.Data.MapData[sKey][(int)MapData.Size], out int size))
                return -4;

            var temp = new byte[size];
            int ret = _mmva.ReadArray<byte>(addr, temp, 0, size);
            if (ret == size)
            {
                for (int i = 0; i < size; i++)
                    sValue += (char)temp[i];
            }
            else
                return -5;

            return 0;
        }

        public int SetBytesValue(string sKey, byte value)
        {
            if (_mmva == null)
                return -1;

            if (!SharedMem.Knd.Data.MapData.ContainsKey(sKey))
                return -2;

            if (!int.TryParse(SharedMem.Knd.Data.MapData[sKey][(int)MapData.Address], out int addr))
                return -3;

            _mmva.Write(addr, value);

            return 0;
        }

        public int GetBytesValue(ref byte nValue, string sKey)
        {
            if (_mmva == null)
                return -1;

            if (!SharedMem.Knd.Data.MapData.ContainsKey(sKey))
                return -2;

            if (!int.TryParse(SharedMem.Knd.Data.MapData[sKey][(int)MapData.Address], out int addr))
                return -3;

            nValue = _mmva.ReadByte(addr);
            return 0;
        }

        public int GetDoubleValue(ref double dValue, string sKey)
        {
            if (_mmva == null)
                return -1;

            if (!SharedMem.Knd.Data.MapData.ContainsKey(sKey))
                return -2;

            if (!int.TryParse(SharedMem.Knd.Data.MapData[sKey][(int)MapData.Address], out int addr))
                return -3;

            dValue = _mmva.ReadDouble(addr);
            return 0;
        }

        public int GetBooleanValue(ref bool bValue, string sKey)
        {
            if (_mmva == null)
                return -1;

            if (!SharedMem.Knd.Data.MapData.ContainsKey(sKey))
                return -2;

            if (!int.TryParse(SharedMem.Knd.Data.MapData[sKey][(int)MapData.Address], out int addr))
                return -3;

            bValue = _mmva.ReadBoolean(addr);
            return 0;
        }

        public int SetBooleanValue(string key, bool value)
        {
            if (_mmva == null)
                return -1;

            if (!SharedMem.Knd.Data.MapData.ContainsKey(key))
                return -2;

            if (!int.TryParse(SharedMem.Knd.Data.MapData[key][(int)MapData.Address], out int addr))
                return -3;

            _mmva.Write(addr, value);

            return 0;
        }

        public int GetStructValue<T>(ref T stValue, string sKey) where T : struct
        {
            if (_mmva == null)
                return -1;

            if (!SharedMem.Knd.Data.MapData.ContainsKey(sKey))
                return -2;

            if (!int.TryParse(SharedMem.Knd.Data.MapData[sKey][(int)MapData.Address], out int addr))
                return -3;

            _mmva.Read<T>(addr, out stValue);
            return 0;
        }
    }

    class MappedData
    {
        const string MMF_BIT_PATH = "C:\\EUVSolution\\Config\\SharedMap\\Data_Bit.xml";
        const string MMF_WORD_PATH = "C:\\EUVSolution\\Config\\SharedMap\\Data_Word.xml";
        const string MMF_STRUCT_PATH = "C:\\EUVSolution\\Config\\SharedMap\\Data_Struct.xml";

        public int LoadMapData()
        {
            int ret = 0;

            XmlDocument xml = new XmlDocument();
            xml.Load(MMF_BIT_PATH);
            XmlNodeList xmlList = xml.SelectNodes("/Bits/Bit");
            foreach (XmlNode node in xmlList)
            {
                if (node.Attributes == null && node.Attributes["id"] == null) return -1;

                List<string> list = new List<string>();

                if (node.Attributes["address"] != null)
                    list.Add(node.Attributes["address"].Value);
                if (node.Attributes["type"] != null)
                    list.Add(node.Attributes["type"].Value);
                if (node.Attributes["size"] != null)
                    list.Add(node.Attributes["size"].Value);

                _mapData.Add(node.Attributes["id"].Value, list);
            }

            xml.Load(MMF_WORD_PATH);
            xmlList = xml.SelectNodes("/Words/Word");
            foreach (XmlNode node in xmlList)
            {
                if (node.Attributes == null && node.Attributes["id"] == null) return -1;

                List<string> list = new List<string>();

                if (node.Attributes["address"] != null)
                    list.Add(node.Attributes["address"].Value);
                if (node.Attributes["type"] != null)
                    list.Add(node.Attributes["type"].Value);
                if (node.Attributes["size"] != null)
                    list.Add(node.Attributes["size"].Value);

                _mapData.Add(node.Attributes["id"].Value, list);
            }

            xml.Load(MMF_STRUCT_PATH);
            xmlList = xml.SelectNodes("/Structs/Struct");
            foreach (XmlNode node in xmlList)
            {
                if (node.Attributes == null && node.Attributes["id"] == null) return -1;

                List<string> list = new List<string>();

                if (node.Attributes["address"] != null)
                    list.Add(node.Attributes["address"].Value);

                _mapData.Add(node.Attributes["id"].Value, list);
            }

            return ret;
        }

        public Dictionary<string, List<string>> MapData { get { return _mapData; } }

        private Dictionary<string, List<string>> _mapData = new Dictionary<string, List<string>>();
    }

    class SharedMem
    {
        private static SharedMem _instance = null;
        private static object sync = new object();

        public static SharedMem Knd
        {
            get
            {
                lock (sync)
                {
                    if (_instance == null)
                        _instance = new SharedMem();

                    return _instance;
                }
            }
        }

        private MappedFile _file = new MappedFile();
        private MappedData _data = new MappedData();

        public MappedFile File { get { return _file; } }
        public MappedData Data { get { return _data; } }
    }
}
