﻿#region Class using
using System;
using System.Collections.Generic;
using System.Threading;
using Esol.Framework;
using EsolSharedMemoryPacket;
#endregion

namespace ProcessWatcher
{
    public class SyncResult
    {
        #region Class Members
        /// <summary>
        /// 
        /// </summary>
        private DateTime m_dtReqTime;
        /// <summary>
        /// 
        /// </summary>
        private DateTime m_dtRspTime;
        #endregion

        #region Class Properties
        /// <summary>
        /// 
        /// </summary>
        public DateTime ReqTime
        {
            get
            {
                return m_dtReqTime;
            }
            set
            {
                m_dtReqTime = value;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime RspTime
        {
            get
            {
                return m_dtRspTime;
            }
            set
            {
                m_dtRspTime = value;
            }
        }
        #endregion
    }

    public class ShareMemorySyncSession
    {
        #region Class Members
        /// <summary>
        /// 
        /// </summary>
        public ShardMem m_ShardMem;
        /// <summary>
        /// 
        /// </summary>
        private TcpClinetSession m_Session = new TcpClinetSession();
        /// <summary>
        /// 
        /// </summary>
        private Thread m_ThreadWorker;
        /// <summary>
        /// 
        /// </summary>
        private string m_sIP;
        /// <summary>
        /// 
        /// </summary>
        private int m_iPort;
        /// <summary>
        /// 
        /// </summary>
        private string m_sShareMemoryName;
        /// <summary>
        /// 
        /// </summary>
        private int m_sShareMemorySize;
        /// <summary>
        /// 
        /// </summary>
        private int m_iSyncStart;
        /// <summary>
        /// 
        /// </summary>
        private int m_iSyncTime;
        /// <summary>
        /// 
        /// </summary>
        private int m_iSyncCommand;
        /// <summary>
        /// 
        /// </summary>
        private bool m_bRunning;
        /// <summary>
        /// 
        /// </summary>
        private DateTime m_ConnectDateTime;
        /// <summary>
        /// 
        /// </summary>
        private int m_iSyncSec;
        /// <summary>
        /// 
        /// </summary>
        private List<SyncResult> m_listSyncReult;
        /// <summary>
        /// 
        /// </summary>
        private int m_iReqPackCnt;
        /// <summary>
        /// 
        /// </summary>
        private int m_iPkSyncReadRspCnt;
        /// <summary>
        /// 
        /// </summary>
        private int m_iPkSyncWriteRspCnt;
        /// <summary>
        /// 
        /// </summary>
        public DateTime _sendTesterPacket = PcDateTime.Now;
        /// <summary>
        /// 
        /// </summary>
        private int m_iWorkProcess;
        #endregion

        #region Class Proeperites
        /// <summary>
        /// 
        /// </summary>
        public string IP
        {
            get
            {
                return m_sIP;
            }
            set
            {
                m_sIP = value;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public int Port
        {
            get
            {
                return m_iPort;
            }
            set
            {
                m_iPort = value;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public string ShareMemoryName
        {
            get
            {
                return m_sShareMemoryName;
            }
            set
            {
                m_sShareMemoryName = value;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public int ShareMemorySize
        {
            get
            {
                return m_sShareMemorySize;
            }
            set
            {
                m_sShareMemorySize = value;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public int SyncStart
        {
            get
            {
                return m_iSyncStart;
            }
            set
            {
                m_iSyncStart = value;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public int SyncTime
        {
            get
            {
                return m_iSyncTime;
            }
            set
            {
                m_iSyncTime = value;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public int SyncCommand
        {
            get
            {
                return m_iSyncCommand;
            }
            set
            {
                m_iSyncCommand = value;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public bool Running
        {
            get
            {
                return m_bRunning;
            }
            set
            {
                m_bRunning = value;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime ConnectDateTime
        {
            get
            {
                return m_ConnectDateTime;
            }
            set
            {
                m_ConnectDateTime = value;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public bool IsConnected
        {
            get
            {
                return m_Session.IsConnection;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public List<SyncResult> ListSyncResult
        {
            get
            {
                return m_listSyncReult;
            }
            set
            {
                m_listSyncReult = value;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public int SyncSec
        {
            get
            {
                return m_iSyncSec;
            }
            set
            {
                m_iSyncSec = value;
            }
        }
        #endregion

        #region Class Initialize
        /// <summary>
        /// 
        /// </summary>
        public ShareMemorySyncSession(string sIp, int iPort)
        {
            m_sIP = sIp;
            m_iPort = iPort;
            m_iPkSyncReadRspCnt = 0;
            m_iPkSyncWriteRspCnt = 0;
            m_iWorkProcess = 0;
            m_listSyncReult = new List<SyncResult>();
            m_Session.OnConnet += new DNetConnectEventHandler(Session_OnConnet);
            m_Session.OnClose += new DNetCloseEventHandler(Session_OnClose);
            m_Session.OnReceive += new DNetReceiveEventHandler(Session_OnReceive);
            m_Session.OnSend += new DNetSendEventHandler(Session_OnSend);
            m_Session.OnError += new DNetErrorEventHandler(Session_OnError);
        }
        #endregion

        #region Class Event
        /// <summary>
        /// 
        /// </summary>
        private void Session_OnError(object sender, DNetErrorEventArgs e)
        {

        }
        /// <summary>
        /// 
        /// </summary>
        private void Session_OnSend(object sender, DNetSendEventArgs e)
        {

        }
        /// <summary>
        /// 
        /// </summary>
        private void Session_OnReceive(object sender, DNetReceiveEventArgs e)
        {
            if (e.RecvPacket is PkSyncReadRsp)
            {
                lock (this)
                    m_iReqPackCnt--;

                OnPkSyncReadRsp((PkSyncReadRsp)e.RecvPacket);

            }
            else if (e.RecvPacket is PkSyncWriteRsp)
            {
                lock (this)
                    m_iReqPackCnt--;

                OnPkSyncWriteRsp((PkSyncWriteRsp)e.RecvPacket);
            }
            //e.RecvPacket is PkSyncTest
        }
        /// <summary>
        /// 
        /// </summary>
        private void Session_OnClose(object sender, DNetConnectionEventArgs e)
        {
            m_iReqPackCnt = 0;
        }
        /// <summary>
        /// 
        /// </summary>
        private void Session_OnConnet(object sender, DNetConnectionEventArgs e)
        {
            m_iReqPackCnt = 0;
            m_iPkSyncReadRspCnt = 0;
            m_iPkSyncWriteRspCnt = 0;
            ConnectDateTime = PcDateTime.Now;
        }
        #endregion

        #region Class Private
        /// <summary>
        /// 
        /// </summary>
        private void OnPkSyncWriteRsp(PkSyncWriteRsp pk)
        {
            if (pk.Result == 0)
            {
                lock (ListSyncResult)
                    ListSyncResult.Add(new SyncResult() { ReqTime = pk.ReqTime, RspTime = DateTime.Now });

                if (m_iPkSyncReadRspCnt++ > 10000)
                {
                    m_iPkSyncReadRspCnt = 0;
                    Logger.FileLogger.AppendLine(LogLevel.Info, "RECV Sync Read Rsp =[{0}], START=[{1:00000#}], LENGTH=[{2:00000#}], {3}", pk.Name, pk.Start, pk.Length, m_iPkSyncReadRspCnt);
                }
                m_ShardMem.LastRWTime = DateTime.Now;
            }
            else
                Logger.FileLogger.AppendLine(LogLevel.Warning, "WRITE RSP NAME =[{0}], START=[{1:00000#}], LENGTH=[{2:00000#}]", pk.Name, pk.Start, pk.Length);
        }
        /// <summary>
        /// 
        /// </summary>
        private void OnPkSyncReadRsp(PkSyncReadRsp pk)
        {
            if (pk.Result == 0)
            {
                m_ShardMem.WriteBytes(pk.Start, pk.Length, pk.ReadBytes);

                lock (ListSyncResult)
                    ListSyncResult.Add(new SyncResult() { ReqTime = pk.ReqTime, RspTime = DateTime.Now });

                if (m_iPkSyncWriteRspCnt++ > 10000)
                {
                    m_iPkSyncWriteRspCnt = 0;
                    Logger.FileLogger.AppendLine(LogLevel.Info, "RECV Sync Write Rsp =[{0}], START=[{1:00000#}], LENGTH=[{2:00000#}], {3}", pk.Name, pk.Start, pk.Length, m_iPkSyncWriteRspCnt);
                }
                m_ShardMem.LastRWTime = DateTime.Now;
            }
            else
                Logger.FileLogger.AppendLine(LogLevel.Warning, "READ RSP NAME =[{0}], START=[{1:00000#}], LENGTH=[{2:00000#}]", pk.Name, pk.Start, pk.Length);
        }
        #endregion

        #region Class Public
        /// <summary>
        /// 
        /// </summary>
        public void Start()
        {
            try
            {
                if (m_bRunning == false)
                {
                    m_bRunning = true;

                    m_ShardMem = new ShardMem()
                    {
                        Name = ShareMemoryName,
                        Size = ShareMemorySize
                    };

                    m_ShardMem.Open();

                    m_Session.IpAddress = IP;
                    m_Session.Port = Port;
                    m_Session.Start();


                    m_ThreadWorker = new Thread(OnWorking);
                    m_ThreadWorker.Start();
                }
            }
            catch (Exception ex)
            {
                Logger.FileLogger.AppendLine(LogLevel.Warning, string.Format("{0}, {1}", ex.Message, ex.StackTrace));
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public void OnWorking()
        {
            while (m_bRunning)
            {
                try
                {
                    if (m_Session.IsConnection)
                    {
                        SyncSec = DateTime.Now.Second;

                        if ((PcDateTime.Now - _sendTesterPacket).TotalSeconds > 5)
                        {
                            SendPacket(new PkSyncTest());
                            _sendTesterPacket = PcDateTime.Now;
                        }

                        if (m_iReqPackCnt > 1)
                        {
                            Thread.Sleep(10);
                            continue;
                        }

                        if (m_iWorkProcess++ % 1000 == 0)
                        {
                            Logger.FileLogger.AppendLine(LogLevel.Info, "Work SyncCommand=[{0}], NAME ={1}, START=[{2:00000#}], LENGTH=[{3:00000#}], {4}", SyncCommand, ShareMemoryName, SyncStart, ShareMemorySize, m_iWorkProcess);

                            if (m_iWorkProcess == int.MaxValue)
                                m_iWorkProcess = 0;

                        }

                        if (SyncCommand == 0)
                        {
                            PkSyncReadReq pk = new PkSyncReadReq()
                            {
                                ReqTime = DateTime.Now,
                                Name = ShareMemoryName,
                                Start = SyncStart,
                                Length = ShareMemorySize,
                            };
                            SendPacket(pk);

                            lock (this)
                                m_iReqPackCnt++;
                        }
                        else if (SyncCommand == 1)
                        {
                            byte[] read = new byte[ShareMemorySize];
                            m_ShardMem.ReadBytes(SyncStart, ShareMemorySize, out read);

                            PkSyncWriteReq pk = new PkSyncWriteReq()
                            {
                                ReqTime = DateTime.Now,
                                Name = ShareMemoryName,
                                Start = SyncStart,
                                Length = ShareMemorySize,
                                WriteBytes = read,
                            };

                            SendPacket(pk);
                            lock (this)
                                m_iReqPackCnt++;
                        }
                    }
                    Thread.Sleep(SyncTime);
                }
                catch (Exception ex)
                {
                    Logger.FileLogger.AppendLine(LogLevel.Warning, "{0}, {1}", ex.Message, ex.StackTrace);
                }
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public void SendPacket(Packet pk)
        {
            m_Session.SendPacket(pk);
        }
        /// <summary>
        /// 
        /// </summary>
        public void Stop()
        {
            m_bRunning = false;

            if (m_ThreadWorker != null)
                m_ThreadWorker.Join();

            if (m_Session.IpAddress != null)
                m_Session.Stop();

            if (m_ShardMem != null)
                m_ShardMem.Close();
        }
        /// <summary>
        /// 
        /// </summary>
        public void Disconnect()
        {
            m_Session.Disconnect();
        }
        #endregion
    }
}
