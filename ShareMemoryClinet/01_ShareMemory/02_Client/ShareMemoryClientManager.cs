﻿#region Class Using
using System.Collections.Generic;
#endregion

namespace ProcessWatcher
{
    public class ShareMemoryClientManager
    {
        #region Class Memeber
        /// <summary>
        /// 
        /// </summary>
        private List<ShareMemorySyncSession> m_listSyncSession;
        #endregion

        #region Class Properties
        /// <summary>
        /// 
        /// </summary>
        public List<ShareMemorySyncSession> LstSession
        {
            get
            {
                return m_listSyncSession;
            }
            set
            {
                m_listSyncSession = value;
            }
        }
        #endregion

        #region Class public
        /// <summary>
        /// 
        /// </summary>
        public ShareMemoryClientManager()
        {
            m_listSyncSession = new List<ShareMemorySyncSession>();
        }
        /// <summary>
        /// 
        /// </summary>
        public void Start()
        {
            LstSession.ForEach(f => f.Start());
        }
        /// <summary>
        /// 
        /// </summary>
        public void Stop()
        {
            LstSession.ForEach(f => f.Stop());
        }
        #endregion
    }
}
