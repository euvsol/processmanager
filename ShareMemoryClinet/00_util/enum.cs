﻿namespace ProcessWatcher
{
    /// <summary>
    /// 
    /// </summary>
    public enum eServerSettingKeys
    {
        AutoStart,
        Restart,
        LogDriver,
        LogDays,
        ServerName,
    }
    /// <summary>
    /// 
    /// </summary>
    public enum eClientSettingKeys
    {
        AutoStart,
        ClientAutoStart,
        LogDriver,
        LogDays,
        LocalName,
        LocalNo,
    }
    /// <summary>
    /// 
    /// </summary>
    public enum eProcessKeys
    {
        Name,
        Path,
        Command,
        ChildName1,
        ChildName2,
    }

    public enum eLastestVersion
    {
        Name, 
        Version
    }
    /// <summary>
    /// 
    /// </summary>
    public enum eServerShareMemoryKeys
    {
        Name,
        Size,
    }
    /// <summary>
    /// 
    /// </summary>
    public enum eClientShareMemoryKeys
    {
        No,
        Name,
        Ip,
        Size,
        SyncStart,
        SyncCommand,
        SyncTime,
    }
    /// <summary>
    /// 
    /// </summary>
    public enum eServerClientProcessKeys
    {
        Name,
        Ip,
        Process_1,
        Process_2,
        Process_3,
        Process_4,
        Process_5,
    }

    enum MapData
    {
        Address = 0,
        Type,
        Size
    }

    public enum ERROR_CODE
    {
        ICM_VERSION_NOT_MATCH,
        ITS_VERSION_NOT_MATCH,
        MIR_VERSION_NOT_MATCH,
        CIM_VERSION_NOT_MATCH,
        SOURCE_CONTROL_VERSION_NOT_MATCH,
        SOURCE_MONITORING_VERSION_NOT_AMTCH,
        ADAM_VERSION_NOT_MATCH,
        SOURCE_CONTROL_OFFLINE_STATUS,
        SOURCE_MONITORING_OFFLINE_STATUS,
        ADAM_OFFLINE_STATUS,
        SQ1_OFFLINE_STATUS
    }
}
