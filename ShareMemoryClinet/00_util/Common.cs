﻿#region using
using System.IO;
using System.Windows.Forms;
using Microsoft.Win32;
using System.Diagnostics;
using System;
using System.Management;
using System.Runtime.InteropServices;
using System.Threading;
#endregion

namespace ProcessWatcher
{
    internal static class GuiResource
    {
        [DllImport("user32.dll")]
        public static extern uint GetGuiResources(IntPtr hProcess, uint uiFlags);
    }

    public class Common
    {
        #region Class public
        /// <summary>
        /// 
        /// </summary>
        public static void OnStartUpAddRemove(string sProgramName, string sExecutablePath, bool bDelete = false)
        {
            try
            {
                RegistryKey strUpKey = Registry.CurrentUser.OpenSubKey(Constants.DEF_REG_STARTUP_PATH, true);

                if (!bDelete)
                {
                    if (strUpKey.GetValue(sProgramName) == null)
                    {
                        // 시작프로그램 등록명과 exe경로를 레지스트리에 등록
                        strUpKey.SetValue(sProgramName, sExecutablePath);
                        MessageBox.Show("등록 완료");
                    }
                    else
                        MessageBox.Show("등록되어 있는 상태입니다.");
                }
                else
                {
                    try
                    {
                        // 레지스트리값 제거
                        strUpKey.DeleteValue(sProgramName, false);

                        MessageBox.Show("삭제 완료");                        
                    }
                    catch
                    {
                        MessageBox.Show("삭제 되어 있습니다.");
                    }
                }                
            }
            catch
            { }
            /*
            using (RegistryKey regKey = GetRegKeyValue(Constants.DEF_REG_STARTUP_PATH, true))
            {
                if (!string.IsNullOrEmpty(sExecutablePath))
                {
                    try
                    {
                        if (regKey.GetValue(sProgramName) == null)
                        {
                            
                            regKey.SetValue(sProgramName, sExecutablePath);
                            MessageBox.Show("시작프로그램 등록이 완료 되었습니다.");
                        }
                        else
                            MessageBox.Show("이미 시작프로그램에 등록이 되어있습니다.");
                        regKey.Close();
                    }
                    catch { }
                }
                else
                {
                    try
                    {
                        if (regKey.GetValue(sProgramName) != null)
                        {
                            regKey.DeleteValue(sProgramName, false);
                            MessageBox.Show("시작프로그램 해제가 완료 되었습니다.");
                        }
                        else
                            MessageBox.Show("시작프로그램에 등록되어 있지 않습니다.");

                        regKey.Close();
                    }
                    catch { }
                }
            }*/
        }
        /// <summary>
        /// 
        /// </summary>
        public static void OnHelpFileCheck()
        {
            string sPath = System.Windows.Forms.Application.StartupPath;
            string sFullPath = Path.Combine(sPath, Constants.DEF_CHM_FILE_PATH);

            if (File.Exists(sFullPath))
                Process.Start(sFullPath);
            else
                MessageBox.Show("도움말 파일이 존재하지 않습니다.");
        }
        /// <summary>
        /// 
        /// </summary>
        public static string TotalCPU(string sProcessName)
        {
            try
            {
                switch (sProcessName)
                {
                    case Constants.DEF_TOTAL_TEXT:
                        var cpuCounter = new PerformanceCounter("Processor", "% Processor Time", "_Total");
                        
                        float totalCpuUsage = 0f;
                        int numSamples = 1000;
                        
                        for (int i = 0; i < numSamples; i++)
                        {
                            float cpuUsage = cpuCounter.NextValue();
                            totalCpuUsage += cpuUsage;
                        }

                        return string.Format("{0:N1} %", totalCpuUsage / numSamples); 
                    default:
                        Process[] process = Process.GetProcessesByName(sProcessName);
                        if (process.Length != 0)
                        {
                            TimeSpan oldTime = process[0].TotalProcessorTime;
                            Thread.Sleep(500);
                            TimeSpan newTime = process[0].TotalProcessorTime;
                            TimeSpan deltaTime = newTime - oldTime;
                            double dCpuUsage = deltaTime.TotalMilliseconds / (Environment.ProcessorCount * 10);
                            return string.Format("{0:N1} %", dCpuUsage);
                        }
                        else
                            return string.Empty;
                }
            }
            catch
            {
                return string.Empty;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public static string OnRamUsage()
        {
            //PerformanceCounter ramCounter = new PerformanceCounter("Memory", "% Committed Bytes In Use");
            //float ramUsagePercentage = ramCounter.NextValue();

            ManagementClass cls = new ManagementClass("Win32_OperatingSystem");
            
            foreach (ManagementObject mo in cls.GetInstances())
            {
                float iTotal = Convert.ToSingle(mo["TotalVisibleMemorySize"]) / 1024;
                float iFree = Convert.ToSingle(mo["FreePhysicalMemory"]) / 1024;

                float fSum = 100 - (100 * iFree / iTotal);
                return string.Format("{0:N1} %", fSum);
            }
            return string.Empty;
        }
        /// <summary>
        /// 
        /// </summary>
        public static string ProcessRAM(string sProcessName)
        {
            try
            {
                PerformanceCounter PC = new PerformanceCounter()
                {
                    CategoryName = "Process",
                    CounterName = "Working Set - Private",
                    InstanceName = sProcessName
                };

                return string.Format("{0:N1} MB", PC.NextValue() / 1024 / 1024);
            }
            catch
            {
                return string.Empty;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public static int OnTimeDiff(DateTime OldTime, int iScanTIme)
        {
            TimeSpan ts = DateTimeClone() - OldTime;
            int iTime = iScanTIme - (int)ts.TotalMilliseconds;

            return iTime;
        }

        public static uint GetGuiResourcesGDICount(Process pro)
        {
            try
            {
                return GuiResource.GetGuiResources(pro.Handle, 0);
            }
            catch
            {
                return 0;
            }
        }
        public static uint GetGuiResourcesUserCount(Process pro)
        {
            return GuiResource.GetGuiResources(pro.Handle, 1);
        }
        #endregion

        #region Class private
        /// <summary>
        /// 
        /// </summary>
        private static RegistryKey GetRegKeyValue(string regPath, bool writable)
        {
            return Registry.CurrentUser.OpenSubKey(regPath, writable);
        }
        #endregion

        #region Class Util
        /// <summary>
        /// 
        /// </summary>
        public static object Clone(object oValue)
        {
            object oReturnValue = oValue;

            return oReturnValue;
        }
        /// <summary>
        /// 
        /// </summary>
        public static DateTime DateTimeClone()
        {
            return DateTime.Now;
        }
        #endregion
    }
}
