﻿namespace ProcessWatcher
{
    public class Constants
    {
        /// <summary>
        /// 
        /// </summary>
        public const string DEF_SHARE_MEMORY_START_CLOSE_MESSAGE = "ShareMemory가 실행중입니다. 그래도 종료하시겠습니까?";
        /// <summary>
        /// 
        /// </summary>
        public const string DEF_SHARE_MEMORY_HIDE_MESSAGE = "ShareMemory가 시작되어 있지 않습니다.\n그래도 숨기시겠습니까?";
        /// <summary>
        /// 
        /// </summary>
        public const string DEF_SHRARE_MEMORY_CLOSE_MESSAGE = "종료하시겠습니까?";
        /// <summary>
        /// 
        /// </summary>
        public const string DEF_NOT_FILE_PATH = "Process 경로에 File이 없습니다.\nProcess Monitoring 기능이 정상 작동되지 않습니다.\n그래도 실행 하시겠습니까?";
        /// <summary>
        /// 
        /// </summary>
        public const string DEF_WINDOWS_RESTART_COMMAND_MESSAGE = "이명령을 수행하면 Client PC가 재부팅 됩니다.\n정말 실행 하시겠습니까?";
        /// <summary>
        /// 
        /// </summary>
        public const string DEF_PROCESS_KILL_COMMAND_MESSAGE = "이명령을 수행하면 Client Process가 종료 됩니다.\n정말 실행 하시겠습니까?";
        /// <summary>
        /// 
        /// </summary>
        public const string DEF_NOTIFICATION_MESSAGE = "알림";
        /// <summary>
        /// 
        /// </summary>
        public const string DEF_SHARE_MEMORY_CLIENT = "ProcessWatcher";
        /// <summary>
        /// 
        /// </summary>
        public const string DEF_PW_BTN = "ProcessWatcherBtn";
        /// <summary>
        /// 
        /// </summary>
        public const string DEF_SHARE_MEMORY_NAME = "ESOL_PROCESS_MEM";
        /// <summary>
        /// 
        /// </summary>
        public const string DEF_REG_STARTUP_PATH = @"SOFTWARE\Microsoft\Windows\CurrentVersion\Run";
        /// <summary>
        /// 
        /// </summary>
        public const string DEF_BIT_FILE_PATH = @"_ConfigFiles\Client\ShareMemory\01_Data_Bit.xml";
        /// <summary>
        /// 
        /// </summary>
        public const string DEF_WORD_FILE_PATH = @"_ConfigFiles\Client\ShareMemory\02_Data_Word.xml";
        /// <summary>
        /// 
        /// </summary>
        public const string DEF_STRUCT_FILE_PATH = @"_ConfigFiles\Client\ShareMemory\03_Data_Struct.xml";
        /// <summary>
        /// 
        /// </summary>
        public const string DEF_TRX_FILE_PATH = @"_ConfigFiles\Client\ShareMemory\05_Trx.xml";
        /// <summary>
        /// 
        /// </summary>
        public const string DEF_LOG_FILE_PATH = "D:\\_repository\\Log\\ProcessManager";
        /// <summary>
        /// 
        /// </summary>
        public const string DEF_TOTAL_TEXT = "_Total";
        /// <summary>
        /// 
        /// </summary>
        public const string DEF_SETTING_TEXT = "Setting";
        /// <summary>
        /// 
        /// </summary>
        public const string DEF_COUNT_TEXT = "Count";
        /// <summary>
        /// 
        /// </summary>
        public const string DEF_AUTOSTART_TEXT = "AutoStart";
        /// <summary>
        /// 
        /// </summary>
        public const string DEF_MEMORY_TEXT = "Memory";
        /// <summary>
        /// 
        /// </summary>
        public const string DEF_PROCESS_STRUCT = "L2_FDC_STRUCT";
        /// <summary>
        /// 
        /// </summary>
        public const string DEF_OK_TEXT = "ON";
        /// <summary>
        /// 
        /// </summary>
        public const string DEF_OFF_TEXT = "OFF";
        /// <summary>
        /// 
        /// </summary>
        public const string DEF_ENABLE_TEXT = "Enable";
        /// <summary>
        /// 
        /// </summary>
        public const string DEF_DISABLE_TEXT = "Disable";
        /// <summary>
        /// 
        /// </summary>
        public const string DEF_AUTO_START_ENABLE = "AutoStart Enable";
        /// <summary>
        /// 
        /// </summary>
        public const string DEF_AUTO_START_DISABLE = "AutoStart Disable";
        /// <summary>
        /// 
        /// </summary>
        public const string DEF_IP_SECTION = "IP";
        /// <summary>
        /// 
        /// </summary>
        public const string DEF_LOCAL_TEXT = "Local";
        /// <summary>
        /// 
        /// </summary>
        public const string DEF_CLIENT_TEXT = "Client";
        /// <summary>
        /// 
        /// </summary>
        public const string DEF_SERVER_TEXT = "Server";
        /// <summary>
        /// 
        /// </summary>
        public const string DEF_CHM_FILE_PATH = @"Process Watcher.chm";
        /// <summary>
        /// 
        /// </summary>
        public const string DEF_LOG_TEXT = "Log";
        /// <summary>
        /// 
        /// </summary>
        public const string DEF_SERVER_LOCAL = "L1";
        /// <summary>
        /// 
        /// </summary>
        public const string DEF_START_TEXT = "START";
        /// <summary>
        /// 
        /// </summary>
        public const string DEF_KILL_TEXT = "KILL";
        /// <summary>
        /// 
        /// </summary>
        public const string DEF_LOG_DAYS = "LogDays";
        /// <summary>
        /// 
        /// </summary>
        public const string DEF_LOG_DRIVER = "LogDriver";
        /// <summary>
        /// 
        /// </summary>
        public const string DEF_LOCAL_NAME_TEXT = "LocalName";
        /// <summary>
        /// 
        /// </summary>
        public const string DEF_RESTART_TEXT = "Restart";
        /// <summary>
        /// 
        /// </summary>
        public const string DEF_WINDWOS_RESTART_TEXT = "Windwos Restart";
        /// <summary>
        /// 
        /// </summary>
        public const int DEF_CLIENT_ADDRESS_SHIFT = 1000;
        /// <summary>
        /// 
        /// </summary>
        public const int CP_NOCLOSE_BUTTON = 0x200;
    }
}
