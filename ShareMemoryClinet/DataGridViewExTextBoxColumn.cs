﻿
using System.Windows.Forms;

namespace ProcessWatcher
{
    public class DataGridViewExTextBoxColumn : DataGridViewColumn
    {
        public DataGridViewExTextBoxColumn()
            : base(new DataGridViewExTextBoxCell())
        {
        }
    }
}
